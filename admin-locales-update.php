    
<?php

if(session_id() == '' || !isset($_SESSION)) {
  session_start();
}

if (!(isset($_SESSION['iniciado']))) {
  header ("Location: logeo.php");
  exit();
}

if ($_SESSION['tipo'] != 'A'){
  header ("Location: logeo.php");
  exit();
}


if ($_SESSION['iniciado'] != '5dbc98dcc983a70728bd082d1a47546e'){
  header ("Location: logeo.php");
  exit();

}

?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Drinky || Admin - Locales</title>

     <!--LOADING --> 
    <?php include("loading.php"); ?>
    <!--LOADING -->

  <!-- Bootstrap Core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom CSS -->
  <link href="css/sb-admin.css" rel="stylesheet">

  <!-- Custom Fonts -->
   <link rel="icon" type="image/png" href="images/drinky-logo.png" />
  <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="css/admin-general.css" rel="stylesheet" type="text/css">
  <link type="text/css" rel="stylesheet" href="css/chosen.min.css">
</head>



<body>

  <div id="wrapper">

    <?php include('admin-header.php'); ?>

    <div id="page-wrapper">

      <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
          <div class="col-lg-12">
            <h1 class="page-header">
              LOCALES

            </h1>
            <ol class="breadcrumb" >
              <li>
                <i class="fa fa-dashboard"></i>  <a href="admin-index.php">Tablero</a>
              </li>
              <li class="active">
                <i class="fa fa-file"></i> Alta / Baja / Modificación
              </li>
            </ol>


            <div class="page-content">
              <div id="tab-general" style="height:100vh;">
                <div class="col-lg-1 col-xs-12 col-sm-12"><!--relleno-->
                </div>

                <div class="col-md-12 col-lg-10 col-xs-12 col-sm-12">
                  <br>
                  <div class="panel panel-grey">
                    <div class="panel-heading">
                      <h1>Alta / Baja / Modificación</h1>
                    </div>
                    <div class="panel-body pan">
                      <!--h2 style="color:#FFF; background-color:#3C3C3C; padding:5px; padding-left:15px; margin-right:5px; margin-left:5px">&nbsp;Buscar un Bar:</h2> -->
                      <br>

                     <!-- <input type="hidden" id="usuariocarga" name="usuariocarga" value=" echo $_SESSION['usuario']; "> -->
                   
                      
                        <div class="form-group">
                            <label for="title" class="col-sm-12 control-label"><i class="glyphicon glyphicon-chevron-right color-oficial"></i>&nbsp;Seleccione Local</label>
                            <div id="divcombobar" class="col-sm-12">
                              <select name="m_cmbbar" class="form-control chosen-select"  id="m_cmbbar">
                                <option value="0">-- Seleccione un Local</option>
                                
                           <?php 

                           include('conexion/conexion.php');

                           $conection = mysqli_connect($host, $usuario, $pass,$db) or die("error de conexion a DB");

                           $select = $conection->query("SELECT idbar,nombre_bar,tipolocal FROM bares ORDER BY activo desc, tipolocal, nombre_bar");

                           while ($result = mysqli_fetch_assoc($select)) 
                           {
                            ?>     
                            <option value="<?php echo $result["idbar"];?>">
                             <?php echo $result["tipolocal"] . " " . $result["nombre_bar"] . ", " . $result["idbar"] . "</option>";

                           }
                           $select->close();
                           $conection->next_result();

                           ?>

                         </select>
                       </div>
                     </div>
              

                     <div class="row">
                      <div id="divcontenido" style="padding:15px;" class="hidden col-lg-12">
                        <div class="panel-body pan">
                          
                          <h2 style="color:#FFF; background-color:#3C3C3C; padding:5px; padding-left:15px; margin-right:5px; margin-left:5px">&nbsp;Datos del Local</h2>
                          <br> <br>
                        
                          <div id="formnombre" class="form-group">
                            <label for="title" class="col-sm-12 control-label"><i class="glyphicon glyphicon-chevron-right color-oficial"></i>&nbsp;Nombre</label>
                            <div class="col-sm-12">
                              <input type="text" name="m_txtnombre" class="form-control" id="m_txtnombre" placeholder="Introduce Nombre del Local">
                             <div id="m_complete_nombre" class="rojo hidden"><label><h5><b>Ingrese un Nombre (max 100 digitos)</b></h5></label></div><br>
                            </div>
                          </div>

                                                                 <div id="formemail" class="form-group">
                                                                    <label for="title" class="col-sm-12 control-label"><i class="glyphicon glyphicon-chevron-right color-oficial"></i>&nbsp;Email Usuario Dueño</label>
                                                                 
                                                                      <div class="col-sm-2" >

                                                                       <input type="checkbox" name="chkcambiarinput" onclick="cambiarinputusu()" id="chkcambiarinput" class="pull-left" style="width:50px;">Elegir Usuario Existente
                                                                     </div> 

                                                                     <div class="col-sm-10" id="divtxtusu">
                                                                          <input type="text" name="m_txtemail" class="form-control" id="m_txtemail" placeholder="Introduce Email Usuario">
                                                                     </div> 
                                                                     <div class="col-sm-10 hidden" id="divcombousu">
                                                                           <select name="m_cmbusu" class="form-control chosen-select" id="m_cmbusu"></select>
                                                                     </div> 

                                                                    <div class="col-sm-12 col-sm-offset-2">
                                                                     <div id="m_complete_email" class="rojo hidden"><label><h5><b>El email inválido (si no esta en la lista de usuarios, ese usuario no ha ingresado a la app).</b></h5></label></div><br>
                                                                      </div> 
                                                                 
                                                                 </div>

                          <div id="formtelefono" class="form-group">
                            <label for="title" class="col-sm-12 control-label"><i class="glyphicon glyphicon-chevron-right color-oficial"></i>&nbsp;Teléfono</label>
                            <div class="col-sm-12">
                              <input type="text" name="m_txttel" class="form-control" id="m_txttel" placeholder="Introduce Teléfono">
                                <div id="m_complete_tel" class="rojo hidden"><label><h5><b>Ingrese un teléfono valido (max 15 digitos, sin guiones)</b></h5></label></div><br>
                            </div>
                          </div>
                          <div id="formdire" class="form-group">
                            <label for="title" class="col-sm-12 control-label"><i class="glyphicon glyphicon-chevron-right color-oficial"></i>&nbsp;Dirección</label>
                            <div class="col-sm-12">
                              <input type="text" name="m_txtdire" class="form-control" id="m_txtdire" placeholder="Introduce Dirección">
                                <div id="m_complete_dire" class="rojo hidden"><label><h5><b>Ingrese una Dirección (max 200 digitos)</b></h5></label></div><br>
                            </div>
                          </div>
                          <div id="formloca" class="form-group">
                            <label for="title" class="col-sm-12 control-label"><i class="glyphicon glyphicon-chevron-right color-oficial"></i>&nbsp;Localidad</label>
                            <div class="col-sm-12">
                              <select name="m_cmbloca" class="form-control" id="m_cmbloca">
                              <!--  <option value="0">Seleccione una localidad</option> -->
                                <?php 

                                include('conexion/conexion.php');

                                $conection = mysqli_connect($host, $usuario, $pass,$db) or die("error de conexion a DB");

                                $select = $conection->query("SELECT idlocalidad,localidad FROM localidades ORDER BY localidad");

                                while ($result = mysqli_fetch_assoc($select)) 
                                {
                                  ?>     
                                  <option value="<?php echo $result["idlocalidad"];?>" id="optloca<?php echo $result["idlocalidad"];?>" title="<?php echo $result["localidad"];?>">
                                   <?php echo $result["localidad"] . "</option>";

                                 }
                                 $select->close();
                                 $conection->next_result();

                                 ?>

                               </select><br>
                             </div>
                           </div>
                           <div id="formbarrio" class="form-group">
                            <label for="title" class="col-sm-12 control-label"><i class="glyphicon glyphicon-chevron-right color-oficial"></i>&nbsp;Barrio</label>
                            <div class="col-sm-12">
                              <select name="m_cmbbarrio" class="form-control" id="m_cmbbarrio">
                              <!--  <option value="0">Seleccione un Barrio</option> -->
                                <?php 

                                include('conexion/conexion.php');

                                $conection = mysqli_connect($host, $usuario, $pass,$db) or die("error de conexion a DB");

                                $select = $conection->query("SELECT ba.idbarrio, ba.barrio, l.localidad FROM barrios ba INNER JOIN localidades l 
                                  ON ba.idlocalidad = l.idlocalidad ORDER BY localidad, barrio");

                                while ($result = mysqli_fetch_assoc($select)) 
                                {
                                  ?>     
                                  <option value="<?php echo $result["idbarrio"];?>" id="optbarrio<?php echo $result["idbarrio"];?>" title="<?php echo $result["localidad"];?>">
                                   <?php echo $result["localidad"] . ", " . $result["barrio"] . "</option>";

                                 }
                                 $select->close();
                                 $conection->next_result();

                                 ?>

                              </select>
                               <div id="m_complete_barrio" class="rojo hidden"><label><h5><b>Seleccione un Barrio que pertenezca a la localidad seleccionada</b></h5></label></div><br>
                            </div> 
                          </div>

                        
                          <label for="title" class="col-sm-12 control-label"><i class="glyphicon glyphicon-chevron-right color-oficial"></i>&nbsp;Album</label>
                          <div class="col-sm-12">
                            <input type="text" name="m_txtalbum" class="form-control" id="m_txtalbum" placeholder="Carpeta de Album de Fotos" disabled><br>
                          </div>

                            <div id="formtipolocal" class="form-group">
                                        <label for="title" class="col-sm-12 control-label"><i class="glyphicon glyphicon-chevron-right color-oficial"></i>&nbsp;Tipo Local</label>
                                        <div class="col-sm-12">
                                          <select class="form-control" name="m_cmbtipolocal" id="m_cmbtipolocal" placeholder="Selecciona un Tipo de Local">
                                            <option value="Bar">Bar</option>
                                            <option value="Boliche">Boliche</option>
                                          </select>
                                          <br>
                                      </div>
                                  </div>

                                  <div class="form-group">
                                    
                                   <label for="title" class="col-sm-12 control-label"><i class="glyphicon glyphicon-chevron-right color-oficial"></i>&nbsp;Estado</label>
                                   <div class="row">
                                    <div class="col-sm-12">  
                                      <div class="checkbox col-md-1 col-sm-1 col-xs-1" style="margin-left:15px;">
                                        <input type="checkbox" name="m_chkestado" onclick="validarcheck()" id="m_chkestado" class="pull-right form-control" id="m_estado">

                                      </div> 
                                      <div id="lblactivo">

                                      </div>
                                      
                                    </div>
                                  </div>
                                </div>
<!---->
                <div class="row"> 

                     <div class="alert alert-success hidden" style="text-align:center" id="msgmodificado" ><button class="close" data-dismiss="alert"><span>&times;</span></button>
                       <st><span class="glyphicon glyphicon-saved"></span><b> Estupendo! </b>Cambios Efectuados con Éxito. Actualize la página si modifico el nombre del local para verlo en la lista.</st>
                     <br><br><a href="admin-ubicacion-cordenadas.php"  class="btn btn-info">Modificar Cordenada de Dirección</a>
                     </div> 
                     <div class="alert alert-danger hidden" style="text-align:center" id="msgerror"><button class="close" data-dismiss="alert"><span>&times;</span></button>
                       <st><span class="glyphicon glyphicon-remove"> </span><b> Error! </b> No se pudo efectuar la modificación.</st>
                     </div>  
                    <div style="padding-left:15px;padding-right:15px">
                       <button style="text-align:center" onclick="guardarcambios()" id="m_btn_agregar" name="m_btn_agregar" type="button" class="pull-right btn btn-success btn-lg"><i class="fa fa-save"></i>&nbsp;&nbsp;Guardar</button>
                    </div>


                </div>
<!---->
                      </div>

                    </div>

                  </div>

                </div>
              </div>


            </div>
          </div>

          <div class="col-lg-1 col-xs-12 col-sm-12"> <!--relleno-->
          </div>
        </div>



      </div>


    </div>
  </div>
  <!-- /.row -->

</div>
<!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="js/jquery.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>
<script src="js/chosen.jquery.min.js"></script>

   <script>
    function loadingON(){
    $('#loadingDiv').removeClass('hidden');
   }
   function loadingOFF(){
    $('#loadingDiv').addClass('hidden');
    }
   </script>

<script>

        //INICIALIZO LOS CHOSEN SELECT
        function iniciarchosen(){
             $(".chosen-select").chosen({
                no_results_text: "No hay resultados!",
                placeholder_text_single: "Seleccionar",
                placeholder_text_multiple: "Seleccionar"
        });
        }
        iniciarchosen();

      //CAMBIAR INPUTUSU
      function cambiarinputusu(){
       if ($('#chkcambiarinput').prop('checked')){
       $('#divtxtusu').addClass('hidden');
       $('#divcombousu').removeClass('hidden');
       }else{
       $('#divcombousu').addClass('hidden');
       $('#divtxtusu').removeClass('hidden');

       }
      }

        //TRAER USUARIOS
        $( document ).ready(function() {
            $.ajax({
                url     : 'ajax/admin_traerusuarios.ajax.php',
                type    : 'POST',
                dataType: 'json',
                success: function( data ) {
                    $("#m_cmbusu").append('<option value="0">Seleccione usuario</option>');
                    for(var i = 0; i < data.length; i++) {
                        $("#m_cmbusu").append('<option value="'+ data[i][0] +'">'+ data[i][0] +'</option>');
                        $('#m_cmbusu').trigger("chosen:updated");
                    }

                },
                error: function( data ) {
                    alert("Ocurrio un error al cargar usuarios.\n Por favor recargue la pagina e intente nuevamente!");
                    $('#agregar').prop('disabled', false);
                }
            });
        });
// FIN TRAER USUARIOS PARA AUTOCOMPLETADO DE CAMPO

function validarcheck(){
    if ($('#m_chkestado').prop('checked')){
        $('#lblactivo').empty();
        $('#lblactivo').append('<label style="padding-top:12px;color:green;"><h4><b>ACTIVO</b></h4></label>');   
        $('#m_chkestado').val('S');
    }else {
        $('#lblactivo').empty();
        $('#lblactivo').append('<label style="padding-top:12px;color:red;"><h4><b>NO ACTIVO</b></h4></label>');  
        $('#m_chkestado').val('N');
    }
}

//TRAER DATOS DE LOCAL PARA MODIFICAR

$("select[id=m_cmbbar]").change(function(){

//REINICIAR INPUTS DE MAILS
$('#chkcambiarinput').prop('checked',false);
cambiarinputusu();

 var m_idbar = $('#m_cmbbar').val();

if (m_idbar == 0){
  anularmensajes();
  $('#divcontenido').addClass('hidden');
}else{

 $.ajax({
  url: 'ajax/admin_loadmodificar_bares.ajax.php',
  data: {'barid':m_idbar},
  type: 'POST',
  dataType: 'json',

             beforeSend: function () {
              $('body').addClass('loading'); //Agregamos la clase loading al body
            },

  success: function( data ) {
    console.log('peticion lograda con exito');
    console.log(data);

anularmensajes();
     
      $('#divcontenido').removeClass('hidden');
      $('#m_txtnombre').val(data[0][0]);
      $('#m_txttel').val(data[0][1]);
      $('#m_txtemail').val(data[0][2]);
      $('#m_txtdire').val(data[0][3]);
      $('#m_cmbloca').val(data[0][4]);
      $('#m_cmbbarrio').val(data[0][5]);
      $('#m_txtalbum').val(data[0][6]);
      $('#m_cmbtipolocal').val(data[0][12]);

      if (data[0][7] == 'S'){
        $('#m_chkestado').prop('checked',true);

        validarcheck();
      }else if (data[0][7] == 'N'){
        $('#m_chkestado').prop('checked',false); 

        validarcheck();
      }

      $('body').removeClass('loading'); //Removemos la clase loading
     },             
      error: function( data ) {
      console.log('se ejecuto mal la peticion');
      console.log(data);

      $('body').removeClass('loading'); //Removemos la clase loading
      }, 
      })
}

});

//ENVIAR AJAX DE MODIFICACION

function guardarcambios(){

  var m_usu
  if ($('#chkcambiarinput').prop('checked')){
  m_usu = $('#m_cmbusu').val();
  }else{
  m_usu = $('#m_txtemail').val();  
 
  }

var m_id = $('#m_cmbbar').val();
var m_nom = $('#m_txtnombre').val();
var m_tel = $('#m_txttel').val();
var m_dire = $('#m_txtdire').val();
var m_loca = $('#m_cmbloca').val();
var m_barrio = $('#m_cmbbarrio').val();
var m_activo = $('#m_chkestado').val();
var txtlocalidad = ($('#optloca'+$('#m_cmbloca').val()).attr("title"));
var txtbarrio_localidad = ($('#optbarrio'+$('#m_cmbbarrio').val()).attr("title"));
var m_tipolocal = $('#m_cmbtipolocal').val();

if (m_nom != "" && m_nom.length < 101 && m_usu != "" && m_usu.length < 101 && validarEmail(m_usu) != false && !isNaN(m_tel) && m_tel.length < 16 && m_tel != "" && m_dire.length < 201 && m_dire != "" && txtlocalidad == txtbarrio_localidad){
  $('#formnombre').removeClass('has-error');
  $('#formemail').removeClass('has-error');
  $('#formtelefono').removeClass('has-error');
  $('#formdire').removeClass('has-error');
  $('#formbarrio').removeClass('has-error');

  $('#m_complete_nombre').addClass('hidden');
  $('#m_complete_email').addClass('hidden');
  $('#m_complete_tel').addClass('hidden');
  $('#m_complete_dire').addClass('hidden');
  $('#m_complete_barrio').addClass('hidden');

  $('#formnombre').addClass('has-success');
  $('#formemail').addClass('has-success');
  $('#formtelefono').addClass('has-success');
  $('#formdire').addClass('has-success');
  $('#formloca').addClass('has-success');
  $('#formbarrio').addClass('has-success');

  $.ajax({
  url: 'ajax/admin_updatebar.ajax.php',
  data: {'barid':m_id,'nombar':m_nom,'usubar':m_usu,'telbar':m_tel,'direbar':m_dire,'locabar':m_loca,'barriobar':m_barrio,'estado':m_activo, 'tipo_local':m_tipolocal},
  type: 'POST',
  dataType: 'json',

             beforeSend: function () {
              $('body').addClass('loading'); //Agregamos la clase loading al body
            },

  success: function( data ) {
    console.log('peticion lograda con exito');
    console.log(data);

      $('#msgerror').addClass('hidden');
      $('#msgmodificado').removeClass('hidden');

      //actualizarComboBares(m_id); //NO FUNCA EL COMBO DESP DE ACTUALIZAR
      $('body').removeClass('loading'); //Removemos la clase loading
     },             
      error: function( data ) {
      console.log('se ejecuto mal la peticion');
      console.log(data);

      $('#msgerror').removeClass('hidden');
      $('#msgmodificado').addClass('hidden');

      $('body').removeClass('loading'); //Removemos la clase loading
      }, 
      })

  }else{

    $('#msgerror').addClass('hidden');
    $('#msgmodificado').addClass('hidden');

    if (m_nom == "" || m_nom.length > 100){
      $('#m_complete_nombre').removeClass('hidden');
      $('#formnombre').removeClass('has-success');
      $('#formnombre').addClass('has-error');
    }else{
      $('#m_complete_nombre').addClass('hidden');
      $('#formnombre').removeClass('has-error');
      $('#formnombre').addClass('has-success');
    }

    if (validarEmail(m_usu) ==false || m_usu == "" || m_usu.length > 100){
      $('#m_complete_email').removeClass('hidden');
      $('#formemail').removeClass('has-success');
      $('#formemail').addClass('has-error');
    }else{
      $('#m_complete_email').addClass('hidden');  
      $('#formemail').removeClass('has-error');
      $('#formemail').addClass('has-success');
    }

    if(m_tel == "" || m_tel.length > 15 || isNaN(m_tel)){
      $('#m_complete_tel').removeClass('hidden');
      $('#formtelefono').removeClass('has-success');
      $('#formtelefono').addClass('has-error');
    }else{
      $('#m_complete_tel').addClass('hidden');
      $('#formtelefono').removeClass('has-error');
      $('#formtelefono').addClass('has-success');
    }

    if (m_dire == "" || m_dire.length > 200){
      $('#m_complete_dire').removeClass('hidden');
      $('#formdire').removeClass('has-success');
      $('#formdire').addClass('has-error');
    }else{
      $('#m_complete_dire').addClass('hidden');
      $('#formdire').removeClass('has-error');
      $('#formdire').addClass('has-success');
    }

     if (txtlocalidad == txtbarrio_localidad){
      $('#formbarrio').removeClass('has-error');
      $('#formloca').addClass('has-success');
      $('#formbarrio').addClass('has-success');
      $('#m_complete_barrio').addClass('hidden');
     }else{
      $('#m_complete_barrio').removeClass('hidden');
      $('#formloca').addClass('has-success');
      $('#formbarrio').removeClass('has-success');
      $('#formbarrio').addClass('has-error');
     }

  }

}


function validarEmail( email ) {
    expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if ( !expr.test(email) )
       return false;
}

/*
//ACTUALIZAR COMBO, NO TA FUNCANDO
function actualizarComboBares(barseleccionado){

  $.ajax({
  url: 'ajax/admin_actualizarcmbbares.ajax.php',
  type: 'POST',
  dataType: 'json',

  success: function( data ) {
    console.log('peticion lograda con exito');
    console.log(data);

    $('#divcombobar').empty();
    $('#divcombobar').append('<select class="form-control chosen-select" id="m_cmbbar" name="m_cmbbar">');

    $('#m_cmbbar').append('<option value="0">Seleccione un Local</option>');

     for(var i = 0; i < data.length; i++) {
       $("#m_cmbbar").append('<option value="'+data[i][0]+'">'+data[i][0]+', '+data[i][2]+' '+data[i][1]+'</option>');
     }

     $('#divcombobar').append('</select>');
     $('#m_cmbbar').val(barseleccionado);
      iniciarchosen();
     

     },             
      error: function( data ) {
      console.log('se ejecuto mal la peticion');
      console.log(data);

      }, 
    })
}*/

function anularmensajes(){
$('#msgerror').addClass('hidden');
$('#msgmodificado').addClass('hidden');
$('#m_complete_nombre').addClass('hidden');
$('#m_complete_email').addClass('hidden');
$('#m_complete_tel').addClass('hidden');
$('#m_complete_dire').addClass('hidden');
$('#m_complete_barrio').addClass('hidden');
$('#formdire').removeClass('has-success');
$('#formdire').removeClass('has-error');
$('#formnombre').removeClass('has-success');
$('#formnombre').removeClass('has-error');
$('#formtelefono').removeClass('has-success');
$('#formtelefono').removeClass('has-error');
$('#formloca').removeClass('has-success');
$('#formloca').removeClass('has-error');
$('#formbarrio').removeClass('has-success');
$('#formbarrio').removeClass('has-error');
$('#formemail').removeClass('has-success');
$('#formemail').removeClass('has-error');
}
</script>
<script> loadingOFF();</script>
<div class="modalloading"></div>
</body>

</html>

