<?php 

include_once('conexion/conexion.php');
//include_once('inc/auditoria.php');

if(session_id() == '' || !isset($_SESSION)) {
		session_start();
}

if(!empty($_SESSION['iniciado']))
{
	
	//$objFun = new insert_auditoria;
	//$objFun->cargarAuditFin($conection,$_SESSION['usuario']);
	$_SESSION['iniciado'] = '';
	$_SESSION['usuario'] = '';
	$_SESSION['nombre'] = '';

	session_destroy();
}

header("Location:logeo.php");


?>

