    
<?php

if(session_id() == '' || !isset($_SESSION)) {
  session_start();
}

if (!(isset($_SESSION['iniciado']))) {
  header ("Location: logeo.php");
  exit();
}

if ($_SESSION['tipo'] != 'A'){
  header ("Location: logeo.php");
  exit();
}


if ($_SESSION['iniciado'] != '5dbc98dcc983a70728bd082d1a47546e'){
  header ("Location: logeo.php");
  exit();

}

?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Drinky || Admin - Ubicaciones </title>

   <!--LOADING --> 
  <?php include("loading.php"); ?>
  <!--LOADING -->  <!-- Bootstrap Core CSS -->

  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom CSS -->
  <link href="css/sb-admin.css" rel="stylesheet">

  <!-- Custom Fonts -->
  <link rel="icon" type="image/png" href="images/drinky-logo.png" />
  <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="css/admin-general.css" rel="stylesheet" type="text/css">
  <link type="text/css" rel="stylesheet" href="css/chosen.min.css">
</head>

<body>

  <div id="wrapper">

    <?php include('admin-header.php'); ?>

    <div id="page-wrapper">

      <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
          <div class="col-lg-12">
            <h1 class="page-header">
              UBICACIONES

            </h1>
            <ol class="breadcrumb" >
              <li>
                <i class="fa fa-dashboard"></i>  <a href="admin-index.php">Tablero</a>
              </li>
              <li class="active">
                <i class="fa fa-file"></i> ABM Barrios
              </li>
            </ol>


            <div class="page-content">
              <div id="tab-general" style="height:100vh;">
                <div class="col-lg-1 col-xs-12 col-sm-12"><!--relleno-->
                </div>

                <div class="col-md-12 col-lg-10 col-xs-12 col-sm-12">

                  <div class="panel panel-grey">
                    <div class="panel-heading">
                      <h1>Alta / Baja / Modificación de Barrios</h1>
                    </div>
                    <div class="panel-body pan">

                     <div class="row">

                    <div style="padding-left:15px;paddin-right:15px;" class="col-md-12">
                      <div id="divoperacion" class="form-group">
                            <label for="title" class="col-sm-12 control-label"><i class="glyphicon glyphicon-chevron-right color-oficial"></i>&nbsp;Operación</label>
                            <div class="col-sm-12">
                             <div id="divcmboperacion">
                            <select name="cmboperacion" class="form-control chosen-select" id="cmboperacion">
                                           <option value="0">Seleccione un Tipo de Operación</option> 
                                           <option value="agregar">Agregar</option> 
                                           <option value="modificar">Modificar</option> 
                                           <option value="eliminar">Eliminar</option> 
                            </select><br>
                            </div>
                              <div id="complete_operacion" class="rojo hidden"><label><h5><b>Debe seleccionar un Tipo de Operación</b></h5></label></div><br>
                            </div>
                          </div>
                      </div>
                      <!-- AGREGAR BARRIO -->
                      <div id="divcontenido1" style="padding-left:15px;paddin-right:15px;" class="hidden col-md-12">
                        <div class="panel-body pan">

                          <h2 style="color:#FFF; background-color:#3C3C3C; padding:5px; padding-left:15px; margin-right:5px; margin-left:5px">&nbsp;Agregar Barrio</h2>
                          <br>

                             <div id="a_divformloca" class="form-group">
                                    <label for="title" class="col-sm-12 control-label"><i class="glyphicon glyphicon-chevron-right color-oficial"></i>&nbsp;Localidad</label>
                                    <div class="col-sm-12">
                                      <select name="a_cmbloca" class="form-control chosen-select" id="a_cmbloca">
                                           <option value="0">Seleccione una Localidad</option> 
                                          <?php 

                                          include('conexion/conexion.php');

                                          $conection = mysqli_connect($host, $usuario, $pass,$db) or die("error de conexion a DB");

                                          $select = $conection->query("SELECT idlocalidad,localidad FROM localidades ORDER BY localidad");

                                          while ($result = mysqli_fetch_assoc($select)) 
                                          {
                                              ?>     
                                              <option value="<?php echo $result["idlocalidad"];?>" id="optloca<?php echo $result["idlocalidad"];?>" title="<?php echo $result["localidad"];?>">
                                               <?php echo $result["localidad"] . "</option>";

                                           }
                                           $select->close();
                                           $conection->next_result();

                                           ?>

                                       </select><br>
                                       <div id="a_complete_loca" class="rojo hidden"><label><h5><b>Seleccione una Localidad</b></h5></label></div><br>
                                   </div>
                               </div>

                          <div id="a_divformbarrio" class="form-group">
                            <label for="title" class="col-sm-12 control-label"><i class="glyphicon glyphicon-chevron-right color-oficial"></i>&nbsp;Barrio</label>
                            <div class="col-sm-12">
                              <input type="text" name="a_txtbarrio" class="form-control" id="a_txtbarrio" placeholder="Introduce un Barrio">
                              <div id="complete_barrio" class="rojo hidden"><label><h5><b>Debe ingresar un Barrio (no números)</b></h5></label></div><br>
                            </div>
                          </div>

                         <div style="padding-left:15px;padding-right:15px">
                           <button style="text-align:center" onclick="abmbarrio('agregar')" id="btn_agregar" name="btn_agregar" type="button" class="pull-right btn btn-success btn-lg"><i class="fa fa-plus-square"></i>&nbsp;&nbsp;Agregar</button>
                         </div>

                       </div>

                       <!---->
                       <div class="row"> 
                        <div class="col-sm-12">
                         <div class="alert alert-success hidden" style="text-align:center" id="msgagregado"><button class="close" data-dismiss="alert"><span>&times;</span></button>
                           <st><span class="glyphicon glyphicon-saved"></span><b> Estupendo! </b>Barrio Agregado con Éxito</st>
                         </div> 
                         <div class="alert alert-danger hidden" style="text-align:center" id="msgerror"><button class="close" data-dismiss="alert"><span>&times;</span></button>
                           <st><span class="glyphicon glyphicon-remove"> </span><b> Error! </b> No se pudo agregar el Barrio.</st>
                         </div>  
                       </div>

                     </div>
                     <!---->
                   </div>
<!-- /AGREGAR BARRIO-->

<!-- MODIFICAR BARRIO -->
                   <div id="divcontenido2" style="padding-left:15px;paddin-right:15px;" class="hidden col-md-12">
                    <div class="panel-body pan">

                      <h2 style="color:#FFF; background-color:#3C3C3C; padding:5px; padding-left:15px; margin-right:5px; margin-left:5px">&nbsp;Modificar Barrio</h2>
                      <br> 
                                  <div id="m_divformloca" class="form-group">
                                    <label for="title" class="col-sm-12 control-label"><i class="glyphicon glyphicon-chevron-right color-oficial"></i>&nbsp;Localidad</label>
                                    <div class="col-sm-12">
                                      <select name="m_cmbloca" class="form-control chosen-select" id="m_cmbloca">
                                           <option value="0">Seleccione una Localidad</option> 
                                          <?php 

                                          include('conexion/conexion.php');

                                          $conection = mysqli_connect($host, $usuario, $pass,$db) or die("error de conexion a DB");

                                          $select = $conection->query("SELECT idlocalidad,localidad FROM localidades ORDER BY localidad");

                                          while ($result = mysqli_fetch_assoc($select)) 
                                          {
                                              ?>     
                                              <option value="<?php echo $result["idlocalidad"];?>" id="optloca<?php echo $result["idlocalidad"];?>" title="<?php echo $result["localidad"];?>">
                                               <?php echo $result["localidad"] . "</option>";

                                           }
                                           $select->close();
                                           $conection->next_result();

                                           ?>

                                       </select><br><div id="m_complete_loca" class="rojo hidden"><label><h5><b>Seleccione una Localidad</b></h5></label></div><br>
                                   </div>
                               </div>

                      <div id="m_divformbarrio" class="form-group">
                        <label for="title" class="col-sm-12 control-label"><i class="glyphicon glyphicon-chevron-right color-oficial"></i>&nbsp;Barrio</label>
                        <div class="col-sm-12">
                          <div id="m_divcmbbarrio">
                            <select name="m_cmbbarrio" class="form-control chosen-select" id="m_cmbbarrio">
                                           <option value="0">Seleccione un Barrio</option> 
                            </select><br>
                         </div>
                           <div id="m_complete_barrio" class="rojo hidden"><label><h5><b>Seleccione un Barrio</b></h5></label></div><br>
                     </div>
                     </div> 

                          <div id="divnuevonombre" class="form-group">
                            <label for="title" class="col-sm-12 control-label"><i class="glyphicon glyphicon-chevron-right color-oficial"></i>&nbsp;Nuevo Nombre Barrio</label>
                            <div class="col-sm-12" >
                              <input type="text" name="m_txtbarrio" class="form-control" id="m_txtbarrio" placeholder="Introduce un Barrio">
                              <div id="m_complete_txtbarrio" class="rojo hidden"><label><h5><b>Debe ingresar un Barrio (no números)</b></h5></label></div><br>
                            </div>
                          </div>

                     <div style="padding-left:15px;padding-right:15px">
                       <button style="text-align:center" onclick="abmbarrio('modificar')" id="btn_modificar" name="btn_modificar" type="button" class="pull-right btn btn-success btn-lg"><i class="fa fa-save"></i>&nbsp;&nbsp;Guardar</button>
                     </div>

                   </div>

                   <!---->
                   <div class="row"> 
                    <div class="col-sm-12">
                     <div class="alert alert-success hidden" style="text-align:center" id="msgmodificado"><button class="close" data-dismiss="alert"><span>&times;</span></button>
                       <st><span class="glyphicon glyphicon-saved"></span><b> Estupendo! </b>Barrio Modificado con Éxito</st>
                     </div> 
                     <div class="alert alert-danger hidden" style="text-align:center" id="m_msgerror"><button class="close" data-dismiss="alert"><span>&times;</span></button>
                       <st><span class="glyphicon glyphicon-remove"> </span><b> Error! </b> No se pudo efectuar la modificación.</st>
                     </div>  
                   </div>

                 </div>
                 <!---->
               </div>
<!-- /MODIFICAR BARRIO-->
<!-- ELIMINAR BARRIO -->
                   <div id="divcontenido3" style="padding-left:15px;paddin-right:15px;" class="hidden col-md-12">
                    <div class="panel-body pan">

                      <h2 style="color:#FFF; background-color:#3C3C3C; padding:5px; padding-left:15px; margin-right:5px; margin-left:5px">&nbsp;Eliminar Barrio</h2>
                      <br> 

                                  <div id="e_divformloca" class="form-group">
                                    <label for="title" class="col-sm-12 control-label"><i class="glyphicon glyphicon-chevron-right color-oficial"></i>&nbsp;Localidad</label>
                                    <div class="col-sm-12">
                                      <select name="e_cmbloca" class="form-control chosen-select" id="e_cmbloca">
                                           <option value="0">Seleccione una Localidad</option> 
                                          <?php 

                                          include('conexion/conexion.php');

                                          $conection = mysqli_connect($host, $usuario, $pass,$db) or die("error de conexion a DB");

                                          $select = $conection->query("SELECT idlocalidad,localidad FROM localidades ORDER BY localidad");

                                          while ($result = mysqli_fetch_assoc($select)) 
                                          {
                                              ?>     
                                              <option value="<?php echo $result["idlocalidad"];?>" id="optloca<?php echo $result["idlocalidad"];?>" title="<?php echo $result["localidad"];?>">
                                               <?php echo $result["localidad"] . "</option>";

                                           }
                                           $select->close();
                                           $conection->next_result();

                                           ?>

                                       </select><br><div id="e_complete_loca" class="rojo hidden"><label><h5><b>Seleccione una Localidad</b></h5></label></div><br>
                                   </div>
                               </div>

                      <div id="e_divformbarrio" class="form-group">
                        <label for="title" class="col-sm-12 control-label"><i class="glyphicon glyphicon-chevron-right color-oficial"></i>&nbsp;Barrio</label>
                        <div class="col-sm-12">
                          <div id="e_divcmbbarrio">
                            <select name="e_cmbbarrio" class="form-control chosen-select" id="e_cmbbarrio">
                                           <option value="0">Seleccione un Barrio</option> 
                                       </select><br>
                           </div>            
                          <div id="e_complete_barrio" class="rojo hidden"><label><h5><b>Seleccione un Barrio</b></h5></label></div><br>
                        </div>
                      </div>

                     <div style="padding-left:15px;padding-right:15px;">
                       <button style="text-align:center" onclick="abmbarrio('eliminar')" id="btn_eliminar" name="btn_eliminar" type="button" class="pull-right btn btn-danger btn-lg"><i class="fa fa-remove"></i>&nbsp;&nbsp;Eliminar</button>
                     </div>

                   </div>

                   <!---->
                   <div class="row"> 
                    <div class="col-sm-12">
                     <div class="alert alert-success hidden" style="text-align:center" id="msgeliminado"><button class="close" data-dismiss="alert"><span>&times;</span></button>
                       <st><span class="glyphicon glyphicon-saved"></span><b> Estupendo! </b>Barrio Eliminado con Éxito</st>
                     </div> 
                     <div class="alert alert-danger hidden" style="text-align:center" id="e_msgerror"><button class="close" data-dismiss="alert"><span>&times;</span></button>
                       <st><span class="glyphicon glyphicon-remove"> </span><b> Error! </b> No se pudo eliminar el Barrio.</st>
                     </div>  
                   </div>

                 </div>
                 <!---->
               </div>
<!-- /ELIMINAR BARRIO-->
             </div>

           </div>
         </div>


       </div>
     </div>


   </div>

</div>


</div>
</div>
<!-- /.row -->

</div>
<!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="js/jquery.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>
<script src="js/chosen.jquery.min.js"></script>

   <script>
    function loadingON(){
    $('#loadingDiv').removeClass('hidden');
   }
   function loadingOFF(){
    $('#loadingDiv').addClass('hidden');
    }
   </script>

<script>

        //INICIALIZO LOS CHOSEN SELECT
        function iniciarchosen(){
         $(".chosen-select").chosen({
          no_results_text: "No hay resultados!",
          placeholder_text_single: "Seleccionar",
          placeholder_text_multiple: "Seleccionar"
        });
       }
       iniciarchosen();

//VALIDAR TIPO DE OPERACION
 $('select[id=cmboperacion]').val(0);

      $("select[id=cmboperacion]").change(function(){
        operacion=$('select[id=cmboperacion]').val();
        if (operacion == 0){
        $('#divcontenido1').addClass('hidden');
        $('#divcontenido2').addClass('hidden');
        $('#divcontenido3').addClass('hidden');
        }
        else if (operacion == 'agregar'){
        $('#divcontenido1').removeClass('hidden');
        $('#divcontenido2').addClass('hidden');
        $('#divcontenido3').addClass('hidden');
        $('#a_cmbloca').val(0).trigger('chosen:updated');
        $('#a_txtbarrio').val('');
        $('#msgagregado').addClass('hidden');
        }
        else if (operacion == 'modificar'){
        $('#divcontenido1').addClass('hidden');
        $('#divcontenido2').removeClass('hidden');
        $('#divcontenido3').addClass('hidden'); 
        $('#m_cmbloca').val(0).trigger('chosen:updated');
        $('#m_txtbarrio').val('');
        $('#m_divcmbbarrio').empty();
        $('#m_divcmbbarrio').append('<select id="m_cmbbarrio" name="m_cmbbarrio" class="form-control chosen-select"><option value="0">Seleccione un Barrio</option>');
        iniciarchosen();
        $('#msgmodificado').addClass('hidden');
        }
        else if (operacion == 'eliminar'){
        $('#divcontenido1').addClass('hidden');
        $('#divcontenido2').addClass('hidden');
        $('#divcontenido3').removeClass('hidden');
        $('#e_cmbloca').val(0).trigger('chosen:updated');
        $('#msgeliminado').addClass('hidden');
        $('#e_divcmbbarrio').empty();
        $('#e_divcmbbarrio').append('<select id="e_cmbbarrio" name="e_cmbbarrio" class="form-control chosen-select"><option value="0">Seleccione un Barrio</option>');
        iniciarchosen();
        }
        
      });

//CARGAR BARRIOS DE MODIFICAR A PARTIR DE LOCALIDAD
var m_localidad;

      $('select[id=m_cmbloca]').val(0); //CUANDO CARGA LA PAGINA LE ASIGNO EL VALOR 0 Q ES seleccione una loca

      $("select[id=m_cmbloca]").change(function(){
        m_localidad=$('select[id=m_cmbloca]').val();

        if (m_localidad == 0){
         $('#m_cmbbarrio').empty();
         $('#m_divcmbbarrio').empty();
         $('#m_divcmbbarrio').append('<select id="m_cmbbarrio" name="m_cmbbarrio" class="form-control chosen-select"><option value="0">Seleccione un Barrio</option>');
         iniciarchosen();
        }else{

         $.ajax({
          url: 'ajax/procesarbarrio.ajax.php',
          data: {'localidad':m_localidad},
          type: 'POST',
          dataType: 'json',

                             beforeSend: function () {
                             $('body').addClass('loading'); //Agregamos la clase loading al body
                            },
                    
          success: function( data ) {
            console.log('peticion lograda con exito');
            console.log(data);
          
          $('#m_divcmbbarrio').empty();
          $('#m_divcmbbarrio').append('<select id="m_cmbbarrio" name="m_cmbbarrio" class="form-control chosen-select">');
              
            $('#m_cmbbarrio').append('<option value="0">Seleccione un Barrio</option>');

            for(var i = 1; i < data.length; i++) {
              $("#m_cmbbarrio").append('<option value="'+data[i][1]+'">'+data[i][2]+'</option>');
            }

                $('#m_divcmbbarrio').append('</select><br>');

            iniciarchosen();

               $('body').removeClass('loading'); //Removemos la clase loading  
          },             
          error: function( data ) {
            console.log('se ejecuto mal la peticion');
            console.log(data);

            $('body').removeClass('loading'); //Removemos la clase loading 
          }                         
        })
    }
 
});

//CARGAR BARRIOS DE ELIMINAR A PARTIR DE LOCALIDAD
var e_localidad;

      $('select[id=e_cmbloca]').val(0); //CUANDO CARGA LA PAGINA LE ASIGNO EL VALOR 0 Q ES seleccione una loca

      $("select[id=e_cmbloca]").change(function(){
        e_localidad= $('select[id=e_cmbloca]').val();

        if (e_localidad == 0){
         $('#e_divcmbbarrio').empty();
         $('#e_divcmbbarrio').append('<select id="e_cmbbarrio" name="e_cmbbarrio" class="form-control chosen-select"><option value="0">Seleccione un Barrio</option>');
         iniciarchosen();
        }else{

         $.ajax({
          url: 'ajax/procesarbarrio.ajax.php',
          data: {'localidad':e_localidad},
          type: 'POST',
          dataType: 'json',

                             beforeSend: function () {
                             $('body').addClass('loading'); //Agregamos la clase loading al body
                            },
                    
          success: function( data ) {
            console.log('peticion lograda con exito');
            console.log(data);

          $('#e_divcmbbarrio').empty();
          $('#e_divcmbbarrio').append('<select id="e_cmbbarrio" name="e_cmbbarrio" class="form-control chosen-select">');
                
           // $('#m_cmbbarrio').empty();

            $('#e_cmbbarrio').append('<option value="0">Seleccione un Barrio</option>');

            for(var i = 1; i < data.length; i++) {
              $("#e_cmbbarrio").append('<option value="'+data[i][1]+'">'+data[i][2]+'</option>');
            }

            //   $('#m_divcmbbarrio').append('</select><br><br><div id="m_complete_barrio" class="rojo hidden"><label><h5><b>Seleccione un Barrio</b></h5></label></div>');
                $('#e_divcmbbarrio').append('</select><br>');

            iniciarchosen();

               $('body').removeClass('loading'); //Removemos la clase loading  
          },             
          error: function( data ) {
            console.log('se ejecuto mal la peticion');
            console.log(data);

            $('body').removeClass('loading'); //Removemos la clase loading 
          }                         
        })
    }
 
});

//ENVIAR AJAX DE DE ABM DE BARRIO

function abmbarrio(tipo_abm){
  cmb_agregar_loca = $('#a_cmbloca').val();
  txt_agregar_barr = $('#a_txtbarrio').val();
  cmb_modificar_loca = $('#m_cmbloca').val();
  cmb_modificar_barr = $('#m_cmbbarrio').val();
  txt_modificar_barr = $('#m_txtbarrio').val();
  cmb_eliminar_loca = $('#e_cmbloca').val();
  cmb_eliminar_barr = $('#e_cmbbarrio').val();

//VALIDAR SEGUN TIPO ABM
  if ((tipo_abm == 'agregar' && cmb_agregar_loca != 0 &&  txt_agregar_barr != "" && txt_agregar_barr.length < 151 && isNaN(txt_agregar_barr)) || (tipo_abm == 'modificar' && cmb_modificar_loca != 0 && cmb_modificar_barr != 0 && txt_modificar_barr != "" && isNaN(txt_modificar_barr) && txt_modificar_barr.length < 151) || (tipo_abm == 'eliminar' && cmb_eliminar_loca != 0 && cmb_eliminar_barr != 0)){

    $.ajax({
      url: 'ajax/admin_abm_barrio.ajax.php',
      data: {'tipo':tipo_abm,'loca_a_agregar':cmb_agregar_loca,'barr_a_agregar':txt_agregar_barr,'barr_a_modificar':cmb_modificar_barr,'nuevonombre_barr':txt_modificar_barr,'barr_a_eliminar':cmb_eliminar_barr},
      type: 'POST',
      dataType: 'json',

             beforeSend: function () {
              $('body').addClass('loading'); //Agregamos la clase loading al body
            }, 

      success: function( data ) {
        console.log('peticion lograda con exito');
        console.log(data);

        if (data['resultado'] == 1){
          $('#msgerror').addClass('hidden');
          $('#msgmodificado').addClass('hidden');
          $('#msgeliminado').addClass('hidden');
          $('#msgagregado').removeClass('hidden');
          $('#complete_barrio').addClass('hidden');
          $('#a_complete_loca').addClass('hidden');
          
          $('#a_txtbarrio').val("");
          $('#a_cmbloca').val(0).trigger('chosen:updated');
          $('body').removeClass('loading'); //Removemos la clase loading 
        }else if (data['resultado'] == 2){
          $('#m_msgerror').addClass('hidden');
          $('#msgagregado').addClass('hidden');
          $('#msgeliminado').addClass('hidden');
          $('#msgmodificado').removeClass('hidden');
          $('#m_complete_barrio').addClass('hidden');
          $('#m_complete_txtbarrio').addClass('hidden');
          $('#m_complete_loca').addClass('hidden');
          
          $('#m_txtbarrio').val("");
          $('#m_cmbloca').val(0).trigger('chosen:updated');
          $('#m_divcmbbarrio').empty();
          $('#m_divcmbbarrio').append('<select id="m_cmbbarrio" name="m_cmbbarrio" class="form-control chosen-select"><option value="0">Seleccione un Barrio</option>');
          iniciarchosen();
          $('body').removeClass('loading'); //Removemos la clase loading 
        }else if (data['resultado'] == 3){
          $('#e_msgerror').addClass('hidden');
          $('#msgmodificado').addClass('hidden');
          $('#msgagregado').addClass('hidden');
          $('#msgeliminado').removeClass('hidden');
          $('#e_complete_barrio').addClass('hidden');
          $('#e_complete_loca').addClass('hidden');

          $('#e_cmbloca').val(0).trigger('chosen:updated');
          $('#e_divcmbbarrio').empty();
          $('#e_divcmbbarrio').append('<select id="e_cmbbarrio" name="e_cmbbarrio" class="form-control chosen-select"><option value="0">Seleccione un Barrio</option>');
           iniciarchosen();
          $('body').removeClass('loading'); //Removemos la clase loading 
        }

      },             
      error: function( data ) {
        console.log('se ejecuto mal la peticion');
        console.log(data);

        if (data['resultado'] == 1){
          $('#msgagregado').addClass('hidden');
          $('#msgerror').removeClass('hidden');
          $('#complete_barrio').addClass('hidden');
        }else if (data['resultado'] == 2){
          $('#msgmodificado').addClass('hidden');
          $('#m_msgerror').removeClass('hidden');
          $('#m_complete_barrio').addClass('hidden');
          $('#m_complete_txtbarrio').addClass('hidden');
        }else if (data['resultado'] == 3){
          $('#msgeliminado').addClass('hidden');
          $('#e_msgerror').removeClass('hidden');
          $('#e_complete_barrio').addClass('hidden');
        }

       $('body').removeClass('loading'); //Removemos la clase loading 
      }, 
    })

    }else{

        if (tipo_abm == 'agregar'){
          $('#msgagregado').addClass('hidden');
          $('#msgerror').addClass('hidden');

                  if (txt_agregar_barr == "" || txt_agregar_barr.length > 150 || !isNaN(txt_agregar_barr)){
                    $('#complete_barrio').removeClass('hidden');
                  }else{
                    $('#complete_barrio').addClass('hidden');
                  }

                  if (cmb_agregar_loca == 0){
                    $('#a_complete_loca').removeClass('hidden');
                  }else{
                    $('#a_complete_loca').addClass('hidden');
                  }

        }else if (tipo_abm == 'modificar'){
          $('#msgmodificado').addClass('hidden');
          $('#m_msgerror').addClass('hidden');

                  if (cmb_modificar_barr == 0){
                    $('#m_complete_barrio').removeClass('hidden');
                  }else{
                    $('#m_complete_barrio').addClass('hidden');
                  }

                  if(txt_modificar_barr == "" || !isNaN(txt_modificar_barr) || txt_modificar_barr.length > 150){
                    $('#m_complete_txtbarrio').removeClass('hidden');
                  }else{
                    $('#m_complete_txtbarrio').addClass('hidden');
                  }

                  if (cmb_modificar_loca == 0){
                    $('#m_complete_loca').removeClass('hidden');
                  }else{
                    $('#m_complete_loca').addClass('hidden');
                  }

        }else if (tipo_abm == 'eliminar'){
          $('#msgeliminado').addClass('hidden');
          $('#e_msgerror').addClass('hidden');

                  if (cmb_eliminar_barr == 0){
                    $('#e_complete_barrio').removeClass('hidden');
                  }else{
                    $('#e_complete_barrio').addClass('hidden');
                  }

                  if (cmb_eliminar_loca == 0){
                    $('#e_complete_loca').removeClass('hidden');
                  }else{
                    $('#e_complete_loca').addClass('hidden');
                  }
          }
  }
}

</script>
<script> loadingOFF();</script>
<div class="modalloading"></div>
</body>

</html>

