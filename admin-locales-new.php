
<?php

if(session_id() == '' || !isset($_SESSION)) {
  session_start();
}

if (!(isset($_SESSION['iniciado']))) {
  header ("Location: logeo.php");
  exit();
}

if ($_SESSION['tipo'] != 'A'){
  header ("Location: logeo.php");
  exit();
}


if ($_SESSION['iniciado'] != '5dbc98dcc983a70728bd082d1a47546e'){
  header ("Location: logeo.php");
  exit();

}

?>


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Drinky || Admin - Locales</title>

     <!--LOADING --> 
    <?php include("loading.php"); ?>
    <!--LOADING -->

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
     <link rel="icon" type="image/png" href="images/drinky-logo.png" />
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="css/admin-general.css" rel="stylesheet" type="text/css">
    <link type="text/css" rel="stylesheet" href="css/chosen.min.css">
   

    </head>

    <body>

        <div id="wrapper">

            <?php include('admin-header.php'); ?>

            <div id="page-wrapper">

                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">
                                LOCALES
                            </h1>
                            <ol class="breadcrumb">
                                <li>
                                    <i class="fa fa-dashboard"></i>  <a href="admin-index.php">Tablero</a>
                                </li>
                                <li class="active">
                                    <i class="fa fa-file"></i> Agregar Nuevo Local
                                </li>
                            </ol>

                            <div class="page-content">

                                <div id="tab-general" style="height:100vh;">
                                    <div class="col-lg-1 col-xs-12 col-sm-12"><!--relleno-->
                                    </div>

                                    <div class="col-md-12 col-lg-10 col-xs-12 col-sm-12">
                                        <br>
                                        <div class="panel panel-grey">
                                            <div class="panel-heading">
                                                <h1>Nuevo Local</h1>
                                            </div>
                                            <div class="panel-body pan">
                                                <h2 style="color:#FFF; background-color:#3C3C3C; padding:5px; padding-left:15px; margin-right:5px; margin-left:5px">&nbsp;Datos del Local</h2>
                                                <br>
                                            
                                                <div id="formemail" class="form-group">
                                                    <label for="usuario" class="col-sm-12 control-label"><i class="glyphicon glyphicon-chevron-right color-oficial"></i>&nbsp;Email Usuario</label>
                                                 
                                                      <div class="col-sm-2" >

                                                       <input type="checkbox" name="chkcambiarinput" onclick="cambiarinputusu()" id="chkcambiarinput" class="pull-left" style="width:50px;">Nuevo Usu
                                                     </div> 

                                                     <div class="col-sm-10 hidden" id="divtxtusu">
                                                          <input type="text" name="a_txtusu" class="form-control" id="a_txtusu" placeholder="Introduce Email Usuario">
                                                     </div> 
                                                     <div class="col-sm-10" id="divcombousu">
                                                           <select name="a_cmbusu" class="form-control chosen-select" id="a_cmbusu"></select>
                                                     </div> 

                                                    <div class="col-sm-12 col-sm-offset-2">
                                                     <div id="m_complete_email" class="rojo hidden"><label><h5><b>El email ingresado no es valido. (Sino aparece en la lista de usuarios, es porque todavia ese usuario no ha ingresado a la app).</b></h5></label></div><br>
                                                      </div> 
                                                 
                                                 </div>

                                                  <div id="formnombre" class="form-group">
                                                    <label for="title" class="col-sm-12 control-label"><i class="glyphicon glyphicon-chevron-right color-oficial"></i>&nbsp;Nombre</label>
                                                    <div class="col-sm-12">
                                                      <input type="text" name="a_txtnombre" class="form-control" id="a_txtnombre" placeholder="Introduce Nombre Local">
                                                      <div id="m_complete_nombre" class="rojo hidden"><label><h5><b>Ingrese un Nombre (max 100 digitos)</b></h5></label></div><br>
                                                  </div>
                                              </div>

                                          
                                          <div id="formtelefono" class="form-group">
                                            <label for="title" class="col-sm-12 control-label"><i class="glyphicon glyphicon-chevron-right color-oficial"></i>&nbsp;Teléfono</label>
                                            <div class="col-sm-12">
                                              <input type="text" name="a_txttel" class="form-control" id="a_txttel" placeholder="Introduce Teléfono">
                                              <div id="m_complete_tel" class="rojo hidden"><label><h5><b>Ingrese un teléfono valido (max 15 digitos, sin guiones)</b></h5></label></div><br>
                                          </div>
                                      </div>
                                      <div id="formdire" class="form-group">
                                        <label for="title" class="col-sm-12 control-label"><i class="glyphicon glyphicon-chevron-right color-oficial"></i>&nbsp;Dirección</label>
                                        <div class="col-sm-12">
                                          <input type="text" name="a_txtdire" class="form-control" id="a_txtdire" placeholder="Introduce Dirección">
                                          <div id="m_complete_dire" class="rojo hidden"><label><h5><b>Ingrese una Dirección (max 200 digitos)</b></h5></label></div><br>
                                      </div>
                                  </div>
                                  <div id="formloca" class="form-group">
                                    <label for="title" class="col-sm-12 control-label"><i class="glyphicon glyphicon-chevron-right color-oficial"></i>&nbsp;Localidad</label>
                                    <div class="col-sm-12">
                                      <select name="a_cmbloca" class="form-control chosen-select" id="a_cmbloca">
                                           <option value="0">Seleccione una Localidad</option> 
                                          <?php 

                                          include('conexion/conexion.php');

                                          $conection = mysqli_connect($host, $usuario, $pass,$db) or die("error de conexion a DB");

                                          $select = $conection->query("SELECT idlocalidad,localidad FROM localidades ORDER BY localidad");

                                          while ($result = mysqli_fetch_assoc($select)) 
                                          {
                                              ?>     
                                              <option value="<?php echo $result["idlocalidad"];?>" id="optloca<?php echo $result["idlocalidad"];?>" title="<?php echo $result["localidad"];?>">
                                               <?php echo $result["localidad"] . "</option>";

                                           }
                                           $select->close();
                                           $conection->next_result();

                                           ?>

                                       </select><br><div id="m_complete_loca" class="rojo hidden"><label><h5><b>Seleccione una Localidad</b></h5></label></div><br>
                                   </div>
                               </div>
                               <div id="formbarrio" class="form-group">
                                <label for="title" class="col-sm-12 control-label"><i class="glyphicon glyphicon-chevron-right color-oficial"></i>&nbsp;Barrio</label>
                                <div id="divbarrio" class="col-sm-12">
                                  <select id="a_cmbbarrio" name="a_cmbbarrio" class="form-control chosen-select" >
                                        <option value="0">Seleccione un Barrio</option>
                                    
                                   </select><br>
                                   <div id="m_complete_barrio" class="rojo hidden"><label><h5><b>Seleccione un Barrio</b></h5></label></div><br>
                               </div>
                           </div>

                            <div id="formtipolocal" class="form-group">
                                        <label for="title" class="col-sm-12 control-label"><i class="glyphicon glyphicon-chevron-right color-oficial"></i>&nbsp;Tipo Local</label>
                                        <div class="col-sm-12">
                                          <select class="form-control" name="a_cmbtipolocal" id="a_cmbtipolocal" placeholder="Selecciona un Tipo de Local">
                                            <option value="Bar">Bar</option>
                                            <option value="Boliche">Boliche</option>
                                          </select>
                                          <br>
                                      </div>
                                  </div>

                                  <div class="form-group">

                                   <label for="title" class="col-sm-12 control-label"><i class="glyphicon glyphicon-chevron-right color-oficial"></i>&nbsp;Estado</label>

                                   <div class="row">
                                     <div class="col-sm-12"> 
                                      <div class="checkbox col-md-1 col-sm-1 col-xs-1" style="margin-left:15px">
                                        <input type="checkbox" name="a_chkestado" onclick="validarcheck()" value="N" id="a_chkestado" class="pull-right form-control">

                                      </div> 

                                      <div id="lblactivo">
                                        <label style="padding-top:12px;color:red;"><h4><b>NO ACTIVO</b></h4></label>
                                      </div>

                                    </div>
                                  </div>
                                </div>

                        <!--BOTTON-->

                        <div style="padding-left:15px;padding-right:15px">
                            <button style="margin-top: 20px; text-align:center" id="agregar" name="agregar" type="button" onclick="crearnuevobar()" class="pull-right btn btn-success btn-lg"><i class="fa fa-plus-square"></i>&nbsp;&nbsp;Crear</button>
                        </div>

                        <!--/BOTTON-->
                 </div>
                    
                   <div id="contenido" name="contenido" style="padding:15px;">
                    <!--MENSAJES-->
                        <div class="alert alert-success hidden" style="text-align:center;" id="msgcreado"><button class="close" data-dismiss="alert"><span>&times;</span></button>
                       <st><span class="glyphicon glyphicon-saved"></span><b> Estupendo! </b>Nuevo Local Creado<br></st> 
                        </div>
                         
                           <div class="alert alert-danger hidden" style="text-align:center" id="msgerror"><button class="close" data-dismiss="alert"><span>&times;</span></button>
                               <st><span class="glyphicon glyphicon-remove"> </span><b> Error! </b> No se pudo crear el local ni su directorio de album.</st>
                           </div>
                        <!--/MENSAJES--> 

                        <div class="alert alert-success hidden" style="text-align:center;" id="msgcarpetacreada"><button class="close" data-dismiss="alert"><span>&times;</span></button>
                       <st><span class="glyphicon glyphicon-saved"></span> Carpeta de Album de Fotos del Nuevo Local Creada en el Servidor con el nombre <b id="nombrecarpeta"></b></st> 
                        </div>
                         
                           <div class="alert alert-warning hidden" style="text-align:center" id="msgerrorcarpeta"><button class="close" data-dismiss="alert"><span>&times;</span></button>
                               <st><span class="glyphicon glyphicon-remove"> </span><b> Atención! </b> No se pudo crear el directorio de album de fotos o ya está creado.</st>
                           </div>

                          <div class="alert alert-success hidden" style="text-align:center;" id="msg_album_enbd"><button class="close" data-dismiss="alert"><span>&times;</span></button>
                       <st><span class="glyphicon glyphicon-saved"></span> Nombre de Album de Fotos Guardado en la Base de Datos</st> 
                        </div>
                         
                           <div class="alert alert-warning hidden" style="text-align:center" id="msg_error_albumenbd"><button class="close" data-dismiss="alert"><span>&times;</span></button>
                               <st><span class="glyphicon glyphicon-remove"> </span><b> Atención! </b> No se pudo guardar el nombre del Album en la BD.</st>
                           </div>

                        <div class="alert alert-success hidden" style="text-align:center;" id="msg_reg_cordenada"><button class="close" data-dismiss="alert"><span>&times;</span></button>
                       <st><span class="glyphicon glyphicon-saved"></span> Registro del Nuevo Local Creado en Ubicaciones de Cordenadas</st> 
                        </div>

                           <div class="alert alert-warning hidden" style="text-align:center" id="msg_error_regcordenada"><button class="close" data-dismiss="alert"><span>&times;</span></button>
                               <st><span class="glyphicon glyphicon-remove"> </span><b> Atención! </b> No se crear el Registro del Nuevo Local en las Ubicaciones.</st>
                           </div>

                        <div id="divbotonubi" class="row hidden" style="text-align:center;padding:5px;">
                          
                            <a style="text-align:center;" href="admin-ubicacion-cordenadas.php" class="btn btn-info">Crear Cordenada de Dirección</a>
                        </div>

                  

                       
                   </div>
                   
     
               </div>
           </div>
       </div>


   </div>
</div>




</div>
</div>
<!-- /.row -->

</div>
<!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/chosen.proto.min.js"></script>
<script src="js/chosen.jquery.min.js"></script>

   <script>
    function loadingON(){
    $('#loadingDiv').removeClass('hidden');
   }
   function loadingOFF(){
    $('#loadingDiv').addClass('hidden');
    }
   </script>

<script>

//VALIDAR EMAIL
function validarEmail( email ) {
    expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if ( !expr.test(email) )
       return false;
}

//VALIDAR ESPACIOS EN BLANCO
function validarEspaciosBlanco( campo ){
var espacio_blanco = /[a-z]/i;  //Expresión regular
if(!espacio_blanco.test(campo))
{
alert('Error');
}
}
//CAMBIAR INPUTUSU
function cambiarinputusu(){
 if ($('#chkcambiarinput').prop('checked')){
 $('#divcombousu').addClass('hidden');
 $('#divtxtusu').removeClass('hidden');
 }else{
 $('#divtxtusu').addClass('hidden');
 $('#divcombousu').removeClass('hidden');
 }
}

//VALIDAR CHECK
function validarcheck(){
    if ($('#a_chkestado').prop('checked')){

       $('#lblactivo').empty();
       $('#lblactivo').append('<label style="padding-top:12px;color:green;"><h4><b>ACTIVO</b></h4></label>'); 

       $('#a_chkestado').val('S');
   }else {

       $('#lblactivo').empty();
       $('#lblactivo').append('<label style="padding-top:12px;color:red;"><h4><b>NO ACTIVO</b></h4></label>');  
       $('#a_chkestado').val('N');
   }
}

//TRAER USUARIOS PARA AUTOCOMPLETADO DE CAMPO

        //INICIALIZO LOS CHOSEN SELECT 
        function iniciarchosen(){
             $(".chosen-select").chosen({
                no_results_text: "No hay resultados!",
                placeholder_text_single: "Seleccionar",
                placeholder_text_multiple: "Seleccionar"
        });
        }
        iniciarchosen();
        
        //TRAER USUARIOS
        $( document ).ready(function() {
            $.ajax({
                url     : 'ajax/admin_traerusuarios.ajax.php',
                type    : 'POST',
                dataType: 'json',


      
                success: function( data ) {
                    $("#a_cmbusu").append('<option value="0">Seleccione usuario</option>');
                    for(var i = 0; i < data.length; i++) {
                        $("#a_cmbusu").append('<option value="'+ data[i][0] +'">'+ data[i][0] +'</option>');
                        $('#a_cmbusu').trigger("chosen:updated");
                    }
 
                },
                error: function( data ) {
                    alert("Ocurrio un error al cargar usuarios.\n Por favor recargue la pagina e intente nuevamente!");
                    $('#agregar').prop('disabled', false);

                }
            });
        });
// FIN TRAER USUARIOS PARA AUTOCOMPLETADO DE CAMPO



//CARGAR BARRIOS A PARTIR DE LOCALIDAD
var localidad;

      $('select[id=a_cmbloca]').val(0); //CUANDO CARGA LA PAGINA LE ASIGNO EL VALOR 0 Q ES seleccione una loca

      $("select[id=a_cmbloca]").change(function(){
        localidad=$('select[id=a_cmbloca]').val();

        if (localidad == 0){
         $('#a_cmbbarrio').empty();
          $('#a_cmbbarrio').append('<option value="0">Seleccione un Barrio</option>');
        }else{

         $.ajax({
          url: 'ajax/procesarbarrio.ajax.php',
          data: {'localidad':localidad},
          type: 'POST',
          dataType: 'json',


                             beforeSend: function () {
                             $('body').addClass('loading'); //Agregamos la clase loading al body
                            },
                    
          success: function( data ) {
            console.log('peticion lograda con exito');
            console.log(data);
          $('#formbarrio').removeClass('has-success');
          $('#formbarrio').removeClass('has-error');

          $('#divbarrio').empty();
          $('#divbarrio').append('<select id="a_cmbbarrio" name="a_cmbbarrio" class="form-control chosen-select">');

            $('#a_cmbbarrio').append('<option value="0">Seleccione un Barrio</option>');

            for(var i = 1; i < data.length; i++) {
              $("#a_cmbbarrio").append('<option value="'+data[i][1]+'">'+data[i][2]+'</option>');
            }

               $('#divbarrio').append('</select><br><br><div id="m_complete_barrio" class="rojo hidden"><label><h5><b>Seleccione un Barrio</b></h5></label></div>');

            iniciarchosen();

               $('body').removeClass('loading'); //Removemos la clase loading  
          },             
          error: function( data ) {
            console.log('se ejecuto mal la peticion');
            console.log(data);

            $('body').removeClass('loading'); //Removemos la clase loading 
          }                         
        })
    }
 
});


//CREAR NUEVO LOCAL Y CREAR CARPETA CON SU ID Y VALIDAR CAMPOS

function crearnuevobar(){

  var a_usu
  if ($('#chkcambiarinput').prop('checked')){
  a_usu = $('#a_txtusu').val();
  }else{
  a_usu = $('#a_cmbusu').val();
  }
   
   var a_nom = $('#a_txtnombre').val();
   var a_tel = $('#a_txttel').val();
   var a_dire = $('#a_txtdire').val();
   var a_loca = $('#a_cmbloca').val();
   var a_barrio = $('#a_cmbbarrio').val();
   var a_activo = $('#a_chkestado').val();
   var a_tipolocal = $('#a_cmbtipolocal').val();

if (a_nom != "" && a_nom.length < 101 && a_usu != 0 && a_usu.length < 101 && validarEmail(a_usu) != false && !isNaN(a_tel) && a_tel.length < 16 && a_tel != "" && a_dire.length < 201 && a_dire != "" && a_loca != 0 && a_barrio != 0){
  $('#formnombre').removeClass('has-error');
  $('#formemail').removeClass('has-error');
  $('#formtelefono').removeClass('has-error');
  $('#formdire').removeClass('has-error');
  $('#formloca').removeClass('has-error');
  $('#formbarrio').removeClass('has-error');

  $('#m_complete_nombre').addClass('hidden');
  $('#m_complete_email').addClass('hidden');
  $('#m_complete_tel').addClass('hidden');
  $('#m_complete_dire').addClass('hidden');
  $('#m_complete_loca').addClass('hidden');
  $('#m_complete_barrio').addClass('hidden');

  $('#formnombre').addClass('has-success');
  $('#formemail').addClass('has-success');
  $('#formtelefono').addClass('has-success');
  $('#formdire').addClass('has-success');
  $('#formbarrio').addClass('has-success');
  $('#formloca').addClass('has-success');
  $('#formtipolocal').addClass('has-success');

  $.ajax({
  url: 'ajax/admin_nuevobar.ajax.php',
  data: {'nombar':a_nom,'usubar':a_usu,'telbar':a_tel,'direbar':a_dire,'locabar':a_loca,'barriobar':a_barrio,'estado':a_activo,'tipo_local':a_tipolocal},
  type: 'POST',
  dataType: 'json',

         beforeSend: function () {
         $('body').addClass('loading'); //Agregamos la clase loading al body
        },

  success: function( data ) {
    console.log('peticion lograda con exito');
    console.log(data);

      if (data['creado'] == 1){
      $('#msgerror').addClass('hidden');
      $('#msgcreado').removeClass('hidden');
      $('#divcarpetayubi').removeClass('hidden');
    
      $('#msgcarpetacreada').addClass('hidden');
      $('#msgerrorcarpeta').removeClass('hidden');
      $('#divbotonubi').addClass('hidden');

            $('body').removeClass('loading'); //Removemos la clase loading 
      }
      else if (data['creado'] == 2){

         $('#msgerrorcarpeta').addClass('hidden');
         $('#msgcreado').removeClass('hidden');
         $('#msgcarpetacreada').removeClass('hidden');
         $('#nombrecarpeta').append("Local"+data['idcarpeta']);
         $('#divbotonubi').removeClass('hidden');

         $('#agregar').prop('disabled', true);
         $('#formnombre').prop('disabled', true);
         $('#formemail').prop('disabled', true);
         $('#formtelefono').prop('disabled', true);
         $('#formdire').prop('disabled', true);
         $('#formbarrio').prop('disabled', true);
         $('#formloca').prop('disabled', true);
         $('#formtipolocal').prop('disabled', true); 

          guardarCarpetaenBD(data['idcarpeta']);

      }else{
      $('#msgerror').removeClass('hidden');
      $('#msgcreado').addClass('hidden');
      $('#divcarpetayubi').addClass('hidden');
      $('#agregar').prop('disabled', false);

             $('body').removeClass('loading'); //Removemos la clase loading  
      }

     },             
      error: function( data ) {
      console.log('se ejecuto mal la peticion');
      console.log(data);

      $('#msgerror').removeClass('hidden');
      $('#msgcreado').addClass('hidden');
      $('#divcarpetayubi').addClass('hidden');
      $('#agregar').prop('disabled', false);

      $('body').removeClass('loading'); //Removemos la clase loading  
      }, 
      })

  }else{

    $('#divcarpetayubi').addClass('hidden');
    $('#msgerror').addClass('hidden');
    $('#msgcreado').addClass('hidden');

    if (a_nom == "" || a_nom.length > 100){
      $('#m_complete_nombre').removeClass('hidden');
      $('#formnombre').removeClass('has-success');
      $('#formnombre').addClass('has-error');
    }else{
      $('#m_complete_nombre').addClass('hidden');
      $('#formnombre').removeClass('has-error');
      $('#formnombre').addClass('has-success');
    }

    if (validarEmail(a_usu) ==false || a_usu == "" || a_usu.length > 100){
      $('#m_complete_email').removeClass('hidden');
      $('#formemail').removeClass('has-success');
      $('#formemail').addClass('has-error');
    }else{
      $('#m_complete_email').addClass('hidden');  
      $('#formemail').removeClass('has-error');
      $('#formemail').addClass('has-success');
    }

    if(a_tel == "" || a_tel.length > 15 || isNaN(a_tel)){
      $('#m_complete_tel').removeClass('hidden');
      $('#formtelefono').removeClass('has-success');
      $('#formtelefono').addClass('has-error');
    }else{
      $('#m_complete_tel').addClass('hidden');
      $('#formtelefono').removeClass('has-error');
      $('#formtelefono').addClass('has-success');
    }

    if (a_dire == "" || a_dire.length > 200){
      $('#m_complete_dire').removeClass('hidden');
      $('#formdire').removeClass('has-success');
      $('#formdire').addClass('has-error');
    }else{
      $('#m_complete_dire').addClass('hidden');
      $('#formdire').removeClass('has-error');
      $('#formdire').addClass('has-success');
    }

     if (a_loca != 0){
      $('#formloca').removeClass('has-error');
      $('#formloca').addClass('has-success');
      $('#m_complete_loca').addClass('hidden');
     }else{
      $('#formloca').removeClass('has-success');
      $('#formloca').addClass('has-error');
      $('#m_complete_loca').removeClass('hidden');
     }

     if (a_barrio != 0){
      $('#formbarrio').removeClass('has-error');
      $('#formbarrio').addClass('has-success');
      $('#m_complete_barrio').addClass('hidden');
     }else{
      $('#formbarrio').removeClass('has-success');
      $('#formbarrio').addClass('has-error');
      $('#m_complete_barrio').removeClass('hidden');
     }

  }
  
}


function guardarCarpetaenBD(id_bar_carpeta){

  $.ajax({
  url: 'ajax/admin_updatearnuevacarpeta.ajax.php',
  data: {'id_bar_carpeta':id_bar_carpeta},
  type: 'POST',
  dataType: 'json',

  success: function( data ) {
    console.log('peticion lograda con exito');
    console.log(data);

      if (data['update_carpeta'] == 1){

            $('#msg_error_albumenbd').addClass('hidden');
            $('#msg_album_enbd').removeClass('hidden');
            $('#divbotonubi').removeClass('hidden');

            crearRegistroCordenada(id_bar_carpeta);

      }else{
            $('#msg_album_enbd').addClass('hidden');
            $('#msg_error_albumenbd').removeClass('hidden');
            $('#divbotonubi').addClass('hidden');

            $('body').removeClass('loading'); //Removemos la clase loading  
      }

     },             
      error: function( data ) {
      console.log('se ejecuto mal la peticion');
      console.log(data);
            $('#msg_album_enbd').addClass('hidden');
            $('#msg_error_albumenbd').removeClass('hidden');
            $('#divbotonubi').addClass('hidden');

           $('body').removeClass('loading'); //Removemos la clase loading  
      }, 
      });

}


function crearRegistroCordenada(barid){
$.ajax({
  url: 'ajax/admin_crear_registro_cordenada.ajax.php',
  data: {'id':barid},
  type: 'POST',
  dataType: 'json',

  success: function( data ) {
    console.log('peticion lograda con exito');
    console.log(data);

      if (data['registro_cordenada'] == 1){

      $('#msg_error_regcordenada').addClass('hidden');
      $('#msg_reg_cordenada').removeClass('hidden');

      $('body').removeClass('loading'); //Removemos la clase loading  
      }else{
      $('#msg_reg_cordenada').addClass('hidden');
      $('#msg_error_regcordenada').removeClass('hidden');

      $('body').removeClass('loading'); //Removemos la clase loading  
      }

     },             
      error: function( data ) {
      console.log('se ejecuto mal la peticion');
      console.log(data);

      $('#msg_reg_cordenada').addClass('hidden');
      $('#msg_error_regcordenada').removeClass('hidden');

      $('body').removeClass('loading'); //Removemos la clase loading  

      }, 
      });

}

</script>
<script> loadingOFF();</script>
<div class="modalloading"></div>
</body>

</html>
