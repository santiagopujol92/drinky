
<?php

if(session_id() == '' || !isset($_SESSION)) {
  session_start();
}
if ($_SESSION['estado'] != 'S'){
  header ("Location: logeo.php");
  exit();
}
if (!(isset($_SESSION['iniciado']))) {
  header ("Location: logeo.php");
  exit();
}

if ($_SESSION['iniciado'] != '5dbc98dcc983a70728bd082d1a47546e'){
  header ("Location: logeo.php");
  exit();

}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Drinky || Locales Por Producto</title>
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"> 
  
  <!--LOADING --> 
  <?php include("loading.php"); ?>
  <!--LOADING -->

  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBj-ZDx6dq7cBOnHzFkfrlRZx3S-ICWngI"></script>

  <link rel="stylesheet" href="css/bootstrap.min.css"/> 
  
  <script  src="js/jquery-3.1.0.min.js"></script>
  <script  src="js/bootstrap.min.js"></script>
  <script  src="js/bootstrap-datetimepicker.min.js"></script>

  <link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css"/>
  <link rel="stylesheet" href="css/bootstrap-social.css"/>
  <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css ">
  <link rel="stylesheet" href="css/headeryfooter.css"/>
  <link rel="stylesheet" href="css/listarbares.css"/>
  <link rel="icon" type="image/png" href="images/drinky-logo.png" />
  
  <script>

  function activarMiMenu() {
    $('#menureservas').removeClass('activo');
    $('#menulocales').removeClass('activo');   
    $('#menuinicio').removeClass('activo');
    $('#menucontacto').removeClass('activo');
  }

  window.onload = activarMiMenu;
  </script>

   <script>
    function loadingON(){
    $('#loadingDiv').removeClass('hidden');
   }
   function loadingOFF(){
    $('#loadingDiv').addClass('hidden');
    }
   </script>

</head>
<body>
  <?php include_once("header.php"); ?>
  <br>
  <br>
  <br>
  
 <div class="carousel slide" data-ride="carousel" id="carousel-1"> 

    <!-- INDICADORES -->
    <ol class="carousel-indicators">
      <li data-target="#carousel-1" data-slide-to="0" style="background:#6CDAF8;border:2px solid #6CDAF8" class="active"></li>
      <li data-target="#carousel-1" data-slide-to="1" style="background:#6CDAF8;border:2px solid #6CDAF8"></li> 
      <li data-target="#carousel-1" data-slide-to="2" style="background:#6CDAF8;border:2px solid #6CDAF8"></li> 
    </ol>

    <!-- CONTENEDOR DE LOS SLIDE, SON ITEMS -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="images/tragos.jpg"   class="img-responsive" alt=""><!--IMAGENES GRANDES OPTIMIZADAS NO PESADAS -->
        <div class="carousel-caption hidden-xs hidden-sm"><!-- CON HIDDEN OCULTAMOS EL TEXTOPARA CIERTO TAMAÑO-->
         <div class="centrar"><h1><b>Consulta toda la info de Bares y Boliches en Córdoba</b></h1></div>
       </div>
     </div>

     <div class="item">
      <img src="images/celu.jpg"   class="img-responsive" alt=""><!--IMAGENES GRANDES OPTIMIZADAS NO PESADAS -->
      <div class="carousel-caption hidden-xs hidden-sm"><!-- CON HIDDEN OCULTAMOS EL TEXTOPARA CIERTO TAMAÑO-->
       <div class="centrar"><h1><b>Consulta toda la info de Bares y Boliches en Córdoba</b></h1></div>

     </div>
   </div>

   <div class="item">
    <img src="images/vinos.jpg"   class="img-responsive" alt=""><!--IMAGENES GRANDES OPTIMIZADAS NO PESADAS -->
    <div class="carousel-caption hidden-xs hidden-sm"> <!-- CON HIDDEN OCULTAMOS EL TEXTO PARA CIERTO TAMAÑO-->
     <div class="centrar"><h1><b>Consulta toda la info de Bares y Boliches en Córdoba</b></h1></div>
   </div>
 </div>

 <a href="#carousel-1" style="color:#6CDAF8;" class="left carousel-control" role="button" data-slide="prev">
  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
  <span class="sr-only"></span> <!-- PARA VERLO EN DISPOSITIVOS DE LECTURA -->
</a>

<a href="#carousel-1" style="color:#6CDAF8;" class="right carousel-control" role="button" data-slide="next">
  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
  <span class="sr-only"></span> <!-- PARA VERLO EN DISPOSITIVOS DE LECTURA -->
</a>
</div>
</div>

 <div  id="header" class="withPhoto withApps normal withOptional" style="background-image: url(images/nueva.jpg);background-size:cover">

    <div class="headerOverlay showBackground">

     <div class="container">
      <br>

      <div class="row">
       <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
         <h2><b>Listado de Locales por Producto:</b></h2>
         <div class="panel panel-primary">

           <div class="panel-heading" border="none"></div>

           <table class="table table-responsive">   


  <?php 

  $idtipoprodu= $_POST['cmbtipoprodu'];
  $idprodu = $_POST['cmbprodu'];
  $idloca = $_POST['cmblocalidadprod'];
  $preciomax = $_POST['txtpreciomax'];

  if ($preciomax==0){
    $preciomax = 99999;
  } 

  include('conexion/conexion.php');

  $conection = mysqli_connect($host, $usuario, $pass,$db) or die("error de conexion a DB");


  $select = $conection->query("SELECT DISTINCT(b.idbar),b.nombre_bar,b.direccion,b.telefono,b.album, ba.barrio, l.localidad, b.tipolocal FROM bares b INNER JOIN barrios ba ON b.idbarrio = ba.idbarrio 
   INNER JOIN localidades l ON ba.idlocalidad = l.idlocalidad INNER JOIN cartas c ON c.idbar = b.idbar INNER JOIN productos p ON 
   c.idproducto = p.idproducto  WHERE c.idproducto = '$idprodu' AND c.precio <= '$preciomax' AND b.idlocalidad = '$idloca' AND b.activo = 'S' 
   ORDER BY c.precio asc");

if (mysqli_num_rows($select) == 0){
  echo "<p><b><h4 style='text-align:center;color:#337ab7;'>No se encontraron locales. Por favor vuelva a buscar. <a href='index.php' class='btn btn-info'>Volver</a></h4></b></p>";
}

  while ($result = mysqli_fetch_assoc($select)) 
  {

   
    ?>

      <tr style="width:100%;"> 
        <th class="" style="width:60%;">

        <a href="#infobar" class="modalfotos" data-id="<?php echo $result["idbar"];?>" data-toggle="modal" type="submit">
                  <p>
<!-- TITULO PARA WEB --> <h3 class="hidden-xs hidden-sm" style="text-align:center;padding-right:5%;"><b ><?php echo $result["tipolocal"].' '. $result["nombre_bar"]; ?></b></h3>
<!-- TITULO PARA CELU --><h4 class="hidden-lg hidden-md" style="text-align:center;"><b ><?php echo $result["tipolocal"].' '. $result["nombre_bar"]; ?></b></h4>
                  </p>
<!-- FOTO PARA WEB -->  <img src=<?php echo"images/" . $result["album"] . "/principal.jpg"; ?> alt="" style="width:580px;height:350px;padding:0px;margin-top:0px;margin-left:10px;padding-right:0px;margin-bottom:10px;"  class="hidden-xs hidden-sm img-responsive img-rounded">
<!-- FOTO PARA CELU --> <img src=<?php echo"images/" . $result["album"] . "/principal.jpg"; ?> alt="" style="width:450px;height:200px;padding:0px;"  class="hidden-lg hidden-md img-responsive img-rounded">
        </a>
      </th>

      <!-- COLUMNA PARA WEB -->
      <th style="width:40%;margin-left:0px;text-align:center;padding-left:3%;padding-right:10%;" class="hidden-xs hidden-sm">
        <p style="padding-top:15%;"><h4><u>Localidad:</u></p><p><b style="color:#337ab7;"> <?php echo $result["localidad"]; ?></b></h4></p>
        <p><h4><u>Barrio:</u></p><p><b style="color:#337ab7;"> <?php echo $result["barrio"]; ?> </b></h4></p>
        <p><h4><u>Dirección:</u></p><p><b style="color:#337ab7;"> <?php echo $result["direccion"]; ?> </b></h4></p> 
        <p><h4><u>Teléfono:</u></p><p><b style="color:#337ab7;"> <?php echo $result["telefono"]; ?> </b></h4></p> 
        <input type="hidden" id="<?php echo $result["idbar"];?>"/>
        <a href="#ventanacarta" data-toggle="modal" onclick="abrircartasinreserva()" data-id="<?php echo $result["idbar"];?>" type="submit" class="open-Modal btn btn-lg btn-info " style="margin-top:5px;font-size:20px;"> Ver Carta <span class="glyphicon glyphicon-list-alt"></span></a>
        <a onclick="scrolear_mapa();" href="#map" data-id="<?php echo $result["idbar"];?>" type="submit" class="abrirmapa btn btn-lg btn-warning " style="margin-top:5px;margin-left:15px;font-size:20px;">Ver Mapa <span class="glyphicon glyphicon-map-marker"></span></a>
      </th>
       <!-- FIN COLUMNA PARA WEB-->
      <!-- COLUMNA PARA CELU -->
      <th style="width:40%;" class="hidden-lg hidden-md"><a><h4 style="color:#fff">.</h4></a>
        <p><u>Localidad:</u><b style="color:#337ab7;"> <?php echo $result["localidad"]; ?></b></p>
        <p><u>Barrio:</u><b style="color:#337ab7;"> <?php echo $result["barrio"]; ?></b></p>
        <p><u>Dirección:</u><b style="color:#337ab7;"> <?php echo $result["direccion"]; ?> </b></p> 
        <p><u>Teléfono:</u><b style="color:#337ab7;"> <?php echo $result["telefono"]; ?> </b></p> 
        <input type="hidden" id="<?php echo $result["idbar"];?>"/>
        <a href="#ventanacarta" data-toggle="modal" onclick="abrircartasinreserva()" data-id="<?php echo $result["idbar"];?>" type="submit" class="open-Modal btn btn btn-info" style="margin-top:5px;"> Ver Carta <span class="glyphicon glyphicon-list-alt"></span></a>
        <a onclick="scrolear_mapa();" href="#map" data-id="<?php echo $result["idbar"];?>" type="submit" class="abrirmapa btn btn btn-warning" style="margin-top:5px;"> Ver Mapa <span class="glyphicon glyphicon-map-marker"></span></a>
      </th>
      <!-- FIN COLUMNA PARA CELU-->
    </tr>

    <?php
  }
  $select->close();
  $conection->next_result();

  ?>

  <div class="modal fade" id="infobar">  <!-- fade hace que se ponga oscuro LA PAGINA, EL ID ES IGUAL AL HREF DEL BOTON PARA Q SE RELACIONEN CON EL EVENTO CLICK (EN EL ID VA SIN #) -->
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <a type="button" class="close"  data-dismiss="modal" aria-hidden="true">&times;</a> <!-- TIMES ES LA CRUZECITA, DATA DISMISS=MODAL ES LA FUNCION DE CERRAR -->
          <h4 class="modal-title" id="tituloyfotosbar"><b>Fotos del Local:</b></h4>   
        </div>

        

        <div class="modal-footer">
          <a type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</a>        
        </div>
      </div>
    </div>
  </div> 






  <div class="modal fade" id="ventanacarta">  <!-- fade haACIONEN CON EL EVENTO CLICK (EN EL ID VA SIN #) -->

    <div class="modal-dialog">
      <div class="modal-content">

        <!-- HEADER DE LA VENTANA CABEZA-->
        <div class="modal-header">
          <a type="button" class="close"  data-dismiss="modal" aria-hidden="true">&times;</a> <!-- TIMES ES LA CRUZECITA, DATA DISMISS=MODAL ES LA FUNCION DE CERRAR -->
          <h4 class="modal-title" id="titulobar"><b>Detalles de la Carta:</b></h4>

        </div>

        <!-- CONTENIDO DE LA VENTANA BODY-->
        <div  class="modal-body">
          <div id="contmodal" name="contmodal">
            <p>La carta esta vacía.</p>
          </div>

        </div>

        <!-- FOOTER DE LA VENTANA, PIE -->

        <div class="modal-footer">
          <a  href="#reservar" id"btnreservar" onclick="abrirreserva()" data-toggle="pill" class="btn btn-success">Reservar una mesa</a>
          <a type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</a>

        </div>

        <div id="reservar" class="hidden tab-pane fade">
          <div class="container">
           <div class="row">
            <div class="col-md-6 col-sm-8 col-xs-12">

             <div class="form-group">
              <div class='input-group date'id='datetimepicker1'>
                <span class="input-group-addon"> 
                  <span class="glyphicon glyphicon-calendar"></span>
                </span>
                <input disabled="true" placeholder="Elija Fecha y Hora de Reserva con el Boton" id="fechahora" name="fechahora" type='text' class="form-control"/>

              </div>
            </div>

            <div id="completefecha" class="hidden"><label><h5>Complete la fecha y hora </h5></label></div>

            <div class="form-group">
              <div class="input-group">
               <span class="input-group-addon">Nombre Solicitante</span>
               <input value="<?php echo $_SESSION['nombre'];?>" type="text" class="form-control" disabled>
             </div>
           </div>

           <div class="form-group">
            <div class="input-group">
             <span class="input-group-addon">Email Usuario</span>
             <input value="<?php echo $_SESSION['usuario'];?>" type="text" class="form-control" disabled>
           </div>
         </div>

         <div class="form-group">
          <div class="input-group">
           <span class="input-group-addon">Cantidad Personas:</span>
           <input type="text" id="personas" placeholder="" name="personas" class="form-control">
         </div>
       </div>


       <div id="completecant" class="hidden"><label><h5>Debe ingresar una cantidad numerica de personas</h5></label></div>



       <div class="form-group">
        <div class="input-group">
         <span class="input-group-addon">Teléfono de contacto:</span>
         <input id="telefono" name="telefono"  type="text" class="form-control">
       </div>
     </div>

     <div id="completetel" class="hidden"><label><h5>Debe ingresar un telefono (solo números con limite de 15 digitos)</h5></label></div>


     <div class="form-group">         
      <a type="button" class="btn btn-warning" onclick="cerrarmodalreserva()" id="cerrarreserva"> Cerrar </a> 
      <a type="button" class="btn btn-success pull-right"  id="confirmarreserva"> Confirmar </a>
    </div>


    <div class="alert alert-success hidden" id="msgregistrada"><button class="close" data-dismiss="alert"><span>&times;</span></button>
     <st><span class="glyphicon glyphicon-saved"></span><b> Felicidades! </b>Reserva enviada con exito. Revise en el menu "Mis Reservas" la confirmación. </st>
   </div> 
   <div class="alert alert-danger hidden" id="msgreservado"><button class="close" data-dismiss="alert"><span>&times;</span></button>
     <st><span class="glyphicon glyphicon-info-sign"></span> <b> Atención!</b>  Ya has registrado la reserva en ese horario.</st>
   </div>

 </div>
</div>
</div>
</div>
</table>

</div>

<h4 id="titulomapabar"></h4>
</div>
</div>
</div>

<div class="container">
  <div class="row">
    <div class="col-md-12 col-xs-12 responsive">
      <div id="map" class="hidden"></div><br>
    </div>
  </div>
</div>

</div>
</div>

<?php include_once("footer.php"); ?>

<script  src="js/moment.min.js"></script>

    <script type="text/javascript">//INICIO DTP
    $(function () {
      $('#datetimepicker1').datetimepicker();
      startDate: new Date();
      minDate: moment();

    });
    </script>
    
    <script  src="js/listarbares.js"></script>  

    <script type="text/javascript">
    document.oncontextmenu = function(){return false;}
    </script>
    
<div class="modalloading"></div>
  </body>
  <script> loadingOFF();</script>
  </html>