<?php

 
  if(session_id() == '' || !isset($_SESSION)) {
    session_start();
  }
  
include_once('../conexion/conexion.php');

$periodo = $_POST['tiempo'];

$conection = mysqli_connect($host, $usuario, $pass,$db) or die("error de conexion a DB");
     
      if (mysqli_connect_errno()){
            echo "Error de conexion";        
            exit();  
        }
        mysqli_select_db($conection,$db) or die ("No se encuentra la BD"); 
        mysqli_set_charset($conection,"utf8"); 



if ($periodo == 'Hoy'){
 $query = mysqli_query($conection,"SELECT * FROM usuarios WHERE LEFT(ultimo_ingreso, 10) = LEFT(CURDATE(),10) ORDER BY ultimo_ingreso DESC"); 
}else if ($periodo == 'Ayer'){
  $query = mysqli_query($conection,"SELECT * FROM usuarios WHERE ultimo_ingreso BETWEEN subdate(CURDATE(), 1) and CURDATE() ORDER BY ultimo_ingreso DESC");
}else if ($periodo == 'Semana'){
  $query = mysqli_query($conection,"SELECT * FROM usuarios WHERE ultimo_ingreso BETWEEN subdate(CURDATE(), 7) and CURDATE()  ORDER BY ultimo_ingreso DESC");
}else if ($periodo == 'Quincena'){
  $query = mysqli_query($conection,"SELECT * FROM usuarios WHERE ultimo_ingreso BETWEEN subdate(CURDATE(), 15) and CURDATE() ORDER BY ultimo_ingreso DESC");
}else if ($periodo == 'Mes'){
  $query = mysqli_query($conection,"SELECT * FROM usuarios WHERE ultimo_ingreso BETWEEN subdate(CURDATE(), 30) and CURDATE() ORDER BY ultimo_ingreso DESC");
}else if ($periodo == 'Trimestre'){
  $query = mysqli_query($conection,"SELECT * FROM usuarios WHERE ultimo_ingreso BETWEEN subdate(CURDATE(), 90) and CURDATE() ORDER BY ultimo_ingreso DESC");
}else if ($periodo == 'Semestre' ){
  $query = mysqli_query($conection,"SELECT * FROM usuarios WHERE ultimo_ingreso BETWEEN subdate(CURDATE(), 180) and CURDATE() ORDER BY ultimo_ingreso DESC");
}else if ($periodo == 'Año'){
  $query = mysqli_query($conection,"SELECT * FROM usuarios WHERE ultimo_ingreso = BETWEEN subdate(CURDATE(), 365) and CURDATE() ORDER BY ultimo_ingreso DESC");
}else if ($periodo == 'Origen'){
  $query = mysqli_query($conection,"SELECT * FROM usuarios ORDER BY ultimo_ingreso DESC");
}

   while ($row = mysqli_fetch_array($query)) { 

   $fecha = substr($row['ultimo_ingreso'],0,10);
   $hora = substr($row['ultimo_ingreso'],-8,5);

   $result[] = array(
      0 => $row['email'],
      1 => $row['nomgoogle'],
      2 => $row['nomface'],
      3 => $row['tipo'],
      4 => $row['activo'],
      5 => $fecha,
      6 => $hora,
    );
  }

    echo json_encode($result);

?>