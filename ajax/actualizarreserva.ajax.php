<?php

  if(session_id() == '' || !isset($_SESSION)) {
    session_start();
  }
  
include_once('../conexion/conexion.php');
	

$miusu = $_SESSION['usuario']; 

$conection = mysqli_connect($host, $usuario, $pass,$db) or die("error de conexion a DB");
     
      if (mysqli_connect_errno()){
            echo "Error de conexion";        
            exit();  
        }
        mysqli_select_db($conection,$db) or die ("No se encuentra la BD"); 
        mysqli_set_charset($conection,"utf8"); 
  

  $arr = array();

  $query = mysqli_query($conection,"SELECT b.nombre_bar,b.telefono,b.direccion, r.*,b.tipolocal FROM reservas r INNER JOIN bares b ON b.idbar = r.idbar WHERE r.email_usu = '$miusu' ORDER BY r.fecha DESC,r.estado");


  while ($result = mysqli_fetch_array($query)) 
                      {
                           $fecha = substr($result["fecha"],0,10);
                           $hora = substr($result["fecha"],-8,5);
                           $claseestado;

                           if($result['estado'] == "Pendiente"){
                            $claseestado = "enespera";
                           }else if ($result['estado'] == "Confirmado"){
                            $claseestado = "confirmado";
                           }else if ($result['estado'] == "Rechazado"){
                            $claseestado = "rechazado";
                           }
  
                           $arr[] = array(
                            0 => $result["nombre_bar"],
                            1 => $fecha,
                            2 => $hora." hs",
                            3 => $result['telefono'],
                            4 => $result['direccion'],
                            5 => $result['cantidad_pers'],
                            6 => $result['estado'],
                            7 => $claseestado,
                            8 => $result['idbar'],
                            9 => $result['email_usu'],
                            10 => $result['fecha'],
                            11 => $result['tipolocal'],
                            );
                          }

    echo json_encode($arr);
    ?>