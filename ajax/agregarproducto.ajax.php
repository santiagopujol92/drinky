
<?php

  if(session_id() == '' || !isset($_SESSION)) {
    session_start();
  }
  
include_once('../conexion/conexion.php');
	

$cmbidtipoprodu = $_POST['tipoprodu']; 
$cmbidprodu = $_POST['produ']; 
$txt_precio = $_POST['pre']; 
$id_bar = $_POST['barid']; 

$conection = mysqli_connect($host, $usuario, $pass,$db) or die("error de conexion a DB");
     
      if (mysqli_connect_errno()){
            echo "Error de conexion";        
            exit();  
        }
        mysqli_select_db($conection,$db) or die ("No se encuentra la BD"); 
        mysqli_set_charset($conection,"utf8"); 

$mensaje = "";
$existente;


   $sql="INSERT INTO cartas (idbar,idproducto,precio) VALUES (?,?,?)"; 
       $resultado=  mysqli_prepare($conection, $sql);
       $ok=  mysqli_stmt_bind_param($resultado,"iid",$id_bar,$cmbidprodu,$txt_precio); 

       $ok= mysqli_stmt_execute($resultado); 
       
        if ($ok==false)
        {   
          $mensaje = "El producto ya existe en la carta"; 
          $existente = 1;
        }
        else
        {
        $mensaje = "Producto agregado con exito";
        $existente = 0;
        }
        
        mysqli_stmt_close($resultado); 

        $arr = array('mensaje' => $mensaje, 'existente' => $existente);
        echo json_encode($arr);
?>