<?php

  if(session_id() == '' || !isset($_SESSION)) {
    session_start();
  }

include_once('../conexion/conexion.php');
	$conection = mysqli_connect($host, $usuario, $pass,$db) or die("error de conexion a DB");

 
 $usu = $_POST["datousu"];
 $nomusu = mysqli_real_escape_string($conection,$_POST["datonomusu"]);
 $dniusu = (int)$_POST["datodni"];
 $nombar = mysqli_real_escape_string($conection,$_POST["datonombar"]);
 $locabar = mysqli_real_escape_string($conection,$_POST["datolocabar"]);
 $barriobar = mysqli_real_escape_string($conection,$_POST["datobarriobar"]);
 $direbar = mysqli_real_escape_string($conection,$_POST["datodirebar"]);
 $tel = (int)$_POST["datotel"];
 $tipo = $_POST["datotipo"];
 $fechahoy = date("Y-m-d H:i:s");
 $tipo_soli = "Alta";
 $estado = "Pendiente";
 

     if (mysqli_connect_errno()){
            echo "Error de conexion";        
            exit();  
        }

            mysqli_select_db($conection,$db) or die ("No se encuentra la BD"); 
        mysqli_set_charset($conection,"utf8"); 

  $registrado;

   $sql="INSERT INTO solicitudes_pendientes (email_usu,nombre_usu,dni,nombre_bar,domicilio_bar,localidad,barrio,telefono,tipo_local,fecha,tipo_solicitud,estado) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)"; 
       $resultado=  mysqli_prepare($conection, $sql);
       $ok=  mysqli_stmt_bind_param($resultado,"ssissssissss",$usu,$nomusu,$dniusu,$nombar,$locabar,$barriobar,$direbar,$tel,$tipo,$fechahoy,$tipo_soli,$estado); 

       $ok= mysqli_stmt_execute($resultado); 
       
        if ($ok==false)
        {   
          $mensaje = "No se pudo registrar la solicitud"; 
          $registrado = 0;
        }
        else
        {
        $mensaje = "Solicitud registrada con exito";
        $registrado = 1;
        }
        
        mysqli_stmt_close($resultado); 

        $arr = array('mensaje' => $mensaje, 'registrado' => $registrado);
        echo json_encode($arr);
    
    ?>