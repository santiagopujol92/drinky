<?php

  if(session_id() == '' || !isset($_SESSION)) {
    session_start();
  }
  
include_once('../conexion/conexion.php');
	

$id_bar = $_POST['barid']; 


$conection = mysqli_connect($host, $usuario, $pass,$db) or die("error de conexion a DB");
     
      if (mysqli_connect_errno()){
            echo "Error de conexion";        
            exit();  
        }
        mysqli_select_db($conection,$db) or die ("No se encuentra la BD"); 
        mysqli_set_charset($conection,"utf8"); 

  $result = array();
  $result[] = array(
        0 => "Id",
        1 => "Nombre",
        2 => "Teléfono",
        3 => "UserEmail",
        4 => "Dirección",
        5 => "Barrio",
        6 => "Localidad",
        7 => "CarpetaFotos",
        8 => "Activo",
        9 => "Tipo Local"
      );  

if ($id_bar == "Todos"){
  $query = mysqli_query($conection,"SELECT b.*,ba.barrio,lo.localidad FROM bares b inner join barrios ba on b.idbarrio = ba.idbarrio inner join localidades lo on lo.idlocalidad = b.idlocalidad ORDER BY b.activo desc,b.tipolocal, b.nombre_bar");
}else if ($id_bar == "Boliches") {
   $query = mysqli_query($conection,"SELECT b.*,ba.barrio,lo.localidad FROM bares b inner join barrios ba on b.idbarrio = ba.idbarrio inner join localidades lo on lo.idlocalidad = b.idlocalidad WHERE b.tipolocal = 'Boliche' ORDER BY b.activo desc,b.tipolocal, b.nombre_bar"); 
}else if ($id_bar == "Bares"){
  $query = mysqli_query($conection,"SELECT b.*,ba.barrio,lo.localidad FROM bares b inner join barrios ba on b.idbarrio = ba.idbarrio inner join localidades lo on lo.idlocalidad = b.idlocalidad WHERE b.tipolocal = 'Bar' ORDER BY b.activo desc,b.tipolocal, b.nombre_bar");
}else{
  $query = mysqli_query($conection,"SELECT b.*,ba.barrio,lo.localidad FROM bares b inner join barrios ba on b.idbarrio = ba.idbarrio inner join localidades lo on lo.idlocalidad = b.idlocalidad where b.idbar = '$id_bar'");
}



   while ($row = mysqli_fetch_array($query)) { 
    $result[] = array(
      0 => $row['idbar'],
      1 => $row['nombre_bar'],
      2 => $row['telefono'],
      3 => $row['email_usu'],
      4 => $row['direccion'],
      5 => $row['barrio'],
      6 => $row['localidad'],
      7 => $row['album'],
      8 => $row['activo'],
      9 => $row['tipolocal'],
    );
  }

  echo json_encode($result);
?>