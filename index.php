
<?php

if(session_id() == '' || !isset($_SESSION)) {
  session_start();
}

if ($_SESSION['estado'] != 'S'){
  header ("Location: logeo.php");
  exit();
}

if (!(isset($_SESSION['iniciado']))) {
  header ("Location: logeo.php");
  exit();
}

if ($_SESSION['iniciado'] != '5dbc98dcc983a70728bd082d1a47546e'){
  header ("Location: logeo.php");
  exit();
}



?>



<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Drinky || Inicio</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"> 
	
  <!--LOADING -->
  <?php include("loading.php"); ?>
  <!--LOADING -->

  <link rel="stylesheet" href="css/bootstrap.min.css"/> 
  <link rel="stylesheet" href="css/bootstrap-social.css"/>
  <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css ">
  <link rel="icon" type="image/png" href="images/drinky-logo.png" />

  <link rel="stylesheet" href = "css/index.css"/>
  <link rel="stylesheet" href = "css/headeryfooter.css"/>

  <script  src="js/jquery-3.1.0.min.js"></script>
  <script  src="js/bootstrap.min.js"></script>

  <script>
  function activarMiMenu() {
    $('#menureservas').removeClass('activo');
    $('#menulocales').removeClass('activo');  
    $('#menucontacto').removeClass('activo');
    $('#menuinicio').addClass('activo');

  }
  window.onload = activarMiMenu;

  function loadingON(){
    $('#loadingDiv').removeClass('hidden');
  }
  function loadingOFF(){
    $('#loadingDiv').addClass('hidden');
  }
  </script>

</head>

<body >

 <?php include_once("header.php"); ?>

  <div class="carousel slide" data-ride="carousel" id="carousel-1"> 

    <!-- INDICADORES -->
    <ol class="carousel-indicators">
      <li data-target="#carousel-1" data-slide-to="0" style="background:#6CDAF8;border:2px solid #6CDAF8" class="active"></li>
      <li data-target="#carousel-1" data-slide-to="1" style="background:#6CDAF8;border:2px solid #6CDAF8"></li> 
      <li data-target="#carousel-1" data-slide-to="2" style="background:#6CDAF8;border:2px solid #6CDAF8"></li> 
    </ol>

    <!-- CONTENEDOR DE LOS SLIDE, SON ITEMS -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="images/tragos.jpg"    class="img-responsive" alt=""><!--IMAGENES GRANDES OPTIMIZADAS NO PESADAS -->
        <div class="carousel-caption hidden-xs hidden-sm"><!-- CON HIDDEN OCULTAMOS EL TEXTOPARA CIERTO TAMAÑO-->
         <div class="centrar"><h1><b>Consulta toda la info de Bares y Boliches en Córdoba</b></h1></div>
       </div>
     </div>

     <div class="item">
      <img src="images/celu.jpg"   class="img-responsive" alt=""><!--IMAGENES GRANDES OPTIMIZADAS NO PESADAS -->
      <div class="carousel-caption hidden-xs hidden-sm"><!-- CON HIDDEN OCULTAMOS EL TEXTOPARA CIERTO TAMAÑO-->
       <div class="centrar"><h1><b>Consulta toda la info de Bares y Boliches en Córdoba</b></h1></div>

     </div>
   </div>

   <div class="item">
    <img src="images/vinos.jpg"   class="img-responsive" alt=""><!--IMAGENES GRANDES OPTIMIZADAS NO PESADAS -->
    <div class="carousel-caption hidden-xs hidden-sm"> <!-- CON HIDDEN OCULTAMOS EL TEXTO PARA CIERTO TAMAÑO-->
     <div class="centrar"><h1><b>Consulta toda la info de Bares y Boliches en Córdoba</b></h1></div>
   </div>
 </div>

 <a href="#carousel-1" style="color:#6CDAF8;" class="left carousel-control" role="button" data-slide="prev">
  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
  <span class="sr-only"></span> <!-- PARA VERLO EN DISPOSITIVOS DE LECTURA -->
</a>

<a href="#carousel-1" style="color:#6CDAF8;" class="right carousel-control" role="button" data-slide="next">
  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
  <span class="sr-only"></span> <!-- PARA VERLO EN DISPOSITIVOS DE LECTURA -->
</a>
</div>
</div>

<div  id="header" class="withPhoto withApps normal withOptional" style="background-image: url(images/nueva.jpg);background-size:cover">

<div class="headerOverlay showBackground">

<div class="centrar hidden-md hidden-lg"><h2 style="color:#fff;margin-top:0%;"><br><b>Consulta toda la info de Bares y Boliches en Córdoba</b></h2></div>
  <div class="centrar gris hidden"><h4><u>Seleccione lugar que desea consultar informacion:</u></h4></div>

  <div class="container" style="min-height:471px;">
    <div class="row pintar">
     <div class="col-md-12 centrar">
       <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">

       </div> 
       <div class="tab-content tab-content col-md-12 col-xs-12 col-sm-12 centrar">
        <br>
        <br>
        <div id="Bar" class=" ">
          <div class="row pintar"> 
            <img class="img img-responsive" style="width:50px;" src="images/buscar.png"></img>
            <div class=" centrar gris "><h4><u>Seleccione una opción de búsqueda:</u></h4><br></div>

            <ul class="nav nav-pills nav-justified">

              <li ><a data-toggle="pill" class="btn btn-success zoomIt" href="#NomBar" onclick="scrolear_tab();"><img src="images/nombre.png" width="50" class="img img-responsive"></img><h4 style="padding:0px;margin:0px;"><b>Nombre</b></h4></a></li>
              <li><a data-toggle="pill" class="btn btn-danger zoomIt"  href="#UbiBar" onclick="scrolear_tab();"><img src="images/ubicacion.png" width="50" class="img img-responsive"></img><h4 style="padding:0px;margin:0px;"><b>Ubicación</b></h4></a></li>
              <li><a data-toggle="pill" class="btn btn-warning zoomIt"  href="#BebiBar" onclick="scrolear_tab();"><img src="images/trago.png" width="50" class="img img-responsive"></img><h4 style="padding:0px;margin:0px;"><b>Producto</b></h4></a></li>

            </ul>

            <div id="tabs" class="tab-content">
              <br>
              <br>
             

              <div id="NomBar" class="tab-pane fade" >
               <form action="barespornombre.php" method="post" id="FrmBarNom" name="FrmBarNom" class="form-horizontal">  <!-- formulario en linea -->

                 <fieldset class="row">  
                   <br>
                   <label class="text_fixed"><h4><u>Buscá Bares o Boliches por Nombre:</u></h4></label>
                   <br>
                   <table>
                    <tr>
                      <th>
                       <span class="texto">Tipo de Local:</span>
                     </th>
                     <th>
                      <select class="form-control" name="cmbtipolocal" id="cmbtipolocal" placeholder="Selecciona un Tipo de Local">
                        <option value="Todos los locales">Todos los locales</option>
                        <option value="Bar">Bar</option>
                        <option value="Boliche">Boliche</option>
                      </select>
                    </th>
                  </tr>
                  <tr>
                    <th>
                      <span for="nombre" class="texto ">Nombre Local:</span>
                    </th>
                    <th>
                      <input  type="text" name="nombrebar" id="nombrebar" class="form-control"  placeholder="Escribe nombre o parte">
                    </th>
                  </tr>
                  <tr>
                   <th></th>
                   <th>
                    <input  type="submit" name="buscarpornom" id="buscarpornom" class="btn btn-success " value="Buscar"></input>
                  </th>
                </tr>  
              </table> 
            </fieldset>
            
          </form>
        </div>


        <div id="UbiBar" class="tab-pane fade">
          <form id="FrmBarubi" name="FrmBarubi" action="barporubi.php" method="post">
            <fieldset class="row">  
             <br>
             <label class="text_fixed"><h4><u>Buscá Bares o Boliches por localidad, barrio y dirección:</u></h4></label>
             <br>
             <table>
              <tr>
                <th>
                 <span class="texto">Tipo de Local:</span>
               </th>
               <th>
                <select class="form-control" name="u_cmbtipolocal" id="u_cmbtipolocal" placeholder="Selecciona un Tipo de Local">
                  <option value="Todos los locales">Todos los locales</option>
                  <option value="Bar">Bar</option>
                  <option value="Boliche">Boliche</option>
                </select>
              </th>
            </tr>
            <tr>
              <th>
                <span class="texto">Localidad:</span>
              </th>
              <th>
                <select class="form-control" name="cmblocalidad" id="cmblocalidad" placeholder="Selecciona una Localidad">
                 <option value="0">Seleccionar Una localidad</option>
                 <?php 

                 include('conexion/conexion.php');

                 $conection = mysqli_connect($host, $usuario, $pass,$db) or die("error de conexion a DB");

                 $select = $conection->query("SELECT * FROM localidades ORDER BY localidad");

                 while ($result = mysqli_fetch_assoc($select)) 
                 {
                  ?>     
                  <option value="<?php echo $result["idlocalidad"];?>">
                   <?php echo $result["localidad"] . "</option>";


                 }
                 $select->close();
                 $conection->next_result();

                 ?>
               </select>
             </th>
           </tr>
           <tr id="filaubibarrio" class="hidden">
            <th>
              <span id="lblbarrio" class="texto hidden">Barrio:</span>
            </th>
            <th>
              <select class="form-control hidden"  name="cmbbarrio" id="cmbbarrio" placeholder="Selecciona un barrio">
              </select>
            </th>
          </tr>
          <tr id="filaubidire" class="hidden">
            <th>
              <span id="lbldireccion" class="texto hidden">Dirección:</span>
            </th>
            <th>
              <input  type="text" name="txtdireccion" id="txtdireccion" class="form-control hidden"  id="direbar" placeholder="(opcional)">
            </th>
          </tr>
          <tr id="filaubiboton" class="hidden">
           <th></th>
           <th>
            <input  type="submit" name="buscarporubi" id="buscarporubi" class="btn btn-success hidden" value="Buscar"></input>
          </th>
        </tr>  
      </table> 
    </fieldset>
  </form> 
</div>

<div id="BebiBar" class="tab-pane fade">
  <form id="FrmBarProdu" name="FrmBarProdu" onsubmit="return validarprecio();" action="barporproducto.php" method="post">
    <fieldset class="row">  
     <br>
     <label class="text_fixed"><h4><u>Buscá Bares o Boliches por Comida, Bebida o Promociones:</u></h4></label>
     <br>
     <table>
      <tr>
        <th>
          <span class="texto" >Tipo de Producto:</span>
        </th>
        <th>
          <select class="form-control" name="cmbtipoprodu" id="cmbtipoprodu" placeholder="Selecciona un Tipo de Producto">
           <option value="0">Selecciona un Tipo de Producto</option>
           <option value="1">Bebidas</option>
           <option value="2">Comidas</option>
           <option value="3">Promociones</option>

         </select>
       </th>
     </tr>
     <tr id="filaprodu" class="hidden">
      <th>
        <span id="lblprodu" class="texto hidden">Producto:</span>
        <div id="completeproducto" class="hidden"><label><h5>Seleccione un Producto</h5></label></div>
      </th>
      <th>
        <select class="form-control hidden" name="cmbprodu" id="cmbprodu" placeholder="Selecciona un Producto">
        </select>
      </th>
    </tr>

    <tr id="filaloca" class="hidden">
      <th>
        <span class="texto hidden" id="lbllocalidad">Localidad:</span>
      </th>
      <th>
        <select class="form-control hidden" name="cmblocalidadprod" id="cmblocalidadprod" placeholder="Opcional">
         <option value="">Seleccionar Una localidad</option>
       </select>
     </th>
   </tr>

   <tr id="filaprecio" class="hidden">
    <th>
      <span id="lblpreciomax" class="texto hidden">Precios menor a:</span>
      <div id="completeprecio" class="hidden"><label><h5>Debe ingresar un precio numerico entero</h5></label></div>
    </th>

    <th>
      <input  type="text" name="txtpreciomax" id="txtpreciomax" class="form-control hidden "  id="" placeholder="(opcional)">
    </th>
  </tr>
  <tr id="filaboton" class="hidden">
   <th></th>
   <th>
    <input  type="submit" name="buscarporprodu" id="buscarporprodu" class="btn btn-success hidden" value="Buscar"></input>
  </th>
</tr> 

</table> 
</fieldset>
</form> 
</div>

</div>

</div>

</div>
</div>


</div>
</div>
<br>
<br>
<br>
<div class="row ">
 <div class="col-md-12 centrar ">
  <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
    <ul class="nav nav-pills nav-justified header-text ">   
      <h4 class="hidden-xs hidden-sm"><span>Si eres un propietario de un Local y quieres</span></h4><br>
      <h4 class="hidden-xs hidden-sm"><span>aparecer en nuestras busquedas, clickee el boton siguiente:</span></h4><br>
      <a  class="btn btn-info active" data-toggle="modal" type="submit" id="Milocal" href="#SolicitudLocal"><h4><b>Solicitar Registro de Mi Local</b></h4></a>
    </ul>
  </div>
</div>
</div>

<div class="modal fade" id="SolicitudLocal">

  <div class="modal-dialog">
    <div class="modal-content">


      <div class="modal-header">
        <a type="button" class="close"  data-dismiss="modal" aria-hidden="true">&times;</a> 
        <h4 class="modal-title" id="titulosolicitud"><b>Solicitud de Registro de Local:</b></h4> 
      </div>


      <div class="modal-body">

        <p><h5>Complete la solicitud con los datos exactos de su local, al enviarla en el transcurso de 24 hs nos pondremos en contacto con usted via telefonica y email para continuar con el proceso.</h5></p><br>

        <form action="" method="post" name"FrmSolicitud" class="form-horizontal">  <!-- formulario en linea -->

          <fieldset> 

           <div class="form-group has-primary">
            <label for="dni" class="control-label col-md-2">Usuario:</label>
            <div class="col-md-10">
              <input type="text" value="<?php echo $_SESSION['usuario'];?>" disabled class="form-control" name="S_usuario"id="S_usuario" >
            </div>  
          </div>

          <div class="form-group has-primary">
            <label for="nombre" class="control-label col-md-2">Nombre:</label>
            <div class="col-md-10">
              <input type="text" value="<?php echo $_SESSION['nombre'];?>" class="form-control" id="S_nombreusu" name ="S_nombreusu" placeholder="Introduce tu Nombre y Apellido">
              <div id="complete_nombre" class="validar hidden"><label><h5>Debe ingresar su nombre completo (sin numeros).</h5></label></div>
            </div>  
          </div>

          <div class="form-group has-primary">
            <label for="dni" class="control-label col-md-2">Dni:</label>
            <div class="col-md-10">
              <input type="text" class="form-control" name="S_dni"id="S_dni" placeholder="Introduce un Dni">
              <div id="complete_dni" class="validar hidden"><label><h5>Debe ingresar su dni (sólo números, max 8 digitos).</h5></label></div>
            </div>  
          </div>

          <div class="form-group has-primary">
            <label for="fecha" class="control-label col-md-2">Local:</label>
            <div class="col-md-10">
              <input type="text" class="form-control" name="S_nombrebar"id="S_nombrebar" placeholder="Introduce el Nombre de tu Local">
              <div id="complete_bar" class="validar hidden"><label><h5>Debe ingresar el nombre de su Local.</h5></label></div>
            </div>  
          </div>

          <div class="form-group has-primary">
            <label for="fecha" class="control-label col-md-2">Localidad:</label>
            <div class="col-md-10">
              <input type="text" class="form-control" name="S_localidad"id="S_localidad" placeholder="Introduce Localidad del Bar">
              <div id="complete_localidad" class="validar hidden"><label><h5>Debe ingresar la localidad del Local (no numeros).</h5></label></div>
            </div>  
          </div>

          <div class="form-group has-primary">
            <label for="fecha" class="control-label col-md-2">Barrio:</label>
            <div class="col-md-10">
              <input type="text" class="form-control" name="S_barrio"id="S_barrio" placeholder="Introduce Barrio del Bar">
              <div id="complete_barrio" class="validar hidden"><label><h5>Debe ingresar el barrio del Local (no numeros).</h5></label></div>
            </div>  
          </div>

          <div class="form-group has-primary">
            <label for="tel" class="control-label col-md-2">Dirección:</label>
            <div class="col-md-10">
              <input type="text" class="form-control" name="S_direccion" id="S_direccion" placeholder="Introduce la Direccion del Bar">
              <div id="complete_direccion" class="validar hidden"><label><h5>Debe ingresar la direccion del Local.</h5></label></div>
            </div>  
          </div>

          <div class="form-group has-primary">
            <label for="tel" class="control-label col-md-2">Teléfono:</label>
            <div class="col-md-10">
              <input type="text" class="form-control" name="S_tel" id="S_tel" placeholder="Introduce un Teléfono">
              <div id="complete_telefono" class="validar hidden"><label><h5>Debe ingresar un telefono numerico (maximo 15 digitos).</h5></label></div>
            </div>  
          </div>

          <div class="form-group has-primary">
            <label for="tipo" class="control-label col-md-2">Local:</label>
            <div class="col-md-10">
              <select class="form-control" name="S_cmbtipo" id="S_cmbtipo" placeholder="Selecciona un Tipo Dni">
                <option value="Bar">Bar</option>
                <option value="Boliche">Boliche</option>
              </select>
            </div>  
          </div>
          <div class="form-group">         
            <a type="button" class="btn btn-danger" onclick="ocultarmensajes_modal_solicitud()" data-dismiss="modal" id="cerrarsoli"> Cerrar </a> 
            <a type="button" class="btn btn-success pull-right" onclick="enviarsolicitud()" id="btnenviarsoli" name="btnenviarsoli"> Enviar Solicitud </a>
          </div>

          <div class="alert alert-success hidden" id="msg_soli_registrada"><button class="close" data-dismiss="alert"><span>&times;</span></button>
           <st>Felicidades!</st> Solicitud enviada con éxito. En el transcurso de las siguientes 24 hs será contactado por nosotros.
         </div> 
         <div class="alert alert-danger hidden" id="msg_soli_error"><button class="close" data-dismiss="alert"><span>&times;</span></button>
           <st>Atención!</st> Error al enviar la solicitud. Contactar Administradores.
         </div> 
       </fieldset> 
     </form>

   </div>
 </div>

</div>
</div>
</div>
<br>


</div>
</div>


<?php include_once("footer.php"); ?>

<script>//IR A UN ELEMENTO CON SCROLL
function scrolear_tab(){
  var dest = $('#tabs').offset().top;
  $("html, body").animate({scrollTop: dest});
}
</script>

<script>

// CARGAR COMBO BARRIO SOEGUN LOCALIDAD

var localidad;

$(document).ready(function(){

      $('select[id=cmblocalidad]').val(0); //CUANDO CARGA LA PAGINA LE ASIGNO EL VALOR 0 Q ES seleccione una loca

      $("select[id=cmblocalidad]").change(function(){
        localidad=$('select[id=cmblocalidad]').val();

        if (localidad == 0){
          $('#filaubibarrio').addClass('hidden');
          $('#filaubidire').addClass('hidden');
          $('#filaubiboton').addClass('hidden');
          $('#buscarporubi').addClass('hidden');
          $('#lblbarrio').addClass('hidden');
          $('#cmbbarrio').addClass('hidden');
          $('#txtdireccion').addClass('hidden');
          $('#lbldireccion').addClass('hidden');
        }else{

         $.ajax({
          url: 'ajax/procesarbarrio.ajax.php',
          data: {'localidad':localidad},
          type: 'POST',
          dataType: 'json',

          beforeSend: function () {
             $('body').addClass('loading'); //Agregamos la clase loading al body
           },

           success: function( data ) {
            console.log('peticion lograda con exito');
            console.log(data);

            $('#filaubibarrio').removeClass('hidden');
            $('#filaubidire').removeClass('hidden');
            $('#filaubiboton').removeClass('hidden');
            $('#buscarporubi').removeClass('hidden');
            $('#cmbbarrio').empty();
            $('#lblbarrio').removeClass('hidden');
            $('#cmbbarrio').removeClass('hidden');

            $('#cmbbarrio').append('<option value="0">Todos los Barrios</option>');

            for(var i = 1; i < data.length; i++) {
              $("#cmbbarrio").append('<option value="'+data[i][1]+'">'+data[i][2]+'</option>');
            }

            $('#lbldireccion').removeClass('hidden');      
            $('#txtdireccion').removeClass('hidden');

             $('body').removeClass('loading'); //Removemos la clase loading

           },             
           error: function( data ) {
            console.log('se ejecuto mal la peticion');
            console.log(data);
            $('#filaubidire').addClass('hidden');
            $('#filaubiboton').addClass('hidden');
            $('#buscarporubi').addClass('hidden');
            $('#lblbarrio').addClass('hidden');
            $('#cmbbarrio').addClass('hidden');
            $('#txtdireccion').addClass('hidden');
            $('#lbldireccion').addClass('hidden');

            $('body').removeClass('loading'); //Removemos la clase loading
          }                         
        })
}

});
});

//CARGAR COMBO PRODUCTO A PARTIR DE TIPOPRODU

var tipoprodu;


$(document).ready(function(){
  $('select[id=cmbtipoprodu]').val(0); 

  $("select[id=cmbtipoprodu]").change(function(){
    tipoprodu=$('select[id=cmbtipoprodu]').val();

    if (tipoprodu == 0){
     $('#filaprodu').addClass('hidden');
     $('#filaprecio').addClass('hidden');
     $('#filaloca').addClass('hidden');
     $('#filaboton').addClass('hidden');
     $('#buscarporprodu').addClass('hidden');
     $('#lblprodu').addClass('hidden');
     $('#cmbprodu').addClass('hidden');
     $('#lblpreciomax').addClass('hidden');
     $('#txtpreciomax').addClass('hidden');
     $('#cmblocalidadprod').addClass('hidden');
     $('#lbllocalidad').addClass('hidden');
     $('#completeprecio').addClass('hidden');
     $('#completeproducto').addClass('hidden');
   }else{ 

     $.ajax({
      url: 'ajax/procesartipoproducto.ajax.php',
      data: {'tipoprodu':tipoprodu},
      type: 'POST',
      dataType: 'json',

      beforeSend: function () {
             $('body').addClass('loading'); //Agregamos la clase loading al body
           },

           success: function( data ) {
            console.log('peticion lograda con exito');
            console.log(data);

            $('#filaprodu').removeClass('hidden');
            $('#cmbprodu').empty();
            $('#lblprodu').removeClass('hidden');
            $('#cmbprodu').removeClass('hidden');
            $('#cmbprodu').append('<option value="0">Selecciona un Producto</option>');
            for(var i = 1; i < data.length; i++) {
              $("#cmbprodu").append('<option value="'+data[i][1]+'">'+data[i][2]+'</option>');
            }
      $('body').removeClass('loading'); //Removemos la clase loading
    },             
    error: function( data ) {
      console.log('se ejecuto mal la peticion');
      console.log(data);
      $('#filaprodu').addClass('hidden');
      $('#filaprecio').addClass('hidden');
      $('#filaloca').addClass('hidden');
      $('#filaboton').addClass('hidden');
      $('#buscarporprodu').addClass('hidden');
      $('#lblprodu').addClass('hidden');
      $('#cmbprodu').addClass('hidden');
      $('#cmblocalidadprod').addClass('hidden');
      $('#lbllocalidad').addClass('hidden');
      $('#lblpreciomax').addClass('hidden');
      $('#txtpreciomax').addClass('hidden');
      $('#completeprecio').addClass('hidden');
      $('#completeproducto').addClass('hidden');

       $('body').removeClass('loading'); //Removemos la clase loading

     }                         
   })
}

});
});

//CARGAR COMBO LOCALIDAD A PARTIR DE PRODU

var produ;

$(document).ready(function(){
  $('select[id=cmbprodu]').val(0); 

  $("select[id=cmbprodu]").change(function(){
    produ=$('select[id=cmbprodu]').val();

    if (produ == 0){
     $('#filaprecio').addClass('hidden');
     $('#filaloca').addClass('hidden');
     $('#filaboton').addClass('hidden');
     $('#cmblocalidadprod').empty();
     $('#buscarporprodu').addClass('hidden');
     $('#lblpreciomax').addClass('hidden');
     $('#txtpreciomax').addClass('hidden');
     $('#cmblocalidadprod').addClass('hidden');
     $('#lbllocalidad').addClass('hidden');
     $('#completeprecio').addClass('hidden');
     $('#completeproducto').addClass('hidden');
   }else{ 

     $.ajax({
      url: 'ajax/procesarproducto.ajax.php',
      data: {'produ':produ},
      type: 'POST',
      dataType: 'json',

      beforeSend: function () {
             $('body').addClass('loading'); //Agregamos la clase loading al body
           },

           success: function( data ) {
            console.log('peticion lograda con exito');
            console.log(data);

            $('#filaloca').removeClass('hidden');
            $('#cmblocalidadprod').empty();
            $('#buscarporprodu').removeClass('hidden');
            $('#cmblocalidadprod').removeClass('hidden');
            $('#lbllocalidad').removeClass('hidden');

            for(var j = 1; j < data.length; j++) {
              $("#cmblocalidadprod").append('<option value="'+data[j][0]+'">'+data[j][1]+'</option>');
            }
            $('#filaboton').removeClass('hidden');
            $('#filaprecio').removeClass('hidden');
            $('#lblpreciomax').removeClass('hidden');      
            $('#txtpreciomax').removeClass('hidden');  

        $('body').removeClass('loading'); //Removemos la clase loading                              
      },             
      error: function( data ) {
        console.log('se ejecuto mal la peticion');
        console.log(data);

        $('#filaprecio').addClass('hidden');
        $('#filaloca').addClass('hidden');
        $('#filaboton').addClass('hidden');
        $('#cmblocalidadprod').empty();
        $('#buscarporprodu').addClass('hidden');
        $('#cmblocalidadprod').addClass('hidden');
        $('#lbllocalidad').addClass('hidden');
        $('#lblpreciomax').addClass('hidden');
        $('#txtpreciomax').addClass('hidden');
        $('#completeprecio').addClass('hidden');
        $('#completeproducto').addClass('hidden');

        $('body').removeClass('loading'); //Removemos la clase loading
      }                         
    })   
}       
});
});



function validarprecio(){
  var preciomax = document.getElementById("txtpreciomax").value;
  var producto = document.getElementById("cmbprodu").value;
  var formulario = document.getElementById("FrmBarProdu"); 

         if (isNaN(preciomax) && producto != 0){ //si no es un numero y producto existe
          $('#completeproducto').addClass('hidden');
          $('#completeprecio').removeClass('hidden');
          return false;
        }
         if (!isNaN(preciomax) && producto == 0){  //si es un numero y producto no existe
          $('#completeprecio').addClass('hidden');
          $('#completeproducto').removeClass('hidden');
          return false;
        }


          if (!isNaN(preciomax) && producto != 0 ){ //si es un numero y producto existe
            $('#completeprecio').addClass('hidden');
            $('#completeproducto').addClass('hidden');
            formulario.submit();
            return true;
          }else{
            $('#completeproducto').removeClass('hidden');
            $('#completeprecio').removeClass('hidden');
            return false;
          }

        }


        function ocultarmensajes_modal_solicitud(){ 
          $("#complete_nombre").addClass('hidden');
          $('#complete_dni').addClass('hidden');
          $('#complete_bar').addClass('hidden');
          $('#complete_localidad').addClass('hidden');
          $('#complete_barrio').addClass('hidden');
          $('#complete_direccion').addClass('hidden'); 
          $('#complete_telefono').addClass('hidden'); 
          $('#msg_soli_registrada').addClass('hidden');
          $('#msg_soli_error').addClass('hidden'); 

          $('#S_dni').val("");
          $('#S_nombrebar').val("");
          $('#S_localidad').val("");
          $('#S_barrio').val("");
          $('#S_direccion').val("");
          $('#S_tel').val("");
        }

        function enviarsolicitud(){

          var usu = $('#S_usuario').val();
          var nomusu = $('#S_nombreusu').val();
          var dniusu = $('#S_dni').val();
          var nombar = $('#S_nombrebar').val();
          var locabar = $('#S_localidad').val();
          var barriobar = $('#S_barrio').val();
          var direbar = $('#S_direccion').val();
          var tel = $('#S_tel').val();
          var tipo = $('#S_cmbtipo').val();

//VALIDAR INPUTS
if (isNaN(nomusu) && nomusu != "" && !isNaN(dniusu) && dniusu != "" && isNaN(nombar) && nombar != "" && isNaN(locabar) && locabar != "" && isNaN(barriobar) && barriobar != "" && isNaN(direbar) && direbar != "" && !isNaN(tel) && tel != ""){

  $('#complete_telefono').addClass('hidden');
  $('#complete_nombre').addClass('hidden');
  $('#complete_dni').addClass('hidden');
  $('#complete_bar').addClass('hidden');
  $('#complete_localidad').addClass('hidden');
  $('#complete_barrio').addClass('hidden');
  $('#complete_direccion').addClass('hidden');

  $.ajax({
    url: 'ajax/procesarsolicitudaltabar.ajax.php',
    data: {'datousu':usu,'datonomusu':nomusu,'datodni':dniusu,
    'datonombar':nombar,'datolocabar':locabar,'datobarriobar':barriobar,
    'datodirebar':direbar,'datotel':tel,'datotipo':tipo},
    type: 'POST',
    dataType: 'json',

    beforeSend: function () {
             $('body').addClass('loading'); //Agregamos la clase loading al body
           },

           success: function( data ) {
            console.log('peticion lograda con exito');
            console.log(data);

            $(document).ready(function() {

              if (data['registrado'] == 1){
               $("#msg_soli_registrada").removeClass('hidden');
               $('#S_dni').val("");
               $('#S_nombrebar').val("");
               $('#S_localidad').val("");
               $('#S_barrio').val("");
               $('#S_direccion').val("");
               $('#S_tel').val("");

          $('body').removeClass('loading'); //Removemos la clase loading  
        }
        if(data['registrado'] == 0){
         $('#msg_soli_error').removeClass('hidden');

         $('body').removeClass('loading'); //Removemos la clase loading 
       }
     });

          },             
          error: function( data ) {
            console.log('se ejecuto mal la peticion');
            console.log(data);
            console.log(data['mensaje']);

    $('body').removeClass('loading'); //Removemos la clase loading 
  }                         
})   

}
if (tel == "" || isNaN(tel) || tel.length > 15){
  $('#complete_telefono').removeClass('hidden');
  $('#complete_nombre').addClass('hidden');
  $('#complete_dni').addClass('hidden');
  $('#complete_bar').addClass('hidden');
  $('#complete_localidad').addClass('hidden');
  $('#complete_barrio').addClass('hidden');
  $('#complete_direccion').addClass('hidden');
  $('#msg_soli_registrada').addClass('hidden');
  $('#msg_soli_error').addClass('hidden'); 
}
if (direbar == ""){
  $('#complete_direccion').removeClass('hidden');
  $('#complete_nombre').addClass('hidden');
  $('#complete_dni').addClass('hidden');
  $('#complete_bar').addClass('hidden');
  $('#complete_localidad').addClass('hidden');
  $('#complete_barrio').addClass('hidden');
  $('#complete_telefono').addClass('hidden');
  $('#msg_soli_registrada').addClass('hidden');
  $('#msg_soli_error').addClass('hidden'); 
}
if (barriobar == "" || !isNaN(barriobar)){
  $('#complete_barrio').removeClass('hidden');
  $('#complete_nombre').addClass('hidden');
  $('#complete_dni').addClass('hidden');
  $('#complete_bar').addClass('hidden');
  $('#complete_localidad').addClass('hidden');
  $('#complete_direccion').addClass('hidden');
  $('#complete_telefono').addClass('hidden');
  $('#msg_soli_registrada').addClass('hidden');
  $('#msg_soli_error').addClass('hidden'); 
}
if (locabar == "" || !isNaN(locabar)){
  $('#complete_localidad').removeClass('hidden');
  $('#complete_bar').addClass('hidden');
  $('#complete_nombre').addClass('hidden');
  $('#complete_dni').addClass('hidden');
  $('#complete_barrio').addClass('hidden');
  $('#complete_direccion').addClass('hidden');
  $('#complete_telefono').addClass('hidden');
  $('#msg_soli_registrada').addClass('hidden');
  $('#msg_soli_error').addClass('hidden'); 
}
if (nombar == ""){
  $('#complete_bar').removeClass('hidden');
  $('#complete_nombre').addClass('hidden');
  $('#complete_dni').addClass('hidden');
  $('#complete_localidad').addClass('hidden');
  $('#complete_barrio').addClass('hidden');
  $('#complete_direccion').addClass('hidden');
  $('#complete_telefono').addClass('hidden');
  $('#msg_soli_registrada').addClass('hidden');
  $('#msg_soli_error').addClass('hidden'); 
}

if( dniusu == "" || isNaN(dniusu) || dniusu.length > 8){
  $('#complete_dni').removeClass('hidden');
  $('#complete_nombre').addClass('hidden');
  $('#complete_bar').addClass('hidden');
  $('#complete_localidad').addClass('hidden');
  $('#complete_barrio').addClass('hidden');
  $('#complete_direccion').addClass('hidden');
  $('#complete_telefono').addClass('hidden');
  $('#msg_soli_registrada').addClass('hidden');
  $('#msg_soli_error').addClass('hidden'); 
}

if(nomusu == "" || !isNaN(nomusu) ) {
  $('#complete_nombre').removeClass('hidden');
  $('#complete_dni').addClass('hidden');
  $('#complete_bar').addClass('hidden');
  $('#complete_localidad').addClass('hidden');
  $('#complete_barrio').addClass('hidden');
  $('#complete_direccion').addClass('hidden');
  $('#complete_telefono').addClass('hidden');
  $('#msg_soli_registrada').addClass('hidden');
  $('#msg_soli_error').addClass('hidden'); 
}

}

</script>
<script type="text/javascript">
document.oncontextmenu = function(){return false;}
</script>
<script> loadingOFF(); </script>
<div class="modalloading"></div>
</body>
</html>