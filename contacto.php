
<?php

    if(session_id() == '' || !isset($_SESSION)) {
        session_start();
    }

if ($_SESSION['estado'] != 'S'){
  header ("Location: logeo.php");
  exit();
}

    if (!(isset($_SESSION['iniciado']))) {
        header ("Location: logeo.php");
        exit();
    }
        
    if ($_SESSION['iniciado'] != '5dbc98dcc983a70728bd082d1a47546e'){
        header ("Location: logeo.php");
        exit();
        
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Drinky || Contacto</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"> 
	<link rel="stylesheet" href="css/bootstrap.min.css"/> 
	
  <!--LOADING YA INICIADO -->
  <?php include("loading.php"); ?>
  <!--LOADING -->
	<script  src="js/jquery-3.1.0.min.js"></script>
  
   <script>
    function loadingON(){
    $('#loadingDiv').removeClass('hidden');
   }
   function loadingOFF(){
    $('#loadingDiv').addClass('hidden');
    }
   </script>

	<script  src="js/bootstrap.min.js"></script>

  <link rel="stylesheet" href="css/bootstrap-social.css"/>
  <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css ">
  <link rel="stylesheet" href="css/headeryfooter.css"/>
  <link rel="icon" type="image/png" href="images/drinky-logo.png" />
  <link rel="stylesheet" href="css/contacto.css"/>

</head>

<script>

    function activarMiMenu() {
    $('#menureservas').removeClass('activo');
    $('#menulocales').removeClass('activo');   
    $('#menuinicio').removeClass('activo');
    $('#menucontacto').addClass('activo');
    
    }

    window.onload = activarMiMenu;


</script>


<body>
	<?php include_once("header.php"); ?>
<br>
<br>
<br>

 <div class="carousel slide" data-ride="carousel" id="carousel-1"> 

    <!-- INDICADORES -->
    <ol class="carousel-indicators">
      <li data-target="#carousel-1" data-slide-to="0" style="background:#6CDAF8;border:2px solid #6CDAF8" class="active"></li>
      <li data-target="#carousel-1" data-slide-to="1" style="background:#6CDAF8;border:2px solid #6CDAF8"></li> 
      <li data-target="#carousel-1" data-slide-to="2" style="background:#6CDAF8;border:2px solid #6CDAF8"></li> 
    </ol>

    <!-- CONTENEDOR DE LOS SLIDE, SON ITEMS -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="images/tragos.jpg"   class="img-responsive" alt=""><!--IMAGENES GRANDES OPTIMIZADAS NO PESADAS -->
        <div class="carousel-caption hidden-xs hidden-sm"><!-- CON HIDDEN OCULTAMOS EL TEXTOPARA CIERTO TAMAÑO-->
         <div class="centrar"><h1><b>Consulta toda la info de Bares y Boliches en Córdoba</b></h1></div>
       </div>
     </div>

     <div class="item">
      <img src="images/celu.jpg"   class="img-responsive" alt=""><!--IMAGENES GRANDES OPTIMIZADAS NO PESADAS -->
      <div class="carousel-caption hidden-xs hidden-sm"><!-- CON HIDDEN OCULTAMOS EL TEXTOPARA CIERTO TAMAÑO-->
       <div class="centrar"><h1><b>Consulta toda la info de Bares y Boliches en Córdoba</b></h1></div>

     </div>
   </div>

   <div class="item">
    <img src="images/vinos.jpg"   class="img-responsive" alt=""><!--IMAGENES GRANDES OPTIMIZADAS NO PESADAS -->
    <div class="carousel-caption hidden-xs hidden-sm"> <!-- CON HIDDEN OCULTAMOS EL TEXTO PARA CIERTO TAMAÑO-->
     <div class="centrar"><h1><b>Consulta toda la info de Bares y Boliches en Córdoba</b></h1></div>
   </div>
 </div>

 <a href="#carousel-1" style="color:#6CDAF8;" class="left carousel-control" role="button" data-slide="prev">
  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
  <span class="sr-only"></span> <!-- PARA VERLO EN DISPOSITIVOS DE LECTURA -->
</a>

<a href="#carousel-1" style="color:#6CDAF8;" class="right carousel-control" role="button" data-slide="next">
  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
  <span class="sr-only"></span> <!-- PARA VERLO EN DISPOSITIVOS DE LECTURA -->
</a>
</div>
</div>
        
     <div  id="header" class="withPhoto withApps normal withOptional" style="background-image: url(images/nueva.jpg);background-size:cover">     
<div class="headerOverlay showBackground">
            <br>

	<div class="container">
		<div class="row" style="min-height:600px;">
			<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">

 <h2><b>Contacto:</b></h2>
 <br>
          <form action=""  method="post" name"FrmContacto" class="form-horizontal">  

                        <div class="form-group has-primary">
                          <label for="usuario" class="control-label col-md-2"><u>Usuario:</u></label>
                          <div class="col-md-10">
                            <input type="text" value=<?php echo $_SESSION['usuario'];?> class="form-control" id="con_txtusuario" name ="con_txtusuario" placeholder="" disabled/>
                        </div>
                        </div>
                        
                        <div class="form-group has-primary">
                          <label for="tipo" class="control-label col-md-2"><u>Tipo:</u></label>
                          <div class="col-md-10">
                            <select class="form-control" name="con_cmbtipomensaje" id="con_cmbtipomensaje" placeholder="Selecciona un tipo de Mensaje">
                             <option value="0">-- Selecciona un Tipo de Mensaje</option>
                             <option value="1">Consulta</option>
                             <option value="2">Solicitud Cambio de Dirección</option>
                             <option value="3">Otro</option>
                           </select>
                           <div id="complete_cmbtipo" class="hidden"><label><h5><b>Seleccione un tipo de mensaje</b></h5></label></div> 
                          </div>  
                        </div>

                         <div class="form-group has-primary hidden" id="divcmbbar">
                          <label for="bar" class="control-label col-md-2"><u>Local:</u></label>
                          <div class="col-md-10">
                            <select class="form-control" name="con_cmbbar" id="con_cmbbar" placeholder="Selecciona un Local a Modificar Direccion">
                            
                           </select>
                          </div>  
                        </div>

                        <div class="form-group has-primary">
                          <label for="telefono" class="control-label col-md-2"><u>Teléfono:</u></label>
                          <div class="col-md-10">
                            <input type="text" class="form-control" id="con_txt_telefono" name ="con_txt_telefono" placeholder="Ingrese un numero de teléfono (opcional)">
                               <div id="complete_telefono" class="hidden"><label><h5><b>Ingrese un número telefónico no mayor a 15 digitos (solo números)</b></h5></label></div> 
                          </div>  
                        </div>

                      <div class="form-group has-primary">
                          <label for="mensaje" class="control-label col-md-2"><u>Mensaje:</u></label>
                          <div class="col-md-10">
                             <textarea height="300" class=" form-control" rows="3" id="con_text_mensaje" placeholder="Introduce un mensaje" required></textarea>
                            <div id="complete_mensaje" class="hidden"><label><h5><b>Debe ingresar un mensaje (max: 250 caracteres)</b></h5></label></div> 
                          </div>  
                        </div>

                         <div class="form-group has-success">
                          <div class="col-md-12">
                          <table class="pull-right">
                            <tr>
                              <th width="730" style="padding-right:10px;">

                           <div class="alert alert-success hidden" id="msgenviado"><button class="close" data-dismiss="alert"><span>&times;</span></button>
                           <st><span class="glyphicon glyphicon-saved"> </span><b> Muy Bien! </b> El Mensaje ha sido enviado con Éxito</st>
                         </div> 
                         <div class="alert alert-danger hidden" id="msgerror"><button class="close" data-dismiss="alert"><span>&times;</span></button>
                         <st><span class="glyphicon glyphicon-remove"></span></st> Error al enviar el mensaje. Vuelva a intentarlo.
                       </div> 
                            </th>
                              <th>
                          <a type="button" id="btnlimpiar" class="btn btn-warning" style="margin-right:5px;" onclick="limpiar()" tittle="Enviar Mensaje">Limpiar</a>&nbsp;&nbsp;
                        </th>
                        <th>
                             <a type="button" id="btnenviar" class="btn btn-success " onclick="enviarmensaje()" tittle="Enviar Mensaje">Enviar Mensaje</a>&nbsp;&nbsp;&nbsp;
                         </th> </tr>
                          </table>
                           </div>
                        </div>
          </form>

			</div>
		</div>

	</div>
</div>
</div>
<?php include_once("footer.php"); ?>
<div class="modalloading"></div>
</body>


<script>

//TRAER BARES DE ESE USUARIO PARA MODIFICAR

var usu = $('#con_txtusuario').val();

$("select[id=con_cmbtipomensaje]").change(function(){

$('#con_cmbbar').empty();
if ($('#con_cmbtipomensaje').val() == '2'){

$('#divcmbbar').removeClass('hidden');

$.ajax({
      url: 'ajax/procesar_contacto_barescambiodire.ajax.php',
      data: {'emailusu':usu},
      type: 'POST',
      dataType: 'json',

             beforeSend: function () {
             $('body').addClass('loading'); //Agregamos la clase loading al body
            },

      success: function( data ) {
        console.log('peticion lograda con exito');
        console.log(data);

      $('#con_cmbbar').append('<option value="0">Seleccione Local');
        for(var i = 1; i < data.length; i++) {
        $('#con_cmbbar').append('<option value="'+data[i][0]+'">'+data[i][2]+' '+data[i][1]+'</option>');
        }
        
        $('#con_cmbbar').val(0);

          $('body').removeClass('loading'); //Removemos la clase loading
       },             
      error: function( data ) {
        console.log('se ejecuto mal la peticion');
        console.log(data);
      
          $('body').removeClass('loading'); //Removemos la clase loading
      }, 
    })


}else{
  $('#divcmbbar').addClass('hidden');
}

});

//ENVIAR MENSAJE

function enviarmensaje(){

  var usu = $('#con_txtusuario').val();
  var tipomensaje = $('#con_cmbtipomensaje').val();
  var txttel = $('#con_txt_telefono').val();
  var msg = $('#con_text_mensaje').val();

if (!isNaN(txttel) && txttel != "" && txttel.length < 16 && msg.length < 250 && msg != "" && tipomensaje != 0){

if ($('#con_cmbtipomensaje').val() == '2'){
  var cmbidbar =  $('#con_cmbbar').val();

  $.ajax({
      url: 'ajax/procesar_mensaje.ajax.php',
      data: {'emailusu':usu,'tipomsg':tipomensaje,'telefono':txttel,'men':msg,'idbar':cmbidbar},
      type: 'POST',
      dataType: 'json',

             beforeSend: function () {
             $('body').addClass('loading'); //Agregamos la clase loading al body
            },
       
      success: function( data ) {
        console.log('peticion lograda con exito');
        console.log(data);

        if (data['enviado'] ==1){
        $('#complete_cmbtipo').addClass('hidden');
        $('#complete_telefono').addClass('hidden');
        $('#complete_mensaje').addClass('hidden');
        $('#msgenviado').removeClass('hidden');
        $('#divcmbbar').addClass('hidden');
        $('#con_cmbtipomensaje').val(0);
        $('#con_txt_telefono').val("");
        $('#con_text_mensaje').val("");

           $('body').removeClass('loading'); //Removemos la clase loading
        }else{
        $('#msgenviado').addClass('hidden');
        $('#msgerror').removeClass('hidden');

        $('body').removeClass('loading'); //Removemos la clase loading
        }

       },             
      error: function( data ) {
        console.log('se ejecuto mal la peticion');
        console.log(data);
         $('#msgenviado').addClass('hidden');
        $('#msgerror').removeClass('hidden');

            $('body').removeClass('loading'); //Removemos la clase loading
      }, 
    })

}else{

$.ajax({
      url: 'ajax/procesar_mensaje.ajax.php',
      data: {'emailusu':usu,'tipomsg':tipomensaje,'telefono':txttel,'men':msg},
      type: 'POST',
      dataType: 'json',

             beforeSend: function () {
             $('body').addClass('loading'); //Agregamos la clase loading al body
            },
          
      success: function( data ) {
        console.log('peticion lograda con exito');
        console.log(data);

        if (data['enviado'] ==1){
            $('#complete_cmbtipo').addClass('hidden');
            $('#complete_telefono').addClass('hidden');
            $('#complete_mensaje').addClass('hidden');
            $('#msgenviado').removeClass('hidden');
            $('#con_cmbtipomensaje').val(0);
            $('#con_txt_telefono').val("");
            $('#con_text_mensaje').val("");

             $('body').removeClass('loading'); //Removemos la clase loading
        }else{
            $('#msgenviado').addClass('hidden');
            $('#msgerror').removeClass('hidden');

            $('body').removeClass('loading'); //Removemos la clase loading
        }

       },             
      error: function( data ) {
        console.log('se ejecuto mal la peticion');
        console.log(data);
      $('#msgenviado').addClass('hidden');
      $('#msgerror').removeClass('hidden');

        $('body').removeClass('loading'); //Removemos la clase loading
      }, 
    })
  }


}else{
  $('#msgenviado').addClass('hidden');
  $('#msgerror').addClass('hidden');
  if ((isNaN(txttel) || txttel == "" || txttel.length > 15) && (msg.length > 250 || msg == "") && tipomensaje == 0){
    $('#complete_cmbtipo').removeClass('hidden');
    $('#complete_telefono').removeClass('hidden');
    $('#complete_mensaje').removeClass('hidden');
   // return false;
  } else if ((isNaN(txttel) || txttel == "" || txttel.length > 15) && (msg.length > 250 || msg == "")) {
    $('#complete_cmbtipo').addClass('hidden');
    $('#complete_telefono').removeClass('hidden');
    $('#complete_mensaje').removeClass('hidden');
  //  return false;
  }else if ((isNaN(txttel) || txttel == "" || txttel.length > 15) && tipomensaje == 0){
    $('#complete_cmbtipo').removeClass('hidden');
    $('#complete_telefono').removeClass('hidden');
    $('#complete_mensaje').addClass('hidden');
   // return false;
  }else if ((msg.length > 250 || msg == "") && tipomensaje == 0){
    $('#complete_cmbtipo').removeClass('hidden');
    $('#complete_telefono').addClass('hidden');
    $('#complete_mensaje').removeClass('hidden');
    //return false;
  }else if (isNaN(txttel) || txttel == "" || txttel.length > 15){
    $('#complete_cmbtipo').addClass('hidden');
    $('#complete_telefono').removeClass('hidden');
    $('#complete_mensaje').addClass('hidden');
    //return false;
  }else if (msg.length > 250 || msg == ""){
    $('#complete_cmbtipo').addClass('hidden');
    $('#complete_telefono').addClass('hidden');
    $('#complete_mensaje').removeClass('hidden');
    //return false; 
  }else if (tipomensaje == 0){
    $('#complete_telefono').addClass('hidden');
    $('#complete_mensaje').addClass('hidden');
    $('#complete_cmbtipo').removeClass('hidden');
   // return false;
  }
    
}
  
}



function limpiar(){
  $('#msgenviado').addClass('hidden');
  $('#msgerror').addClass('hidden');
  $('#complete_cmbtipo').addClass('hidden');
  $('#complete_telefono').addClass('hidden');
  $('#complete_mensaje').addClass('hidden');
  $('#con_cmbtipomensaje').val(0);
  $('#con_txt_telefono').val("");
  $('#con_text_mensaje').val("");
}
</script>

<script type="text/javascript">
document.oncontextmenu = function(){return false;}
</script>

<script> loadingOFF(); </script>
</html>