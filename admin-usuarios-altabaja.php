	
<?php

if(session_id() == '' || !isset($_SESSION)) {
  session_start();
}

if (!(isset($_SESSION['iniciado']))) {
  header ("Location: logeo.php");
  exit();
}

if ($_SESSION['tipo'] != 'A'){
  header ("Location: logeo.php");
  exit();
}


if ($_SESSION['iniciado'] != '5dbc98dcc983a70728bd082d1a47546e'){
  header ("Location: logeo.php");
  exit();

}

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Drinky || Admin - Usuarios</title>

    <!--LOADING --> 
    <?php include("loading.php"); ?>
    <!--LOADING -->
    
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link rel="icon" type="image/png" href="images/drinky-logo.png" />
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="css/admin-general.css" rel="stylesheet" type="text/css">
    <link type="text/css" rel="stylesheet" href="css/chosen.min.css">
</head>



<body>

    <div id="wrapper">

        <?php include('admin-header.php'); ?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            USUARIOS

                        </h1>
                        <ol class="breadcrumb" >
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="admin-index.php">Tablero</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-file"></i> Alta / Baja y Roles de Usuarios
                            </li>
                        </ol>


                        <div class="page-content">
                            <div id="tab-general" style="height:100vh;">
                                <div class="col-lg-1 col-xs-12 col-sm-12"><!--relleno-->
                                </div>

                                <div class="col-md-12 col-lg-10 col-xs-12 col-sm-12">
                                    <br>
                                    <div class="panel panel-grey">
                                        <div class="panel-heading">
                                            <h1>Alta / Baja y Roles de Usuarios</h1>
                                        </div>
                                        <div class="panel-body pan">
                                           <!--h2 style="color:#FFF; background-color:#3C3C3C; padding:5px; padding-left:15px; margin-right:5px; margin-left:5px">&nbsp;Buscar un Local:</h2> -->
                                           <br>

                                           <input type="hidden" id="usuariocarga" name="usuariocarga" value="<?php echo $_SESSION['usuario']; ?>">
                                           <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                                            <label class="col-sm-12 control-label"><i class="glyphicon glyphicon-chevron-right color-oficial"></i>&nbsp;Usuario:</label>
                                            <div class="col-sm-12">
                                                <select class="form-control chosen-select" id="cmbusers"  name="cmbusers">
                                                   <option value="0">Seleccione un Usuario</option>
                                                   <?php 

                                                   include('conexion/conexion.php');

                                                   $conection = mysqli_connect($host, $usuario, $pass,$db) or die("error de conexion a DB");

                                                   $select = $conection->query("SELECT * FROM usuarios ORDER BY email");

                                                   $estado;
                                                   $tipousu;

                                                   while ($result = mysqli_fetch_assoc($select)) 
                                                   {
                                                    if ($result['activo'] == "S"){
                                                        $estado = 'Activo';
                                                    }else {
                                                        $estado = 'Inactivo';
                                                    }
                                                    if ($result['tipo'] == 'A'){
                                                        $tipousu = 'Admin';
                                                    }else{
                                                        $tipousu = 'Consultor';
                                                    }
                                                    ?>     
                                                    <option value="<?php echo $result["email"];?>" id="opttipo<?php echo $result["email"];?>" title="<?php echo $result["email"];?>">
                                                       <?php echo $result["email"] . ", " .$tipousu. "</b>, ".$estado."</option>";

                                                   }
                                                   $select->close();
                                                   $conection->next_result();

                                                   ?>

                                               </select>
                                           </div>
                                       </div>

                                       <div class="row">
                                        <div id="divcontenido" style="" class="hidden col-lg-12">
  
                                           <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                                            <h2 style="color:#FFF; background-color:#3C3C3C; padding:5px; padding-left:15px; margin-right:15px; margin-left:15px;">&nbsp;Rol y Estado de Usuario</h2><br>
                                            <label class="col-sm-12 control-label"><i class="glyphicon glyphicon-chevron-right color-oficial"></i>&nbsp;Tipo de Usuario:</label>
                                            <div class="col-sm-12">
                                                <select class="form-control" id="cmbtipo"  name="cmbtipo">
                                                   <option value="U">Consultor</option>
                                                   <option value="A">Admin</option>
                                               </select>

                                           </div>
                                       </div>

                                       <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                                        <label class="col-sm-12 control-label"><i class="glyphicon glyphicon-chevron-right color-oficial"></i>&nbsp;Estado:</label>
                                        <div class="col-sm-12">
                                            <select class="form-control" id="cmbestado"  name="cmbestado">
                                               <option value="S">Activo</option>
                                               <option value="N">Inactivo</option>
                                           </select>

                                       </div>
                                   </div>
                                   <!---->
                                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
                                    
                                       <div class="alert alert-success hidden" style="text-align:center" id="msgmodificado" ><button class="close" data-dismiss="alert"><span>&times;</span></button>
                                         <st><span class="glyphicon glyphicon-saved"></span><b> Estupendo! </b>Cambios Efectuados con Éxito. Actualize la página si desea ver los cambios en el Selector.</st>
                                     </div> 
                                     <div class="alert alert-danger hidden" style="text-align:center" id="msgerror"><button class="close" data-dismiss="alert"><span>&times;</span></button>
                                         <st><span class="glyphicon glyphicon-remove"> </span><b> Error! </b> No se pudo efectuar la modificación.</st>
                                     </div>  
                                     <div style="padding-left:15px;padding-right:15px">
                                         <button style="text-align:center" onclick="guardarcambios()" id="m_btn_agregar" name="m_btn_agregar" type="button" class="pull-right btn btn-success btn-lg"><i class="fa fa-save"></i>&nbsp;&nbsp;Guardar</button>
                                     </div>


                                 </div>
                                 <!---->
                             </div>
                         </div>

                     </div>
                 </div>


             </div>

         </div>
         <!-- /.row -->

     </div>
     <!-- /.container-fluid -->

 </div>
 <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="js/jquery.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>
<script src="js/chosen.jquery.min.js"></script>

<script>
function loadingON(){
    $('#loadingDiv').removeClass('hidden');
}
function loadingOFF(){
    $('#loadingDiv').addClass('hidden');
}
</script>

<script>

        //INICIALIZO LOS CHOSEN SELECT
        $(".chosen-select").chosen({
            no_results_text: "No hay resultados!",
            placeholder_text_single: "Seleccionar",
            placeholder_text_multiple: "Seleccionar"
        });
        //TRAER DATOS USUARIOS
        $("select[id=cmbusers]").change(function(){
           var cmbusu = $('#cmbusers').val();

           if (cmbusu == 0){
              $('#divcontenido').empty();
              return false;  
          }
        
          $.ajax({
              url: 'ajax/admin_buscar_usuarios.ajax.php',
              data: {'usuarioemail':cmbusu},
              type: 'POST',
              dataType: 'json',

              beforeSend: function () {
              $('body').addClass('loading'); //Agregamos la clase loading al body
          },

          success: function( data ) {
            console.log('peticion lograda con exito');
            console.log(data);

            $('#divcontenido').removeClass('hidden')
            $('#cmbtipo').val(data[0][3]);
            $('#cmbestado').val(data[0][4]);


             $('body').removeClass('loading'); //Removemos la clase loading

         },             
         error: function( data ) {
            console.log('se ejecuto mal la peticion');
            console.log(data);

               $('body').removeClass('loading'); //Removemos la clase loading

           }, 
       })
     
      });

function guardarcambios(){
var usuario = $('#cmbusers').val();
var usutipo = $('#cmbtipo').val();
var usuest = $('#cmbestado').val();

          $.ajax({
              url: 'ajax/admin_update_usuarios.ajax.php',
              data: {'usuarioemail':usuario,'tipousu':usutipo,'estadousu':usuest},
              type: 'POST',
              dataType: 'json',

              beforeSend: function () {
              $('body').addClass('loading'); //Agregamos la clase loading al body
          },

          success: function( data ) {
            console.log('peticion lograda con exito');
            console.log(data);

            if (data['confirmado'] == 1){
            $('#msgerror').addClass('hidden');
            $('#msgmodificado').removeClass('hidden');
            }else{
            $('#msgerror').removeClass('hidden');
            $('#msgmodificado').addClass('hidden');
            }

             $('body').removeClass('loading'); //Removemos la clase loading

         },             
         error: function( data ) {
            console.log('se ejecuto mal la peticion');
            console.log(data);
            $('#msgmodificado').addClass('hidden');
            $('#msgerror').removeClass('hidden');

               $('body').removeClass('loading'); //Removemos la clase loading

           }, 
       })
}

        </script>
        <script>loadingOFF();</script>
        <div class="modalloading"></div>
    </body>

    </html>

