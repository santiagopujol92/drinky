	
<?php

if(session_id() == '' || !isset($_SESSION)) {
  session_start();
}

if (!(isset($_SESSION['iniciado']))) {
  header ("Location: logeo.php");
  exit();
}

if ($_SESSION['tipo'] != 'A'){
  header ("Location: logeo.php");
  exit();
}


if ($_SESSION['iniciado'] != '5dbc98dcc983a70728bd082d1a47546e'){
  header ("Location: logeo.php");
  exit();

}

?>

        	<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Drinky || Admin - Usuarios</title>

     <!--LOADING --> 
    <?php include("loading.php"); ?>
    <!--LOADING -->
    
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
     <link rel="icon" type="image/png" href="images/drinky-logo.png" />
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	 <link href="css/admin-general.css" rel="stylesheet" type="text/css">
    <link type="text/css" rel="stylesheet" href="css/chosen.min.css">
</head>



<body>

    <div id="wrapper">

<?php include('admin-header.php'); ?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            USUARIOS
    
                        </h1>
                        <ol class="breadcrumb" >
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="admin-index.php">Tablero</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-file"></i> Datos y Monitoreo de Usuarios
                            </li>
                        </ol>

                         
			   <div class="page-content">
                    <div id="tab-general" style="height:100vh;">
                        <div class="col-lg-1 col-xs-12 col-sm-12"><!--relleno-->
                        </div>
                        
                        <div class="col-md-12 col-lg-10 col-xs-12 col-sm-12">
                            <br>
                            <div class="panel panel-grey">
                                <div class="panel-heading">
                                    <h1>Datos y Monitoreo de Usuarios</h1>
                                </div>
                                <div class="panel-body pan">
                                	<!--h2 style="color:#FFF; background-color:#3C3C3C; padding:5px; padding-left:15px; margin-right:5px; margin-left:5px">&nbsp;Buscar un Local:</h2> -->
                                    <br>

                                    <input type="hidden" id="usuariocarga" name="usuariocarga" value="<?php echo $_SESSION['usuario']; ?>">
                                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                                        <label class="col-sm-12 control-label"><i class="glyphicon glyphicon-chevron-right color-oficial"></i>&nbsp;Ver Datos Usuario:</label>
                                        <div class="col-sm-12">
                                            <select class="form-control chosen-select" id="cmbusers"  name="cmbusers">
                                                 <option value="0">Seleccione un Usuario</option>
                                                 <option value="Todos">Todos</option>
                                                    <?php 

                                                    include('conexion/conexion.php');

                                                    $conection = mysqli_connect($host, $usuario, $pass,$db) or die("error de conexion a DB");

                                                    $select = $conection->query("SELECT * FROM usuarios ORDER BY email");

                                                    while ($result = mysqli_fetch_assoc($select)) 
                                                    {
                                                        ?>     
                                                        <option value="<?php echo $result["email"];?>" id="opttipo<?php echo $result["email"];?>" title="<?php echo $result["email"];?>">
                                                         <?php echo $result["email"] . "</option>";

                                                     }
                                                     $select->close();
                                                     $conection->next_result();

                                                     ?>

                                                 </select>
                                        </div>
                                    </div>

                                                      <div class="row">
                                                        <div id="divcontenido" style="padding:15px" class="col-lg-12">
                                                  
                                                        </div>
                                                    </div>

                                   <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                                        <label class="col-sm-12 control-label"><i class="glyphicon glyphicon-chevron-right color-oficial"></i>&nbsp;Ver Accesos de Usuarios:</label>
                                        <div class="col-sm-12">
                                            <select class="form-control chosen-select" id="cmbtiempo"  name="cmbtiempo">
                                                 <option value="0">Seleccione una Fecha</option>
                                                 <option value="Hoy">Hoy</option>
                                                 <option value="Ayer">Desde Ayer</option>
                                                 <option value="Semana">Última Semana</option>  
                                                 <option value="Quincena">Últimos 15 dias</option> 
                                                 <option value="Mes">Último Mes</option> 
                                                 <option value="Trimestre">Último Trimestre</option> 
                                                 <option value="Semestre">Último Semestre</option> 
                                                 <option value="Año">Último Año</option> 
                                                 <option value="Origen">El Origen de los Tiempos</option> 
                                                 </select>
                                        </div>
                                    </div>

                                                     <div class="row">
                                                        <div id="divcontenido2" style="padding:15px" class="col-lg-12">
                                                  
                                                        </div>
                                                    </div>

                                             
                                            </div>
                                        </div>

                        <div class="col-lg-1 col-xs-12 col-sm-12"> <!--relleno-->
                        </div>
                    </div>
                </div>
                        
                        
                    </div>

                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/chosen.jquery.min.js"></script>

   <script>
    function loadingON(){
    $('#loadingDiv').removeClass('hidden');
   }
   function loadingOFF(){
    $('#loadingDiv').addClass('hidden');
    }
   </script>

    <script>

        //INICIALIZO LOS CHOSEN SELECT
        $(".chosen-select").chosen({
                no_results_text: "No hay resultados!",
                placeholder_text_single: "Seleccionar",
                placeholder_text_multiple: "Seleccionar"
        });
        //MOSTRAR DATOS USUARIOS
        $("select[id=cmbusers]").change(function(){
         var cmbusu = $('#cmbusers').val();

         if (cmbusu == 0){
          $('#divcontenido').empty();
          return false;  
         }
        
        $.ajax({
              url: 'ajax/admin_buscar_usuarios.ajax.php',
              data: {'usuarioemail':cmbusu},
              type: 'POST',
              dataType: 'json',

             beforeSend: function () {
              $('body').addClass('loading'); //Agregamos la clase loading al body
            },

              success: function( data ) {
                console.log('peticion lograda con exito');
                console.log(data);

                $('#divcontenido').empty();
                $('#divcontenido').append('<div class="table-responsive"><table class="table table-bordered table-hover">'
                 +'<thead><tr class="info"><th>Email</th><th>Nombre Google</th><th>Nombre Face</th><th>Tipo</th><th>Activo</th><th>Fecha Último Ingreso</th><th>Hora Último Ingreso</th>'
                 +'</tr></thead><tbody id="tablebody">');

                   var contarfilas = 0;
                   var colorfila = '';
                   var estado = '';
                   var tipousu = '';
                   var admines = 0;
                    for(var i = 0; i < data.length; i++) {
                        if (data[i][4] == 'S'){
                            colorfila = 'success';
                            estado = 'Activo'
                        }else if (data[i][4] == "N"){
                            colorfila = 'danger';
                             estado = 'Inactivo'
                        }
                        if (data[i][3] == 'A'){
                            celdatipo = 'orange';
                            tipousu = 'Admin';
                            admines = admines+1;
                        }else{
                            tipousu = 'Consultor'
                            celdatipo = '';
                        }

                    $('#tablebody').append('<tr class="'+colorfila+'"><td>'+data[i][0]+'</td><td>'+data[i][1]+'</td><td>'+data[i][2]+'</td><td style="background:'+celdatipo+'">'+tipousu+'</td><td>'+estado+'</td><td>'+data[i][5]+'</td><td>'+data[i][6]+' hs</td></tr>');
                    
                    contarfilas = contarfilas+1;

                    }
                $('#divcontenido').append('</tbody></table></div>');

              $('#divcontenido').append('<div><h4 style="color:orange;">Admines: <b>'+admines+'</b></h4><h4>Total Usuarios: <b>'+contarfilas+'</b></h4></div>');
             
             $('body').removeClass('loading'); //Removemos la clase loading

               },             
              error: function( data ) {
                console.log('se ejecuto mal la peticion');
                console.log(data);

               $('body').removeClass('loading'); //Removemos la clase loading

              }, 
            })
        });

//FILTRAR USUARIOS POR FECHA

   $("select[id=cmbtiempo]").change(function(){
         var cmb_tiempo = $('#cmbtiempo').val();

         if (cmb_tiempo == 0){
          $('#divcontenido2').empty();
          return false;  
         }
        
        $.ajax({
              url: 'ajax/admin_usuarios_portiempo.ajax.php',
              data: {'tiempo':cmb_tiempo},
              type: 'POST',
              dataType: 'json',

             beforeSend: function () {
              $('body').addClass('loading'); //Agregamos la clase loading al body
            },

              success: function( data ) {
                console.log('peticion lograda con exito');
                console.log(data);

                $('#divcontenido2').empty();
                $('#divcontenido2').append('<div class="table-responsive"><table class="table table-bordered table-hover">'
                 +'<thead><tr class="info"><th>Email</th><th>Nombre Google</th><th>Nombre Face</th><th>Tipo</th><th>Activo</th><th>Fecha Último Ingreso</th><th>Hora Último Ingreso</th>'
                 +'</tr></thead><tbody id="tablebody2">');

                   var contarfilas = 0;
                   var colorfila = '';
                   var estado = '';
                   var tipousu = '';
                   var admines = 0;
                    for(var i = 0; i < data.length; i++) {
                        if (data[i][4] == 'S'){
                            colorfila = 'success';
                            estado = 'Activo'
                        }else if (data[i][4] == "N"){
                            colorfila = 'danger';
                             estado = 'Inactivo'
                        }
                        if (data[i][3] == 'A'){
                            celdatipo = 'orange';
                            tipousu = 'Admin';
                            admines = admines+1;
                        }else{
                            tipousu = 'Consultor'
                            celdatipo = '';
                        }

                    $('#tablebody2').append('<tr class="'+colorfila+'"><td>'+data[i][0]+'</td><td>'+data[i][1]+'</td><td>'+data[i][2]+'</td><td style="background:'+celdatipo+'">'+tipousu+'</td><td>'+estado+'</td><td>'+data[i][5]+'</td><td>'+data[i][6]+' hs</td></tr>');
                    
                    contarfilas = contarfilas+1;

                    }
                $('#divcontenido2').append('</tbody></table></div>');

              $('#divcontenido2').append('<div><h4 style="color:orange;">Admines: <b>'+admines+'</b></h4><h4>Total Usuarios: <b>'+contarfilas+'</b></h4></div>');
             
             $('body').removeClass('loading'); //Removemos la clase loading

               },             
              error: function( data ) {
                console.log('se ejecuto mal la peticion');
                console.log(data);

               $('body').removeClass('loading'); //Removemos la clase loading

              }, 
            })
        });

    </script>
<script>loadingOFF();</script>
<div class="modalloading"></div>
</body>

</html>

