    
<?php

if(session_id() == '' || !isset($_SESSION)) {
  session_start();
}

if (!(isset($_SESSION['iniciado']))) {
  header ("Location: logeo.php");
  exit();
}

if ($_SESSION['tipo'] != 'A'){
  header ("Location: logeo.php");
  exit();
}


if ($_SESSION['iniciado'] != '5dbc98dcc983a70728bd082d1a47546e'){
  header ("Location: logeo.php");
  exit();

}

?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Drinky || Admin - Ubicaciones </title>

   <!--LOADING --> 
  <?php include("loading.php"); ?>
  <!--LOADING -->

  <!-- Bootstrap Core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom CSS -->
  <link href="css/sb-admin.css" rel="stylesheet">

  <!-- Custom Fonts -->
  <link rel="icon" type="image/png" href="images/drinky-logo.png" />
  <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="css/admin-general.css" rel="stylesheet" type="text/css">
  <link type="text/css" rel="stylesheet" href="css/chosen.min.css">
</head>

<body>

  <div id="wrapper">

    <?php include('admin-header.php'); ?>

    <div id="page-wrapper">

      <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
          <div class="col-lg-12">
            <h1 class="page-header">
              UBICACIONES

            </h1>
            <ol class="breadcrumb" >
              <li>
                <i class="fa fa-dashboard"></i>  <a href="admin-index.php">Tablero</a>
              </li>
              <li class="active">
                <i class="fa fa-file"></i> ABM Cordenadas
              </li>
            </ol>


            <div class="page-content">
              <div id="tab-general" style="height:100vh;">
                <div class="col-lg-1 col-xs-12 col-sm-12"><!--relleno-->
                </div>

                <div class="col-md-12 col-lg-10 col-xs-12 col-sm-12">
                  <br>
                  <div class="panel panel-grey">
                    <div class="panel-heading">
                      <h1>Alta / Baja / Modificación de Cordenadas de Local</h1>
                    </div>
                    <div class="panel-body pan">
                   
                      <br>

                    
                   
                      
                        <div class="form-group">
                            <label for="title" class="col-sm-12 control-label"><i class="glyphicon glyphicon-chevron-right color-oficial"></i>&nbsp;Seleccione Bar</label>
                            <div id="divcombobar" class="col-sm-12">
                              <select name="m_cmbbar" class="form-control chosen-select"  id="m_cmbbar">
                                <option value="0">-- Seleccione un Local</option>
                                
                           <?php 

                           include('conexion/conexion.php');

                           $conection = mysqli_connect($host, $usuario, $pass,$db) or die("error de conexion a DB");

                           $select = $conection->query("SELECT idbar,nombre_bar,tipolocal FROM bares ORDER BY activo desc,tipolocal, nombre_bar");

                           while ($result = mysqli_fetch_assoc($select)) 
                           {
                            ?>     
                            <option value="<?php echo $result["idbar"];?>">
                             <?php echo $result["tipolocal"] . " " . $result["nombre_bar"] . ", " . $result["idbar"] . "</option>";

                           }
                           $select->close();
                           $conection->next_result();

                           ?>

                         </select>
                       </div>
                     </div>
              

                     <div class="row">
                      <div id="divcontenido" style="padding:15px;" class="hidden col-lg-12">
                        <div class="panel-body pan">
                          
                          <h2 style="color:#FFF; background-color:#3C3C3C; padding:5px; padding-left:15px; margin-right:5px; margin-left:5px">&nbsp;Datos de Ubicación</h2>
                          <br> 
                        
           

                          <div id="formdire" class="form-group">
                            <label for="title" class="col-sm-12 control-label"><i class="glyphicon glyphicon-chevron-right color-oficial"></i>&nbsp;Dirección</label>
                            <div class="col-sm-12">
                              <input type="text" name="c_txtdire" class="form-control" id="c_txtdire" placeholder="Introduce Dirección" disabled><br>
                            </div>
                          </div>
                          <div id="formloca" class="form-group">
                            <label for="title" class="col-sm-12 control-label"><i class="glyphicon glyphicon-chevron-right color-oficial"></i>&nbsp;Localidad</label>
                            <div class="col-sm-12">
                             <input type="text" name="c_txt_loca" class="form-control" id="c_txt_loca" placeholder="Introduce Localidad" disabled><br>
                             </div>
                           </div>
                           <div id="formbarrio" class="form-group">
                            <label for="title" class="col-sm-12 control-label"><i class="glyphicon glyphicon-chevron-right color-oficial"></i>&nbsp;Barrio</label>
                            <div class="col-sm-12">
                            <input type="text" name="c_txt_barrio" class="form-control" id="c_txt_barrio" placeholder="Introduce Barrio" disabled><br>
                              </div>
                            </div>
                          
                        <div class="col-sm-12" style="padding:0px;"> 
                         <h2 style="color:#FFF; background-color:#3C3C3C; padding:5px; padding-left:15px; margin-right:5px; margin-left:5px">&nbsp;Cordenadas</h2>
                          <br></div>
                             <div id="divlatitud" class="form-group">
                            <label for="title" class="col-sm-12 control-label"><i class="glyphicon glyphicon-chevron-right color-oficial"></i>&nbsp;Latitud</label>
                            <div class="col-sm-12">

                              <input type="text" name="c_txt_latitud" class="form-control" id="c_txt_latitud" placeholder="Introduce Latitud Exacta">
                                <div id="c_complete_latitud" class="rojo hidden"><label><h5><b>Ingrese una latitud (numero negativo o positivo)</b></h5></label></div><br>
                            </div>
                          </div>
                            <div id="divlongitud" class="form-group">
                            <label for="title" class="col-sm-12 control-label"><i class="glyphicon glyphicon-chevron-right color-oficial"></i>&nbsp;Longitud</label>
                            <div class="col-sm-12">
                              <input type="text" name="c_txt_longitud" class="form-control" id="c_txt_longitud" placeholder="Introduce Longitud Exacta">
                                <div id="c_complete_longitud" class="rojo hidden"><label><h5><b>Ingrese una longitud (numero negativo o positivo)</b></h5></label></div><br>
                            </div>
                          </div>
                          <div class="col-sm-12">
                         <a href="http://www.coordenadas-gps.com/" class="btn btn-info" target="_blank" style="margin-bottom:6px;"><span class="glyphicon glyphicon-search"></span> Buscar Cordenadas</a>
                          </div>
                        
                    <div style="padding-left:15px;padding-right:15px">
                       <button style="text-align:center" onclick="guardarcordenada()" id="m_btn_guardar" name="m_btn_guardar" type="button" class="pull-right btn btn-success btn-lg"><i class="fa fa-save"></i>&nbsp;&nbsp;Guardar</button>
                    </div>

                      </div>

<!---->
                <div class="row"> 
                    <div class="col-sm-12">
                     <div class="alert alert-success hidden" style="text-align:center" id="msgmodificado"><button class="close" data-dismiss="alert"><span>&times;</span></button>
                       <st><span class="glyphicon glyphicon-saved"></span><b> Estupendo! </b>Cordenadas Modificadas con Éxito</st>
                     </div> 
                     <div class="alert alert-danger hidden" style="text-align:center" id="msgerror"><button class="close" data-dismiss="alert"><span>&times;</span></button>
                       <st><span class="glyphicon glyphicon-remove"> </span><b> Error! </b> No se pudo efectuar la modificación.</st>
                     </div>  
                    </div>

                </div>
<!---->
                    </div>

                  </div>

                </div>
              </div>


            </div>
          </div>

        
        </div>

      </div>


    </div>
  </div>
  <!-- /.row -->

</div>
<!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="js/jquery.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>
<script src="js/chosen.jquery.min.js"></script>

   <script>
    function loadingON(){
    $('#loadingDiv').removeClass('hidden');
   }
   function loadingOFF(){
    $('#loadingDiv').addClass('hidden');
    }
   </script>

<script>
        //INICIALIZO LOS CHOSEN SELECT
        function iniciarchosen(){
             $(".chosen-select").chosen({
                no_results_text: "No hay resultados!",
                placeholder_text_single: "Seleccionar",
                placeholder_text_multiple: "Seleccionar"
        });
        }
        iniciarchosen();

//TRAER DATOS DE LOCAL Y CORDENADAS PARA MODIFICAR 

$("select[id=m_cmbbar]").change(function(){

 var m_idbar = $('#m_cmbbar').val();

if (m_idbar == 0){
  anularmensajes();
  $('#divcontenido').addClass('hidden');
}else{

 $.ajax({
  url: 'ajax/admin_loadmodificar_bares.ajax.php',
  data: {'barid':m_idbar},
  type: 'POST',
  dataType: 'json',

             beforeSend: function () {
              $('body').addClass('loading'); //Agregamos la clase loading al body
            },

  success: function( data ) {
    console.log('peticion lograda con exito');
    console.log(data);

anularmensajes();
      $('#c_txtdire').empty();
      $('#c_txt_loca').empty();
      $('#c_txt_barrio').empty();
      $('#c_txt_latitud').empty();
      $('#c_txt_longitud').empty();

      $('#divcontenido').removeClass('hidden');
      $('#c_txtdire').val(data[0][3]);
      $('#c_txt_loca').val(data[0][10]);
      $('#c_txt_barrio').val(data[0][11]);
      $('#c_txt_latitud').val(data[0][8]);
      $('#c_txt_longitud').val(data[0][9]);

       $('body').removeClass('loading'); //Removemos la clase loading  
     },             
      error: function( data ) {
      console.log('se ejecuto mal la peticion');
      console.log(data);

      $('body').removeClass('loading'); //Removemos la clase loading
      }, 
      })
}

});

//ENVIAR AJAX DE MODIFICACION DE CORDENADA

function guardarcordenada(){

c_id_bar = $('#m_cmbbar').val();
c_lati = $('#c_txt_latitud').val();
c_longi = $('#c_txt_longitud').val();

if (c_lati != "" && c_lati.length < 51 && !isNaN(c_lati) && c_longi != "" && c_longi.length < 51 && !isNaN(c_longi)){
  $('#divlatitud').removeClass('has-error');
  $('#divlongitud').removeClass('has-error');

  $('#c_complete_latitud').addClass('hidden');
  $('#c_complete_longitud').addClass('hidden');

  $('#divlatitud').addClass('has-success');
  $('#divlongitud').addClass('has-success');

  $.ajax({
  url: 'ajax/admin_modificar_cordenadas.ajax.php',
  data: {'id':c_id_bar,'lati':c_lati,'longi':c_longi},
  type: 'POST',
  dataType: 'json',

             beforeSend: function () {
              $('body').addClass('loading'); //Agregamos la clase loading al body
            },

  success: function( data ) {
    console.log('peticion lograda con exito');
    console.log(data);

if(data['cordenada_modificada'] == 1){

    $('#msgerror').addClass('hidden');
    $('#msgmodificado').removeClass('hidden');

   $('body').removeClass('loading'); //Removemos la clase loading
}else{

    $('#msgmodificado').addClass('hidden');
    $('#msgerror').removeClass('hidden');

    $('body').removeClass('loading'); //Removemos la clase loading
}

     },             
      error: function( data ) {
      console.log('se ejecuto mal la peticion');
      console.log(data);

    $('#msgmodificado').addClass('hidden');
    $('#msgerror').removeClass('hidden');

    $('body').removeClass('loading'); //Removemos la clase loading

      }, 
      })

  }else{

    $('#msgerror').addClass('hidden');
    $('#msgmodificado').addClass('hidden');

     if (c_lati == "" || c_lati.length > 50 || isNaN(c_lati)){
      $('#divlatitud').removeClass('has-success');
      $('#divlatitud').addClass('has-error');
      $('#c_complete_latitud').removeClass('hidden');
     }else{
      $('#divlatitud').addClass('has-success');
      $('#divlatitud').removeClass('has-error');
      $('#c_complete_latitud').addClass('hidden');
     }
     if (c_longi == "" || c_longi.length > 50 || isNaN(c_longi)){
      $('#divlongitud').removeClass('has-success');
      $('#divlongitud').addClass('has-error');
      $('#c_complete_longitud').removeClass('hidden');
     }else{
      $('#divlongitud').addClass('has-success');
      $('#divlongitud').removeClass('has-error');
      $('#c_complete_longitud').addClass('hidden');
     }


  }

}


function anularmensajes(){
$('#msgerror').addClass('hidden');
$('#msgmodificado').addClass('hidden');
$('#c_complete_latitud').addClass('hidden');
$('#c_complete_longitud').addClass('hidden');
$('#divlongitud').removeClass('has-error');
$('#divlatitud').removeClass('has-error');
$('#divlongitud').removeClass('has-success');
$('#divlatitud').removeClass('has-success');
}

</script>
<script> loadingOFF();</script>
<div class="modalloading"></div>
</body>

</html>

