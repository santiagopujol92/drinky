
<?php

if(session_id() == '' || !isset($_SESSION)) {
    session_start();
}

if ($_SESSION['estado'] != 'S'){
  header ("Location: logeo.php");
  exit();
}

if (!(isset($_SESSION['iniciado']))) {
    header ("Location: logeo.php");
    exit();
}

if ($_SESSION['iniciado'] != '5dbc98dcc983a70728bd082d1a47546e'){
    header ("Location: logeo.php");
    exit();

}

?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Drinky || Mis Reservas</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"> 
	<!--LOADING -->
  <?php include("loading.php"); ?>
  <!--LOADING -->

  <link rel="stylesheet" href="css/bootstrap.min.css"/> 
	
	<script  src="js/jquery-3.1.0.min.js"></script>
	<script  src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/bootstrap-social.css"/>
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css ">
    <link rel="icon" type="image/png" href="images/drinky-logo.png" />    
    <link rel="stylesheet" href="css/headeryfooter.css"/>
    <link rel="stylesheet" href="css/misreservas.css"/>

   <script>
    function loadingON(){
    $('#loadingDiv').removeClass('hidden');
   }
   function loadingOFF(){
    $('#loadingDiv').addClass('hidden');
    }
   </script>

</head>



<body>
	<?php include_once("header.php"); ?>
    <br>
    <br>
    <br>

 <div class="carousel slide" data-ride="carousel" id="carousel-1"> 

    <!-- INDICADORES -->
    <ol class="carousel-indicators">
      <li data-target="#carousel-1" data-slide-to="0" style="background:#6CDAF8;border:2px solid #6CDAF8" class="active"></li>
      <li data-target="#carousel-1" data-slide-to="1" style="background:#6CDAF8;border:2px solid #6CDAF8"></li> 
      <li data-target="#carousel-1" data-slide-to="2" style="background:#6CDAF8;border:2px solid #6CDAF8"></li> 
    </ol>

    <!-- CONTENEDOR DE LOS SLIDE, SON ITEMS -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="images/tragos.jpg"   class="img-responsive" alt=""><!--IMAGENES GRANDES OPTIMIZADAS NO PESADAS -->
        <div class="carousel-caption hidden-xs hidden-sm"><!-- CON HIDDEN OCULTAMOS EL TEXTOPARA CIERTO TAMAÑO-->
         <div class="centrar"><h1><b>Consulta toda la info de Bares y Boliches en Córdoba</b></h1></div>
       </div>
     </div>

     <div class="item">
      <img src="images/celu.jpg"   class="img-responsive" alt=""><!--IMAGENES GRANDES OPTIMIZADAS NO PESADAS -->
      <div class="carousel-caption hidden-xs hidden-sm"><!-- CON HIDDEN OCULTAMOS EL TEXTOPARA CIERTO TAMAÑO-->
       <div class="centrar"><h1><b>Consulta toda la info de Bares y Boliches en Córdoba</b></h1></div>

     </div>
   </div>

   <div class="item">
    <img src="images/vinos.jpg"   class="img-responsive" alt=""><!--IMAGENES GRANDES OPTIMIZADAS NO PESADAS -->
    <div class="carousel-caption hidden-xs hidden-sm"> <!-- CON HIDDEN OCULTAMOS EL TEXTO PARA CIERTO TAMAÑO-->
     <div class="centrar"><h1><b>Consulta toda la info de Bares y Boliches en Córdoba</b></h1></div>
   </div>
 </div>

 <a href="#carousel-1" style="color:#6CDAF8;" class="left carousel-control" role="button" data-slide="prev">
  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
  <span class="sr-only"></span> <!-- PARA VERLO EN DISPOSITIVOS DE LECTURA -->
</a>

<a href="#carousel-1" style="color:#6CDAF8;" class="right carousel-control" role="button" data-slide="next">
  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
  <span class="sr-only"></span> <!-- PARA VERLO EN DISPOSITIVOS DE LECTURA -->
</a>
</div>
</div>

 <div  id="header" class="withPhoto withApps normal withOptional" style="background-image: url(images/nueva.jpg);background-size:cover">
      
<!--BOTON DE ACTUALIZAR FIJO-->
<div id="actualizarfijo" style="position:fixed !important;z-index:10;bottom:0%;left:0%;margin:10px;">
 <a class="btn btn-success" id="btn_actreservas" onclick="actualizarmisreservas()" style="padding:5px;" title="Actualizar Todo"><b>Actualizar <span width="100" class="glyphicon glyphicon-refresh"></span></b></a>
</div>
<!--BOTON DE ACTUALIZAR FIJO-->

        <div class="headerOverlay showBackground">
            <br>
            <div class="container">
              <div class="row" style="min-height:600px;">

               <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12" >

                <h2><b>Lista de Reservas Realizadas:</b></h2>
            <div id="divtablaresp" class="table table-responsive">
                    <table id="tablamisreservas" class="table table-hover table-border"  >

                     <tr class="danger tablatit" >
                          <th width="800" >
                            
                            <h4 ><u><b>Local:</b></u></h4>
                        </th>
                          <th width="800">
                             
                            <h4><u> <b>Fecha:</b> </u></h4>
                        </th>
                          <th width="800">
                              
                            <h4><u> <b>Hora:</b> </u></h4>
                        </th>

                          <th width="800">
                              
                            <h4><u> <b>Teléfono Local:</b> </u></h4>
                        </th>
                          <br>
                              <th width="800">
                                 
                            <h4> <u><b>Dirección Local:</b></u> </h4>
                        </th>
                        <th width="300">
                             
                           <h4> <u> <b>Personas:</b></u> </h4>
                        </th>
                        <th width="400">
                            
                           <h4> <u> <b>Estado:</b></u> </h4>
                        </th>
                        <th width="100" >
                            <a class="btn btn-success" id="btn_actreservas" onclick="actualizarmisreservas()"  title="Actualizar Todo"><b><span class="glyphicon glyphicon-refresh"></span></b></a>
                        </th>

                    </tr>


                      <?php 

                      $miusu = $_SESSION['usuario'];

                      include('conexion/conexion.php');

                      $conection = mysqli_connect($host, $usuario, $pass,$db) or die("error de conexion a DB");

                      $select = $conection->query("SELECT b.nombre_bar,b.telefono,b.direccion,b.tipolocal, r.* FROM reservas r INNER JOIN bares b ON b.idbar = r.idbar
                        WHERE r.email_usu = '$miusu' ORDER BY r.fecha DESC,r.estado");


                      while ($result = mysqli_fetch_assoc($select)) 
                      {
                        $fecha = substr($result["fecha"],0,10);
                        $hora = substr($result["fecha"],-8,5);

                        ?>

                        <tr  id="filareserva" class="filareserva">

                          <th width="800">
                           
                           <h4> <b><?php echo $result["tipolocal"].' '. $result["nombre_bar"]; ?> </b></h4>
                        </th>
                          <th width="800">
                           
                           <h4> <?php echo $fecha;?> </h4>
                        </th>
                          <th width="800">
                             
                           <h4> <?php echo $hora." hs";?></h4>
                        </th>

                          <th width="800">
                            
                           <h4><?php echo $result['telefono'];?></h4>
                        </th>
                              <th width="800">
                                
                           <h4> <?php echo $result['direccion'];?> </h4>
                        </th>
                              <th width="300">
                              
                           <h4> <?php echo $result['cantidad_pers'];?>  </h4>
                        </th>
                        <th width="400">
                           <?php $claseestado;
                           if($result['estado'] == "Pendiente"){
                            $claseestado = "enespera";
                           }else if ($result['estado'] == "Confirmado"){
                            $claseestado = "confirmado";
                           }else if ($result['estado'] == "Rechazado"){
                            $claseestado = "rechazado";
                           }
                            echo '<h4><b class="'.$claseestado.'">'.$result['estado'];?></b></h4>
                        </th>
                         <th width="100">
                           <a id="btneliminar" data-toggle="modal" type="submit" onclick="" data-id="<?php echo $result['email_usu'].'*'.$result['idbar'].'*'.$result['fecha'] ?>" class="modaleliminar btn btn-danger" href="#modal_confirmacion" title="Eliminar Reserva">
                            <span class="glyphicon glyphicon-trash"></span></a>
                        </th>
               
                    </tr>

                    <?php
                      }
                      $select->close();
                      $conection->next_result();

                      ?>

            
          </table>

</div>



            <div class="modal fade" id="modal_confirmacion">

              <div class="modal-dialog">
                <div class="modal-content">

                 
                  <div class="modal-header">
                    <a type="button" class="close"  data-dismiss="modal" aria-hidden="true">&times;</a> 
                     <h4 class="modal-title" id="tituloyfotosbar">
                        <b>Eliminar Reserva:</b></h4> 
                  </div>

                
                  <div class="modal-body">
                  <h4><p>Esta seguro que desea Eliminar la Reserva?</p></h4><br>
                  <p>Al eliminarla desaparecerá de la lista de reservas del propietario del local que las adminsitra.</p>
                  </div>

                 

                  <div class="modal-footer">
                    <a type="button" class="btn btn-lg btn-danger"  data-dismiss="modal"><span class="glyphicon glyphicon-cancel"></span> No</a>
                     <a type="submit" id="btn_sieliminar" onclick="" class="btn btn-lg btn-success pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-ok"></span> Si</a>
                  </div>
                </div>
              </div>
            </div>

      </div>

</div>
</div>
</div>
</div>
</div>
<?php include_once("footer.php"); ?>
<div class='modalloading'></div>
</body>

<script>

function activarMiMenu() {
    $('#menulocales').removeClass('activo');   
    $('#menuinicio').removeClass('activo');
    $('#menucontacto').removeClass('activo');
    $('#menureservas').addClass('activo');
}

window.onload = activarMiMenu;

//ELIMINAR RESERVA SI

var datosreserva;
var arraydatos;


 $(document).on("click", ".modaleliminar", function () {

    datosreserva = $(this).data("id");
    $("#modal_confirmacion").val(datosreserva);
    arraydatos = datosreserva.split("*",3);

  }); 

$(document).on("click", "#btn_sieliminar", function () {

    $.ajax({
      url: 'ajax/procesareliminarreserva.ajax.php',
      data: {'email':arraydatos[0],'idbar':arraydatos[1],'fecha':arraydatos[2]},
      type: 'POST',
      dataType: 'json',

             beforeSend: function () {
             $('body').addClass('loading'); //Agregamos la clase loading al body
            },

      success: function( data ) {
        console.log('peticion lograda con exito');
        console.log(data);

         if(data['eliminada'] == 1){
         actualizarmisreservas();
          $('body').removeClass('loading'); //Removemos la clase loading
         }

      },             
      error:  function( data ) {

        console.log('se ejecuto mal la peticion');
        console.log(data);

         $('body').removeClass('loading'); //Removemos la clase loading
      }                          
    })
    
  });     

//ACTUALIZAR RESERVAS

function actualizarmisreservas(){
 
      $.ajax({
      url: 'ajax/actualizarreserva.ajax.php',
      type: 'POST',
      dataType: 'json',

             beforeSend: function () {
             $('body').addClass('loading'); //Agregamos la clase loading al body
            },
   
      success: function( data ) {
        console.log('peticion lograda con exito');
        console.log(data);

$('#divtablaresp').empty();

$('#divtablaresp').append('<table id="tablamisreservas" class="table table-hover"><tr class="danger tablatit"><th width="800"><h4><u><b>Local:</b></u></h4></th>'
  +'<th width="800"> <h4><u> <b>Fecha:</b> </u></h4></th><th width="800"><h4><u> <b>Hora:</b> </u></h4></th>'
  +'<th width="800"><h4><u> <b>Teléfono Local:</b> </u></h4> </th><br><th width="800"><h4> <u><b>Dirección Local:'
  +'</b></u> </h4> </th> <th width="300"><h4> <u> <b>Personas:</b></u> </h4></th><th width="400"> <h4> <u>'
  +'<b>Estado:</b></u> </h4></th><th width="100"> <a class="btn btn-success" id="btn_actreservas"'
  +'onclick="actualizarmisreservas()"  title="Actualizar Todo"><b><span class="glyphicon glyphicon-refresh">'
   +'</span></b></a></th></tr>');
        
        for(var i = 0; i < data.length; i++){
            $('#tablamisreservas').append('<tr  id="filareserva" class="filareserva"><th width="800">'  
              +'<h4><b>'+data[i][11]+' '+data[i][0]+'</b></h4></th><th width="800"><h4>'+data[i][1]+'</h4></th><th width="800">' 
              +'<h4>'+data[i][2]+'</h4></th><th width="800"><h4>'+data[i][3]+'</h4></th><th width="800">'
               +'<h4>'+data[i][4]+'</h4></th><th width="300"> <h4>'+data[i][5]+'</h4></th><th width="400">'
                +'<h4><b class="'+data[i][7]+'">'+data[i][6]+'</b></h4></th><th width="100">'
                +'<a id="btneliminar" data-toggle="modal" type="submit" onclick="" data-id="'+data[i][9]+'*'+data[i][8]+'*'+data[i][10]+'" class="modaleliminar btn btn-danger" href="#modal_confirmacion" title="Eliminar Reserva">'
                +'<span class="glyphicon glyphicon-trash"></span></a></th></tr>');

             }

             $('#tablamisreservas').append('</table>');

           $('body').removeClass('loading'); //Removemos la clase loading
      },             
      error:  function( data ) {

        console.log('se ejecuto mal la peticion');
        console.log(data);

         $('body').removeClass('loading'); //Removemos la clase loading
      }                          
    });

}

//ACTUALIZAR CADA 10 SEG
/*$(document).ready(function() { 
    setInterval(actualizarmisreservas, 10000);
});*/
</script>

<script type="text/javascript">
document.oncontextmenu = function(){return false;}
</script>
<script> loadingOFF(); </script>
</html>