
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Drinky || Login</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"> 
	<meta name="google-signin-scope" content="profile email">
  <!-- PARA SPUJOLSOFT -->
  <!-- <meta name="google-signin-client_id" content="749409498432-q5nvikm5poi1meci4hpobb464d3rd8t9.apps.googleusercontent.com"> -->
  <!--  PARA LOCALHOST -->
  <meta name="google-signin-client_id" content="749409498432-79hibcp3onmpu8oiovk2hsqg8ihuai76.apps.googleusercontent.com">

  <!--LOADING -->
  <?php include("loading.php"); ?>
  <!--LOADING -->

  <link rel="stylesheet" href="css/bootstrap.min.css"/> 
  <link rel="stylesheet" href="css/bootstrap-social.css"/>
  <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css ">

  <link rel="icon" type="image/png" href="images/drinky-logo.png" />
  <!--<link rel="stylesheet" href ="css/slide.css"/>-->
  <link rel="stylesheet" href ="css/logeo.css"/>

  <script src="js/jquery-3.1.0.min.js"></script>
  <script src="js/bootstrap.min.js"></script>

  <script src="https://apis.google.com/js/platform.js" async defer></script>
  
  <script>
  function loadingON(){
    $('#loadingDiv').removeClass('hidden');
  }
  function loadingOFF(){
    $('#loadingDiv').addClass('hidden');
  }

  </script>


</head>
<body>

<header style="width:100%;height:80px;background:#1c1c1c;margin-bottom:8px;">
     <a href="logeo.php" id="brand"><img  id="logo" style="float:left;border:0px;margin-left:30px;margin-top:12px;padding:0px;" src="images/drinky-horizontal-min.jpg" onmouseout="sacarborde()" onmouseover="cambiarbordefoto()" class="img-thumbnail" width="200px" height="80" ></img></a>
</header>
  <div  id="header" class="withPhoto withApps normal withOptional" style="background-image: url(images/nueva.jpg);background-size:cover">

    <div class="headerOverlay showBackground" >    


    <div class="carousel slide" data-ride="carousel" id="carousel-1"> 

      <!-- INDICADORES -->
      <ol class="carousel-indicators">
        <li data-target="#carousel-1" data-slide-to="0" style="background:#6CDAF8;border:2px solid #6CDAF8" class="active"></li>
        <li data-target="#carousel-1" data-slide-to="1" style="background:#6CDAF8;border:2px solid #6CDAF8"></li> 
        <li data-target="#carousel-1" data-slide-to="2" style="background:#6CDAF8;border:2px solid #6CDAF8"></li> 
      </ol>

      <!-- CONTENEDOR DE LOS SLIDE, SON ITEMS -->
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img src="images/tragos.jpg"    class="img-responsive" alt=""><!--IMAGENES GRANDES OPTIMIZADAS NO PESADAS -->
          <div class="carousel-caption hidden-xs hidden-sm"><!-- CON HIDDEN OCULTAMOS EL TEXTOPARA CIERTO TAMAÑO-->
           <div class="container-fluid"  id="infousu">

            <div class="row hidden-xs hidden-sm" id="infonuevousuario"> 
              <div class="col-md-12 text-center">
                <h3 class="">
                 <span style="background:#2980b9;padding:15px;">Bienvenido a Drinky</span>
               </h3>
               <br>
               <br>
               <h4 class="">
                <span style="background:#000;padding:15px;">Bares y Boliches de toda Córdoba !</span>
              </h4>

              <br>
              <h4 class="">
                <span style="background:#000;padding:15px;">Consulta, informate y decide donde terminar tu noche</span>
              </h4>
              <br>
              

            </div>
          </div>
          <a data-toggle="pill" class="btn btn-default btn-sm btn-min-block" style="color:#000;" onclick="abrirusutab();scrolear('#infousutab');" href="#infousutab"><h4>Info Nuevo Usuario</h4><img class="img img-responsive" style="width:30px;" src="images/newuser.png"></img></a>
        </div><br>
      </div>
    </div>

    <div class="item">
      <img src="images/celu.jpg"   class="img-responsive" alt=""><!--IMAGENES GRANDES OPTIMIZADAS NO PESADAS -->
      <div class="carousel-caption hidden-xs hidden-sm"><!-- CON HIDDEN OCULTAMOS EL TEXTOPARA CIERTO TAMAÑO-->
       <div class="container-fluid" id="infopro">
        <br><br>
        <div class="row hidden-xs hidden-sm" id="infopropietarios">
          <div class="col-md-12 text-center">
            <h3 class="">
              <span style="background:#2980b9;padding:15px;">Si eres propietario de un Bar o Boliche</span>
            </h3>
            <br>
            <br>
            <h4 style="color:green;">
              <span style="background:#000;padding:15px;">PRIMER MES GRATIS !! AUMENTA CLIENTES EN TU LOCAL</span>
            </h4>
            <br>

          </div>

        </div>
        <a data-toggle="pill" class="btn btn-default btn-sm btn-min-block" style="color:#000;" onclick="abrirprotab();scrolear('#infoprotab');" href="#infoprotab"><h4>Info Propietarios</h4><img class="img img-responsive" style="width:30px;" src="images/jefe.png"></img></a>
      </div><br>

    </div>
  </div>

  <div class="item">
    <img src="images/vinos.jpg"   class="img-responsive" alt=""><!--IMAGENES GRANDES OPTIMIZADAS NO PESADAS -->
    <div class="carousel-caption hidden-xs hidden-sm"> <!-- CON HIDDEN OCULTAMOS EL TEXTO PARA CIERTO TAMAÑO-->
     <div class="container-fluid" id="app">
      <br>
      <div class="row hidden-xs hidden-sm" id="descargarapp">
        <div class="col-md-12 text-center">
          <h3 class="">
            <span style="background:#2980b9;padding:15px;">Tambien puedes acceder desde tu Celular</span>
          </h3>
          <br>
          <br>
          <h4 class="">
            <span style="background:#000;padding:15px;">No pierdas mas tiempo buscando donde ir !</span>
          </h4>
          <br>
          
        </div>
        <br>
      </div>
      <a class="btn btn-default btn-sm btn-min-block" style="color:#000;" onclick="alert('No contamos con App Android por el momento');" href="#" ><h4>Descargar App Drinky</h4><img class="img img-responsive" style="width:30px;" src="images/celular.png"></img></a>

    </div><br>
  </div>
</div>

<a href="#carousel-1" style="color:#6CDAF8;" class="left carousel-control" role="button" data-slide="prev">
  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
  <span class="sr-only"></span> <!-- PARA VERLO EN DISPOSITIVOS DE LECTURA -->
</a>

<a href="#carousel-1" style="color:#6CDAF8;" class="right carousel-control" role="button" data-slide="next">
  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
  <span class="sr-only"></span> <!-- PARA VERLO EN DISPOSITIVOS DE LECTURA -->
</a>
</div>
</div>
<br>
     <div class="container-fluid">
      <div class="row" >

        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 social" >
          <div id="conectarse">
           <h4><p id="textoconec"><u>Conectarse:</u></p><h4>
           </div>

           <div id="logingoogle"  class="hidden form-group btn g-signin2" data-onsuccess="onSignIn">
           </div>

           <div id ="google-sesion" class="hidden">
            <a  id="outgoogle"  onclick="loadingON();signOut();" type="submit" class="btn btn-social btn-google active "><span class="fa fa-google-plus"></span>Cerrar Sesion de Google</a>
            <br><br><strong></strong>
            <img></img>
            <div id='ingresar' class="btn expandir zoomIt">
           <br>  <a id="btningresar"  href='index.php' width="300" onclick="loadingON();"  class='btn'><h3><b>Ingresar</b></h3><span class='glyphicon glyphicon-star'></span></a>
           </div>  
         </div>


         <div class="hidden form-group" id="loginface">
          <a href="#" id="login" class="btn btn-social btn-facebook"><span class="fa fa-facebook"></span>Acceder con Facebook  </a><br> <!-- BTN BLOCK ES PARA QUE SEA TIPO BLOQUE EL -->
        </div>

      </div>
    </div>  

<hr class="hidden-md hidden-lg"></hr>

<div id="infousutab" class="tab-pane fade" style="margin-top:15px;">

  <fieldset class="row">
    <div class="container">  
      <div style="background:#000;color:#fff;"class="col-sm-12"><br>
        <img src="images/barnueva.jpg" width="600" class="img img-responsive">
        <h4><p>Bienvenido a DRINKY, una aplicación que le ayudará a decidir, accediendo a importante información de los lugares
         que el sistema brinda
         donde podría ir con <b style="color:yellow;">sus amigos, su pareja, su familia</b>, a beber, divertirse, tomar un cafe, comer algo, consultar
         información para un próximo evento, etc. 
       </p><p>
       DRINKY ofrece información de la carta (productos, precios), ubicación del lugar, fotos, promociones, gestion de reservas online
       en contacto directo con el dueño o encargado en locales como <b style="color:red;">Bares</b>, <b style="color:red;">Boliches</b>, <b style="color:red;">Pubs</b>,
       de la <b style="color:lightblue;">Ciudad de Córdoba, Argentina</b>.
     </p><p>Para comenzar a vivir la experiencia que ofrece nuestra aplicación, dirijase al encabezado y acceda por <b style="color:#dd4b39;">Google </b><a id="googleboton" onclick="scrolear_top();" class="btn" style="background:#fff;"><img src="images/googlelogo.png" width="20"/></a> o <b style="color:#3b5998;">Facebook</b>
     <a onclick="scrolear_top();" class="btn social btn-facebook"><span style="padding-left:5px;padding-right:5px;padding-top:2px;padding-bootom:2px;" class="fa fa-facebook"></span></a>
   </p><p style="padding-bottom:10px;"> Recuerde que si posee email distintos en las cuentas de facebook y google, tendrá una cuenta en cada email. Una vez más bienvenidos y agradecemos su interés. Para cualquier consulta dentro de la aplicación en el menú "Contacto" 
   podra dejarnos un mensaje.</p></h4>
 </div>
</div>
</fieldset> 

</div> 

<div id="infoprotab" class="tab-pane fade" style="margin-top:15px;">

  <fieldset class="row">
    <div class="container">  
      <div style="background:#000;color:#fff;"class="col-sm-12"><br>
        <img src="images/barcelu2.jpg" width="400" class="img img-responsive">
        <h4><p>Bienvenido propietario a DRINKY, si usted posee un local ya sea <b style="color:red;">Bar</b>, <b style="color:red;">Boliche</b>, <b style="color:red;">Cafetería</b>, <b style="color:red;">Pub</b>
          de la <b style="color:lightblue;">Ciudad de Córdoba, Argentina</b> y le gustaria que mas personas conozcan su local, brindando información previa (productos, promociones, precios, ubicación, fotos)
          y a la vez gestionar reservas online desde la aplicación, no espere más tiempo para incorporarse en nuestras búsquedas.
        </p>Para gestionar la solicitud de alta de su local, debe dirijirse al encabezado y conectarse por <b style="color:#dd4b39;">Google </b><a onclick="scrolear_top();" class="btn" style="background:#fff;"><img src="images/googlelogo.png" width="20"/></a> o <b style="color:#3b5998;">Facebook</b>
        <a onclick="scrolear_top();" class="btn social btn-facebook"><span style="padding-left:5px;padding-right:5px;padding-top:2px;padding-bootom:2px;" class="fa fa-facebook"></span></a> y clickee <b style="color:green;">"Ingresar"</b>.
        Luego en el pie de la página siguiente encontrará un boton <a class="btn btn-info active" style="padding:0px;"><b style="margin:0px;">Solicitar Registro de Mi Local</b></a> clickeando allí podra gestionar su solicitud.
      </p><p>Nos pondremos en contacto con usted para verificar que el bar cumple con los datos ingresados, incorporamos su bar, y usted puede
      acceder a la administración del mismo en el menu <b style="color:red;">"Mis Locales" </b>donde gestiona la carta agregando, modificando y eliminando productos; monitorear las reservas entrantes donde tiene la posibilidad
      de "confirmarla", "rechazarla" y "eliminarla" de acuerdo a su disposición y los datos de la misma; también puede editar datos principales de su bar y agregar fotos; y por ultimo la opción para dar de baja mediante solicitud.</p></h4>
      <p><h4 style="color:green;"><b>ATENCIÓN EL PRIMER MES SON GRATIS</b></h4></p>
      <p style="padding-bottom:10px;">Esta es una breve descripción de los beneficios y acciones que usted puede realizar con DRINKY, esperemos que sea de su agrado y pueda experimentar el rendimiento de su local con nuestra aplicación.</p>
    </div>
  </div>
</fieldset> 

</div>

 <a data-toggle="pill"  class="hidden-md hidden-lg btn btn-default btn-sm btn-min-block" style="margin-top:10px;color:#000;" onclick="abrirusutab();scrolear('#infousutab');" href="#infousutab"><h4>Info Nuevo Usuario</h4><img class="img img-responsive" style="width:30px;" src="images/newuser.png"></img></a><br><br>
 <a data-toggle="pill" class="hidden-md hidden-lg btn btn-default btn-sm btn-min-block" style="color:#000;" onclick="abrirprotab();scrolear('#infoprotab');" href="#infoprotab"><h4>Info Propietarios</h4><img class="img img-responsive" style="width:30px;" src="images/jefe.png"></img></a><br> <br>  
  <a style="margin-bottom:15px;" class="hidden-md hidden-lg btn btn-default btn-sm btn-min-block" style="color:#000;" onclick="alert('No contamos con App Android por el momento');" href="#" ><h4>Descargar App Drinky</h4><img class="img img-responsive" style="width:30px;" src="images/celular.png"></img></a>

</div>

</div>
</div>

<?php include_once("footer.php"); ?>

<script type="text/javascript">

document.oncontextmenu = function(){return false;}

</script>

<script>
//IR A UN ALGO CON SCROLL
function scrolear_algo(){
  var dest = $('#textoconec').offset().top;
  $("html, body").animate({scrollTop: dest});
}

function scrolear(tab){
  var dest = $(tab).offset().top;
  $("html, body").animate({scrollTop: dest});
}

$(document).ready(function(){
  $('#infoprotab').addClass('hidden');
  $('#infousutab').addClass('hidden');
});

function abrirusutab(){
  $('#infoprotab').addClass('hidden');
  $('#infousutab').removeClass('hidden');
}

function abrirprotab(){
  $('#infousutab').addClass('hidden');
  $('#infoprotab').removeClass('hidden');
}

function abrirdescapp(){
  $('#infousutab').addClass('hidden');
  $('#infoprotab').addClass('hidden');
}

(function(d, s, id) {
 var js, fjs = d.getElementsByTagName(s)[0];
 if (d.getElementById(id)) return;
 js = d.createElement(s); js.id = id;
 js.src = "//connect.facebook.net/en_US/sdk.js";
 fjs.parentNode.insertBefore(js, fjs);
	  }(document, 'script', 'facebook-jssdk'));  //VA EN EL BODY
</script>

<!-- LOGIN GOOGLE Y FACEBOOK -->

<script>
$(function() {

  var app_id = '957051937754664';
  var scopes = 'email';

  var conectarse= '<p id="textoconec"><u>Conectarse:</u></p>';

  var btn_login = '<div class=""><a href="#" id="login"  class="btn btn-social btn-facebook"><span class="fa fa-facebook"></span>Acceder Con Facebook</a></div>';
  var div_session = "<div id='facebook-session'>"+
  "<a href='logout.php' id='logout' onclick='loadingON();facebookLogout();' class='btn btn-social btn-facebook' ><span class='fa fa-facebook'></span>Cerrar Sesión de Facebook</a>"+
  "<br><br><strong></strong>"+
  "<img>"+
  ""+
  "<div id='ingresar' class='btn expandir zoomIt'><br><a id='btningresar' onclick='loadingON();' href='index.php' width=500 id='btningresar' class='btn'><h3><b>Ingresar</b></h3><span class='glyphicon glyphicon-glass'></span></a></div></div>";
  
  window.fbAsyncInit = function() {

    FB.init({
      appId      : app_id,
      status     : true,
      cookie     : true, 
      xfbml      : true, 
      version    : 'v2.5'
    });


    FB.getLoginStatus(function(response) {
      statusChangeCallback(response, function() {});

    });

  };

  var statusChangeCallback = function(response, callback) {
     // console.log(response);

     if (response.status === 'connected') {

      getFacebookData();
    } else {

       // $('#loginface').removeClass('hidden');
      //  $('#logingoogle').removeClass('hidden');
      loadingOFF();
      callback(false);

    }

  }

  var checkLoginState = function(callback) {

    FB.getLoginStatus(function(response) {
      callback(response);

    });

  }

  var getFacebookData =  function() {
   loadingON();

   var url = '/me?fields=id,name,email';

   FB.api(url, function(response) {

    $('#login').after(div_session);
    $('#login').remove();
    $('#logingoogle').addClass('hidden');
    $('#textoconec').remove();

    $('#facebook-session strong').text("Bienvenido: "+response.name);
    $('#facebook-session img').attr('src','http://graph.facebook.com/'+response.id+'/picture?type=large');
      /*  console.log("ID: "+ response.id);
        console.log("Name: "+response.name);
        console.log("Email: "+response.email);*/

          function Datos(id, email, cuenta, nombre, foto)   //
          {
            this.id = id;
            this.email = email;
            this.cuenta = cuenta;
            this.nombre = nombre;
                this.foto = 'http://graph.facebook.com/'+foto+'/picture?type=small'; // 
              }

         var dato = new Datos(response.id,response.email,'facebook', response.name, response.id); //
            // Creamos un arreglo para almacenarlos
            var listaDatos = [];

            listaDatos.push(dato);
            
             // Convertimos la lista de objetos en una cadena con el formato JSON
             var datosJSON = JSON.stringify(listaDatos);

              // Realizamos la petición al servidor
              $.post('loginpost.php', {datos: datosJSON},
                function(respuesta) {
                    //  console.log(respuesta);
                    loadingOFF();
                  }).error(
                  function(){
                   //   console.log('Error al ejecutar la petición de ajax a la pagina php');
                 }
                 );
                });

}

var facebookLogin = function() {
  checkLoginState(function(data) {
    if (data.status !== 'connected') {
      FB.login(function(response) {
        if (response.status === 'connected')

          getFacebookData();


      }, {scope: scopes});
    }

  })
}

var facebookLogout = function() {
  checkLoginState(function(data) {
    if (data.status === 'connected') {
      FB.logout(function(response) {
        $('#facebook-session').before(btn_login);
        $('#facebook-session').remove();
        $('#conectarse').before(conectarse);
        $('#logingoogle').removeClass('hidden');


        $('#loginface').removeClass('hidden');
        $('#logingoogle').removeClass('hidden');
        loadingOFF(); 
      })
    }
  })

}

$(document).on('click', '#login', function(e) {

  e.preventDefault();

  facebookLogin();


})

$(document).on('click', '#logout', function(e) {

  e.preventDefault();

  facebookLogout(); 

}) 
})

  // GOOOGLEEE

  function onSignIn(googleUser) {
    loadingON();
            // Useful data for your client-side scripts:
            var profile = googleUser.getBasicProfile();
          /*  console.log("ID: " + profile.getId()); // Don't send this directly to your server!
            console.log('Full Name: ' + profile.getName());
            console.log('Given Name: ' + profile.getGivenName());
            console.log('Family Name: ' + profile.getFamilyName());
            console.log("Image URL: " + profile.getImageUrl());
            console.log("Email: " + profile.getEmail());*/

            // The ID token you need to pass to your backend:
            var id_token = googleUser.getAuthResponse().id_token;
          //  console.log("ID Token: " + id_token);
          $('#logingoogle').remove();
          $('#login').remove();
          $('#textoconec').remove();
          $('#google-sesion').removeClass('hidden');
          $('#google-sesion strong').text('Bienvenido: '+profile.getName());
          $('#google-sesion img').attr('src', profile.getImageUrl());


            function Datos(id, email,cuenta, nombre,foto)   //
            {
              this.id = id;
              this.email = email;
              this.cuenta = cuenta;
              this.nombre = nombre;
                this.foto = foto; //
              }

            var dato = new Datos(profile.getId(),profile.getEmail(),'google',profile.getName(),profile.getImageUrl()); //
            // Creamos un arreglo para almacenarlos
            var listaDatos = [];

            listaDatos.push(dato);
            
             // Convertimos la lista de objetos en una cadena con el formato JSON
             var datosJSON = JSON.stringify(listaDatos);

              // Realizamos la petición al servidor
              $.post('loginpost.php', {datos: datosJSON},
                function(respuesta) {
                     // console.log(respuesta);


                     loadingOFF();          
                   }).error(
                   function(){
                     // console.log('Error al ejecutar la petición de ajax a la pagina php');
                     loadingOFF();
                   });

                 };


                 function signOut() {

                  var auth2 = gapi.auth2.getAuthInstance();
                  auth2.signOut().then(function () {
            //  console.log('User signed out.');

           // $('#google-sesion').addClass('hidden');

           location.href ="logeo.php";
         });
                }

                $('#loginface').removeClass('hidden');
                $('#logingoogle').removeClass('hidden');
loadingOFF();
</script>
</body>
</html>