    
<?php

if(session_id() == '' || !isset($_SESSION)) {
  session_start();
}

if (!(isset($_SESSION['iniciado']))) {
  header ("Location: logeo.php");
  exit();
}

if ($_SESSION['tipo'] != 'A'){
  header ("Location: logeo.php");
  exit();
}


if ($_SESSION['iniciado'] != '5dbc98dcc983a70728bd082d1a47546e'){
  header ("Location: logeo.php");
  exit();

}

?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Drinky || Admin - Locales</title>

     <!--LOADING --> 
    <?php include("loading.php"); ?>
    <!--LOADING -->

  <!-- Bootstrap Core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom CSS -->
  <link href="css/sb-admin.css" rel="stylesheet">
  
  <!-- Custom Fonts -->
   <link rel="icon" type="image/png" href="images/drinky-logo.png" />
  <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="css/admin-general.css" rel="stylesheet" type="text/css">
  <link type="text/css" rel="stylesheet" href="css/chosen.min.css">

<style>
.modal-dialog {

  padding-top:15%;
}

.modal-content {
  height: 100% !important;
  overflow:visible;
}

.modal-body {
  height: 80%;
  overflow: auto;
}

</style>

</head>

<body>

  <div id="wrapper">

    <?php include('admin-header.php'); ?>

    <div id="page-wrapper">

      <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
          <div class="col-lg-12">
            <h1 class="page-header">
              LOCALES

            </h1>
            <ol class="breadcrumb" >
              <li>
                <i class="fa fa-dashboard"></i>  <a href="admin-index.php">Tablero</a>
              </li>
              <li class="active">
                <i class="fa fa-file"></i> Eliminar Local
              </li>
            </ol>


            <div class="page-content">
              <div id="tab-general" style="height:100vh;">
                <div class="col-lg-1 col-xs-12 col-sm-12"><!--relleno-->
                </div>

                <div class="col-md-12 col-lg-10 col-xs-12 col-sm-12">
                  <br>
                  <div class="panel panel-grey">
                    <div class="panel-heading">
                      <h1>Eliminar Local</h1>
                    </div>
                    <div class="panel-body pan">
                      <!--h2 style="color:#FFF; background-color:#3C3C3C; padding:5px; padding-left:15px; margin-right:5px; margin-left:5px">&nbsp;Buscar un Bar:</h2> -->
                      <br>

                      <input type="hidden" id="usuariocarga" name="usuariocarga" value="<?php echo $_SESSION['usuario']; ?>">
                      <div  class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding">
                        <label class="col-sm-12 control-label"><i class="glyphicon glyphicon-chevron-right color-oficial"></i>&nbsp;Seleccione Local:</label>
                        <table><tr ><td width="600px">
                          <div id="divcombobar" class="col-sm-12">
                            <select class="form-control chosen-select" id="b_cmbbar"  name="b_cmbbar">
                             <option value="0">Seleccionar Un Local</option>
                             <?php 

                             include('conexion/conexion.php');

                             $conection = mysqli_connect($host, $usuario, $pass,$db) or die("error de conexion a DB");

                             $select = $conection->query("SELECT idbar,nombre_bar, tipolocal FROM bares ORDER BY activo desc, tipolocal, nombre_bar");

                             while ($result = mysqli_fetch_assoc($select)) 
                             {
                              ?>     
                              <option value="<?php echo $result["idbar"];?>">
                               <?php echo $result["tipolocal"] . " " . $result["nombre_bar"]  . ", " . $result["idbar"] . "</option>";

                             }
                             $select->close();
                             $conection->next_result();

                             ?>

                           </select>

                         </div>
                       </td>
                       <td>
                        <button style="text-align:center" data-toggle="modal" type="submit" href="#modaleliminar"  id="e_btneliminar" name="e_btneliminar" type="button" class="pull-right btn btn-danger btn-lg"><i class="fa fa-remove"></i>&nbsp;&nbsp;Eliminar</button>

                      </td></tr>
                    </table>
                  </div>

                  <div class="row">
                    <div id="divcontenido" style="padding:15px" class="col-lg-12">
                     <div class="alert alert-warning hidden" id="msgseleccione"><button class="close" data-dismiss="alert"><span>&times;</span></button>
                       <st>                                  <span class="glyphicon glyphicon-info-sign"></span><b> Atención!</b> Debe seleccionar un Local a eliminar</st>
                     </div>
                     <div class="alert alert-success hidden" style="text-align:center" id="msgeliminado"><button class="close" data-dismiss="alert"><span>&times;</span></button>
                       <st><span class="glyphicon glyphicon-saved"></span><b> Local eliminado de la base de datos.</b></st>
                     </div> 
                     <div class="alert alert-danger hidden" style="text-align:center" id="msgerror"><button class="close" data-dismiss="alert"><span>&times;</span></button>
                       <st><span class="glyphicon glyphicon-remove"> </span><b> Error! </b> No se pudo eliminar el local o ya esta eliminado.</st>
                     </div>  
                    <div class="alert alert-success hidden" style="text-align:center" id="msg_reg_eliminado"><button class="close" data-dismiss="alert"><span>&times;</span></button>
                       <st><span class="glyphicon glyphicon-saved"></span><b> Registro de Cordenada del Local Eliminado.</b></st>
                     </div> 
                     <div class="alert alert-danger hidden" style="text-align:center" id="msge_error_regeliminado"><button class="close" data-dismiss="alert"><span>&times;</span></button>
                       <st><span class="glyphicon glyphicon-remove"> </span><b> Error! </b> No se pudo eliminar el Registro de Cordenadas del Local Seleccionado</st>
                     </div>  
                   </div>
                 </div>


               </div>
             </div>

             <div class="modal fade" style="position:center;" id="modaleliminar">  

              <div class="modal-dialog">
                <div class="modal-content">


                  <div class="modal-header">
                    <a type="button" class="close"  data-dismiss="modal" aria-hidden="true">&times;</a> 
                    <h4 class="modal-title" id="titulomodalreservas" style="text-align:center;" ><b>Confirmar eliminar Local</b></h4>

                  </div>


                  <div class="modal-footer"  style="text-align:center;">
                    <a class="btn btn-success btn-lg" id="btnsi"  style="text-align:center;" onclick="eliminarbar()"><b>Si <span class="glyphicon glyphicon-ok"></span></b>
                      <a type="button" class="btn btn-lg btn-danger"  style="text-align:center;" data-dismiss="modal">No</a>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>


        </div>


      </div>
      <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

  </div>
  <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="js/jquery.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>
<script src="js/chosen.jquery.min.js"></script>

   <script>
    function loadingON(){
    $('#loadingDiv').removeClass('hidden');
   }
   function loadingOFF(){
    $('#loadingDiv').addClass('hidden');
    }
   </script>

<script>



        //INICIALIZO LOS CHOSEN SELECT 
        function iniciarchosen(){
             $(".chosen-select").chosen({
                no_results_text: "No hay resultados!",
                placeholder_text_single: "Seleccionar",
                placeholder_text_multiple: "Seleccionar"
        });
        }
        iniciarchosen();
        
function eliminarbar(){

 var b_idbar = $('#b_cmbbar').val();
 if (b_idbar != 0){
  $.ajax({
    url: 'ajax/admin_eliminarbar.ajax.php',
    data: {'barid':b_idbar},
    type: 'POST',
    dataType: 'json',

             beforeSend: function () {
              $('body').addClass('loading'); //Agregamos la clase loading al body
            },

    success: function( data ) {
      console.log('peticion lograda con exito');
      console.log(data);

      if (data['eliminado']==1){
        $('#msgseleccione').addClass('hidden');
        $('#msgerror').addClass('hidden');
        $('#msgeliminado').removeClass('hidden');
        eliminarRegistrodeCordenadas(b_idbar);
        actualizarComboBares();
        $('#modaleliminar').modal('hide');
      }else{
        $('#msgseleccione').addClass('hidden');
        $('#msgeliminado').addClass('hidden');
        $('#msgerror').removeClass('hidden');
        $('#modaleliminar').modal('hide');
      }

    },             
    error: function( data ) {
      console.log('se ejecuto mal la peticion');
      console.log(data);
      $('#msgeliminado').addClass('hidden');
      $('#msgerror').removeClass('hidden');
      $('#modaleliminar').modal('hide');

      $('body').removeClass('loading'); //Removemos la clase loading
    }, 
  });

}else{
  $('#modaleliminar').modal('hide');
  $('#msgeliminado').addClass('hidden');
  $('#msgeliminado').addClass('hidden');
  $('#msgseleccione').removeClass('hidden');
}

}

function eliminarRegistrodeCordenadas(id_bar){
$.ajax({
  url: 'ajax/admin_eliminar_registro_cordenada.ajax.php',
  data: {'id':id_bar},
  type: 'POST',
  dataType: 'json',

  success: function( data ) {
    console.log('peticion lograda con exito');
    console.log(data);

      if (data['reg_cord_eliminada'] == 1){

      $('#msge_error_regeliminado').addClass('hidden');
      $('#msg_reg_eliminado').removeClass('hidden');
    
      }else{

      $('#msg_reg_eliminado').addClass('hidden');
      $('#msge_error_regeliminado').removeClass('hidden');
     
      }

     },             
      error: function( data ) {
      console.log('se ejecuto mal la peticion');
      console.log(data);

        $('#msg_reg_eliminado').addClass('hidden');
        $('#msge_error_regeliminado').removeClass('hidden');

      }, 
      });
}


function actualizarComboBares(){

  $.ajax({
    url: 'ajax/admin_actualizarcmbbares.ajax.php',
    type: 'POST',
    dataType: 'json',

    success: function( data ) {
      console.log('peticion lograda con exito');
      console.log(data);
      $('#divcombobar').empty();
     
      $('#divcombobar').append('<select class="form-control chosen-select" id="b_cmbbar" name="b_cmbbar">');
     
      
      $('#b_cmbbar').append('<option value="0">Seleccione un Local</option>');

      for(var i = 0; i < data.length; i++) {
       $("#b_cmbbar").append('<option value="'+data[i][0]+'">'+data[i][0]+', '+data[i][2]+ ' ' +data[i][1]+'</option>');
     }

     $('#divcombobar').append('</select>');
      iniciarchosen();

     $('body').removeClass('loading'); //Removemos la clase loading
   },             
   error: function( data ) {
    console.log('se ejecuto mal la peticion');
    console.log(data);

    $('body').removeClass('loading'); //Removemos la clase loading

  }, 
})
}
</script>
<script> loadingOFF();</script>
<div class="modalloading"></div>
</body>

</html>

