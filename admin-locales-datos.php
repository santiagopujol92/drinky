	
<?php

if(session_id() == '' || !isset($_SESSION)) {
  session_start();
}

if (!(isset($_SESSION['iniciado']))) {
  header ("Location: logeo.php");
  exit();
}

if ($_SESSION['tipo'] != 'A'){
  header ("Location: logeo.php");
  exit();
}


if ($_SESSION['iniciado'] != '5dbc98dcc983a70728bd082d1a47546e'){
  header ("Location: logeo.php");
  exit();

}

?>

        	<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Drinky || Admin - Locales</title>

     <!--LOADING --> 
    <?php include("loading.php"); ?>
    <!--LOADING -->
    
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
     <link rel="icon" type="image/png" href="images/drinky-logo.png" />
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	 <link href="css/admin-general.css" rel="stylesheet" type="text/css">
    <link type="text/css" rel="stylesheet" href="css/chosen.min.css">
</head>



<body>

    <div id="wrapper">

<?php include('admin-header.php'); ?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            LOCALES
    
                        </h1>
                        <ol class="breadcrumb" >
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="admin-index.php">Tablero</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-file"></i> Datos Locales
                            </li>
                        </ol>

                         
			   <div class="page-content">
                    <div id="tab-general" style="height:100vh;">
                        <div class="col-lg-1 col-xs-12 col-sm-12"><!--relleno-->
                        </div>
                        
                        <div class="col-md-12 col-lg-10 col-xs-12 col-sm-12">
                            <br>
                            <div class="panel panel-grey">
                                <div class="panel-heading">
                                    <h1>Datos Locales</h1>
                                </div>
                                <div class="panel-body pan">
                                	<!--h2 style="color:#FFF; background-color:#3C3C3C; padding:5px; padding-left:15px; margin-right:5px; margin-left:5px">&nbsp;Buscar un Local:</h2> -->
                                    <br>

                                    <input type="hidden" id="usuariocarga" name="usuariocarga" value="<?php echo $_SESSION['usuario']; ?>">
                                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                                        <label class="col-sm-12 control-label"><i class="glyphicon glyphicon-chevron-right color-oficial"></i>&nbsp;Local:</label>
                                        <div class="col-sm-12">
                                            <select class="form-control chosen-select" id="b_cmbbar"  name="b_cmbbar">
                                                 <option value="0">Seleccione un Local</option>
                                                 <option value="Todos">Todos los Locales</option>
                                                 <option value="Bares">Bares</option>
                                                 <option value="Boliches">Boliches</option>
                                                 <?php 

                                                 include('conexion/conexion.php');

                                                 $conection = mysqli_connect($host, $usuario, $pass,$db) or die("error de conexion a DB");

                                                 $select = $conection->query("SELECT idbar,nombre_bar,tipolocal FROM bares ORDER BY activo desc, tipolocal, nombre_bar");

                                                 while ($result = mysqli_fetch_assoc($select)) 
                                                 {
                                                  ?>     
                                                  <option value="<?php echo $result["idbar"];?>">
                                                   <?php echo $result["tipolocal"] . " " . $result["nombre_bar"] . ", " . $result["idbar"] . "</option>";

                                                 }
                                                 $select->close();
                                                 $conection->next_result();

                                                 ?>
                                             
                                            </select>
                                        </div>
                                    </div>
                                    
                                    
                                    
                                                      <div class="row">
                                                        <div id="divcontenido" style="padding:15px" class="col-lg-12">
       
                                                        </div>
                                                    </div>

                                             
                                            </div>
                                        </div>

                        <div class="col-lg-1 col-xs-12 col-sm-12"> <!--relleno-->
                        </div>
                    </div>
                </div>
                        
                        
                    </div>

                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/chosen.jquery.min.js"></script>

   <script>
    function loadingON(){
    $('#loadingDiv').removeClass('hidden');
   }
   function loadingOFF(){
    $('#loadingDiv').addClass('hidden');
    }
   </script>

    <script>

        //INICIALIZO LOS CHOSEN SELECT
        $(".chosen-select").chosen({
                no_results_text: "No hay resultados!",
                placeholder_text_single: "Seleccionar",
                placeholder_text_multiple: "Seleccionar"
        });
        //MOSTRAR DATOS LOCAL SELECCIONADO
        $("select[id=b_cmbbar]").change(function(){
         var b_idbar = $('#b_cmbbar').val();

         if (b_idbar == 0){
          $('#divcontenido').empty();
          return false;  
         }
         
        $.ajax({
              url: 'ajax/admin_datosbares.ajax.php',
              data: {'barid':b_idbar},
              type: 'POST',
              dataType: 'json',

             beforeSend: function () {
              $('body').addClass('loading'); //Agregamos la clase loading al body
            },

              success: function( data ) {
                console.log('peticion lograda con exito');
                console.log(data);

                $('#divcontenido').empty();
                $('#divcontenido').append('<div class="table-responsive"><table class="table table-bordered table-hover">'
                 +'<thead><tr class="warning"><th>'+data[0][0]+'</th><th>'+data[0][1]+'</th><th>'+data[0][9]+'</th><th>'+data[0][2]+'</th><th>'+data[0][3]+'</th>'
                 +'<th>'+data[0][4]+'</th><th>'+data[0][5]+'</th><th>'+data[0][6]+'</th> <th>'+data[0][7]+'</th><th>'+data[0][8]+'</th>'
                 +'</tr></thead><tbody id="tablebody">');

                   var contarfilasdesactivas = 0;
                   var contarfilasbar= 0;
                   var contarfilasboli = 0;
                   var totalfilas = 0;
                   var estado = 'default';
                    for(var i = 1; i < data.length; i++) {
                      if (data[i][9] == 'Boliche'){
                        estado = 'info';
                        var contarfilasboli= contarfilasboli+1;
                      }
                      else if (data[i][9] == 'Bar'){
                        estado = 'success';  
                        var contarfilasbar= contarfilasbar+1;
                      }
                      if (data[i][8] == "N"){
                        estado = 'danger';
                        var contarfilasdesactivas = contarfilasdesactivas+1;
                      }
                      totalfilas=totalfilas+1;

                    $('#tablebody').append('<tr class="'+estado+'"><td>'+data[i][0]+'</td><td>'+data[i][1]+'</td><td>'+data[i][9]+'</td><td>'+data[i][2]+'</td>'
                    +'<td>'+data[i][3]+'</td> <td>'+data[i][4]+'</td><td>'+data[i][5]+'</td><td>'+data[i][6]+'</td>'
                    +'<td>'+data[i][7]+'</td><td>'+data[i][8]+'</td></tr>');
                    }
                $('#divcontenido').append('</tbody></table></div>');

              $('#divcontenido').append('<div><h4 class="text-success">Bares: <b>'+contarfilasbar+'</b></h4><h4 class="text-info">Boliches: <b>'+contarfilasboli+'</b></h4><h4 class="text-danger">Locales Inactivos: <b>'+contarfilasdesactivas+'</b></h4><h4>Total Locales: <b>'+totalfilas+'</b></h4></div>');

             $('body').removeClass('loading'); //Removemos la clase loading

               },             
              error: function( data ) {
                console.log('se ejecuto mal la peticion');
                console.log(data);

               $('body').removeClass('loading'); //Removemos la clase loading

              }, 
            })
        });

    </script>
<script>loadingOFF();</script>
<div class="modalloading"></div>
</body>

</html>

