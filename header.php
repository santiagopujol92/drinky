
<?php

    if(session_id() == '' || !isset($_SESSION)) {
        session_start();
    }

if ($_SESSION['estado'] != 'S'){
  header ("Location: logeo.php");
  exit();
}

    if (!(isset($_SESSION['iniciado']))) {
        header ("Location: logeo.php");
        exit();
    }
        
    if ($_SESSION['iniciado'] != '5dbc98dcc983a70728bd082d1a47546e'){
        header ("Location: logeo.php");
        exit();
        
    }

function echoActiveClassIfRequestMatches($requestUri)
{
    $current_file_name = basename($_SERVER['REQUEST_URI'], ".php");

    if ($current_file_name == $requestUri)
        echo ' activo';
}

function echoActiveClassIfRequestM($requestUri)
{
    $current_file_name = basename($_SERVER['REQUEST_URI'], ".php");

    if ($current_file_name == $requestUri)
        echo 'class="active"';
}


?>  


<header class="">

    <nav class="navbar navbar-fixed-top" role="navigation" >

     <a href="logeo.php" id="brand"><img  id="logo" style="float:left;border:0px;margin-left:30px;margin-top:12px;margin-bottom:8px;padding:0px;" src="images/drinky-horizontal-min.jpg" onmouseout="sacarborde()" onmouseover="cambiarbordefoto()" class="img-thumbnail" width="200px" height="80" ></img></a>
   
        <div class="container-fluid">

            <div class="navbar-header">

                <button id="btnmenu" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navegacion-fm">
                    <h4><b><span class="sr-only">Desplegar / Ocultar Menu</span></b></h4>
                    <h4><span class="glyphicon glyphicon-align-justify"></span></h4>
                 

                </button>
    
            </div>
            <!-- INICIA MENU -->

            <div class="collapse navbar-collapse" id="navegacion-fm">
                <ul class="nav navbar-nav" >
                
                    <li>
                    <a href="index.php" id="menuinicio" class="navbar-header" style="background:#1c1c1c;"><h4><b><span class="glyphicon glyphicon-search"></span> Iniciar</b></h4></a>
                    </li>
                    
                    <li>
                        <a id="menulocales" href="mislocales.php"  role="button" style="background:#1c1c1c;">
                           <h4><b><span class="glyphicon glyphicon-home"></span>   Mis Locales</b></h4>
                        </a>

                    </li>

                    
                    <li>
                        <a id="menureservas" href="misreservas.php" class="dropdown-toggle menu" style="background:#1c1c1c;" role="button">
                           <h4><b><span class="glyphicon glyphicon-list-alt"></span>   Mis Reservas</b></h4>
                        </a>

                    </li>

                  
                   <li>
                    <a href="contacto.php" id="menucontacto" class="navbar-header" style="background:#1c1c1c;"><h4><b><span class="glyphicon glyphicon-earphone"></span>  Contacto</b></h4></a>
                    </li>


                     <li  class="dropdown">
                      <a id="menunomusu" class="dropdown-toggle" data-toggle="dropdown" role="button" style="background:#1c1c1c;" >
                        <h4><b><span class="glyphicon glyphicon-user"></span> <?php echo $_SESSION['nombre'];?><i class="fa fa-fw fa-caret-down"></i></b></h4>
                      </a>
                        <ul class="dropdown-menu" style="overflow:auto;">
                            <?php 
                            if ($_SESSION['tipo'] == 'A'){
                      echo '<li>
                                <a href="admin-index.php" class="azul" style="background:#1c1c1c;">
                                    <h4><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-log-out"></span>   Panel de Admin</b></h4>
                                </a>
                            </li>';
                            }
                            ?>
                            <li>
                                <a href="logout.php" id="salir" style="overflow:auto;" class="azul" style="background:#1c1c1c;">
                                    <h4><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-log-out"></span>   Salir</b></h4>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li id="menufotousu" class="hidden-xs">
                        <img src="<?php echo $_SESSION['foto'];?>" class="img-responsive img-circle" width ="90"></img>
                    </li>  
             </ul>
             
                        </div>
                    </div>

                </nav>

        </header>
<br>

<script>
function cambiarbordefoto(){
$('#logo').css('border-bottom','4px solid #6CDAF8');
}
function sacarborde(){
  $('#logo').css('border-bottom','4px solid #1c1c1c');  
}
</script>
      