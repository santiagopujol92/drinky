

/* MODAL DE CARTA */


 var idbar;

    $(document).on("click", ".open-Modal", function () {
    
      $('#contmodal').empty();
      $('#titulobar').html('<h4 class="modal-title" id="titulobar"><h4><b>Detalles de la Carta:</b></h4>');
      $("#contmodal").append("<p>La carta está vacía</p>"); 

      idbar = $(this).data("id");
      $(".modal-body #idbar").val(idbar);


      $.ajax({
        url: 'ajax/procesarcarta.ajax.php',
        data: {'idbar':idbar},
        type: 'POST',
        dataType: 'json',

             beforeSend: function () {
             $('body').addClass('loading'); //Agregamos la clase loading al body
            },
   
        success: function( data ) {
          console.log('peticion lograda con exito');
          console.log(data);

          if (data['vacio'] == 1){    //SI NO TIENE PRODUCTOS EN LA CARTA SALE
              $('body').removeClass('loading'); //Removemos la clase loading
                return false;
          }else{
         
                $('#titulobar').html('<h4 class="modal-title" id="titulobar"><h4 id="colorbar"><b>'+data[1][5]+' '+data[1][3]+'</b></h4><b>Detalles de la Carta:</b></h4>');
                $('#contmodal').empty(); 

                for(var i = 1; i < data.length; i++) {
                  if (data[i][2] == 'Bebidas'){
                    $("#contmodal").append("<p><u><b>" + data[i][2] +":</b></u></p>");  
                    break;
                  }
                }
                for(var j = 1; j < data.length; j++) {
                  if (data[j][2] == 'Bebidas'){
                   $("#contmodal").append("<p>- "+ data[j][0] + ": $" + data[j][1] +"</p>");

                 }
               }

               for(var i2 = 1; i2 < data.length; i2++){
                if(data[i2][2]== 'Comidas'){
                  $("#contmodal").append("<br><p><u><b>" + data[i2][2] +":</b></u></p>");  
                  break;
                }
              }

              for(var j2 = 1; j2 < data.length; j2++) {
                if (data[j2][2] == 'Comidas'){
                 $("#contmodal").append("<p>- "+ data[j2][0] + ": $" + data[j2][1] +"</p>");

               }
             }

             for(var i3 = 1; i3 < data.length; i3++){
              if(data[i3][2]== 'Promociones'){
                $("#contmodal").append("<br><p><u><b>" + data[i3][2] +":</b></u></p>");  
                break;
              }
            }

            for(var j3 = 1; j3 < data.length; j3++) {
              if (data[j3][2] == 'Promociones'){
               $("#contmodal").append("<p>- "+ data[j3][0] + ": $" + data[j3][1] +"</p>");

             }
           }

       $('body').removeClass('loading'); //Removemos la clase loading
      }

   },             
   error:  function( data ) {

    console.log('se ejecuto mal la peticion');
    console.log(data);

     $('body').removeClass('loading'); //Removemos la clase loading
  }                          
})

/* MODAL DE CARTA-RESERVA */


document.getElementById("confirmarreserva").addEventListener("click",datosreserva,false);

});



function abrirreserva(){ 
  $("#reservar").removeClass('hidden');
}

function cerrarmodalreserva(){
  $("#reservar").addClass('hidden'); 
}

function abrircartasinreserva(){ 
  $("#reservar").addClass('hidden');
  $('#completecant').addClass('hidden');
  $('#completefecha').addClass('hidden');
  $('#completetel').addClass('hidden');
  $('#msgreservado').addClass('hidden');
  $('#msgregistrada').addClass('hidden'); 

  $('#personas').val("");
  $('#fechahora').val("");
  $('#telefono').val("");
}

function datosreserva(){

  var personas = document.getElementById("personas").value;
  var fechahora =document.getElementById("fechahora").value;
  var telefono = document.getElementById("telefono").value;


  if(!isNaN(personas) && personas != "" && fechahora != "" && telefono != "" && !isNaN(telefono) ){

    $('#completecant').addClass('hidden');
    $('#completefecha').addClass('hidden');
    $('#completetel').addClass('hidden');
    $('#msgreservado').addClass('hidden');
    $('#msgregistrada').addClass('hidden'); 



    datosreserva = new Array();
    datosreserva[0] = idbar;
    datosreserva[1] = fechahora;
    datosreserva[2] = personas;
    datosreserva[3] = telefono;


    $.ajax({
      url: 'ajax/procesarreserva.ajax.php',
      data: {'dato0':datosreserva[0],'dato1':datosreserva[1],'dato2':datosreserva[2],'dato3':datosreserva[3]},
      type: 'POST',
      dataType: 'json',

           beforeSend: function () {
             $('body').addClass('loading'); //Agregamos la clase loading al body
            },
          
      success: function( data ) {
        console.log('peticion lograda con exito');
        console.log(data);

        $(document).ready(function() {

         if(data['reservado'] == 1){
           $("#msgreservado").removeClass('hidden');
             $('body').removeClass('loading'); //Removemos la clase loading
         }
         if(data['reservado'] == 0){
           $('#msgregistrada').removeClass('hidden');
             $('body').removeClass('loading'); //Removemos la clase loading
         }

       });

      },             
      error:  function( data ) {

        console.log('se ejecuto mal la peticion');
        console.log(data);

          $('body').removeClass('loading'); //Removemos la clase loading

      }                          
    })      

  }

  if(personas == "" || isNaN(personas) ) {
    $('#completecant').removeClass('hidden');
    $('#completefecha').addClass('hidden');
    $('#completetel').addClass('hidden');

  }

  if(fechahora == "" ){
    $('#completefecha').removeClass('hidden');
    $('#completecant').addClass('hidden');
    $('#completetel').addClass('hidden');

  }

  if (telefono == "" || isNaN(telefono) || telefono.length > 15){
    $('#completetel').removeClass('hidden');
    $('#completecant').addClass('hidden');
    $('#completefecha').addClass('hidden');

  }

}

/* MODAL DE FOTOS */

var barid;

$(document).on("click", ".modalfotos", function () {

  barid = $(this).data("id");
  $(".modal-body #idbar").val(barid);

  $.ajax({
    url: 'ajax/procesaralbum.ajax.php',
    data: {'barid':barid},
    type: 'POST',
    dataType: 'json',

           beforeSend: function () {
             $('body').addClass('loading'); //Agregamos la clase loading al body
            },

    success: function( data ) {
      console.log('peticion lograda con exito');
      console.log(data);

      $('#tituloyfotosbar').empty();
      $('#tituloyfotosbar').html('<h4 class="modal-title" id="titulofotosbar"><h4 id="colorbar"><b>'+data[0][2]+' '+data[0][0]+'</b></h4><h4><b>Mas Fotos del '+data[0][2]+':</b></h4><img src="images/'+data[0][1]+'/1.jpg" alt="" class="img-responsive" width="1000"><br><img src="images/'+data[0][1]+'/2.jpg" alt="" class="img-responsive" width="1000"><br><img src="images/'+data[0][1]+'/3.jpg" alt="" class="img-responsive" width="1000">');
       
      $('body').removeClass('loading'); //Removemos la clase loading
    },             
    error: function( data ) {
      console.log('se ejecuto mal la peticion');
      console.log(data);
      $('#tituloyfotosbar').html('<h4 class="modal-title" id="tituloyfotosbar"><b>Fotos del Bar:</b></h4><br><p>Este Bar no tiene mas fotos.</p>');
      
      $('body').removeClass('loading'); //Removemos la clase loading
    }                         
  })

});


/* FUNCIONES DE API GOOGLE MAPS*/

var map;
var id_bar;
var marker;
var place;

$(document).on("click", ".abrirmapa", function () {

  id_bar = $(this).data("id");
  $("#map #idbar").val(id_bar);

  $.ajax({
    url: 'ajax/procesarmapa.ajax.php',
    data: {'idbar':id_bar},
    type: 'POST',
    dataType: 'json',

           beforeSend: function () {
             $('body').addClass('loading'); //Agregamos la clase loading al body
            },

    success: function( data ) {
      console.log('peticion lograda con exito');
      console.log(data);

      $('#titulomapabar').empty();
      $('#map').empty();
      $('#map').removeClass('hidden'); 
      $('#titulomapabar').append('<h4 class="centrar"><span><b>'+data[1][4]+' '+data[1][0]+'</b></span></h4><br>')

      $('body').removeClass('loading'); //Removemos la clase loading
      var mapsOptions = {
        center: new google.maps.LatLng(data[1][2], data[1][3]),
        zoom: 18,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }

      map = new google.maps.Map(document.getElementById('map'),mapsOptions);

           place = new google.maps.LatLng(data[1][2], data[1][3]); //PUNTO
           marker = new google.maps.Marker({
            position: place,
            map: map,
          }); 

         },             
         error:  function( data ) {
          console.log('se ejecuto mal la peticion');
          console.log(data);
          $('#titulomapabar').append('<h4 class="centrar"><span><b>Este Local no tiene registrada su direccion.</span></h4><br>')
        
         $('body').removeClass('loading'); //Removemos la clase loading
        }                          
      })   
    
});

function scrolear_mapa(){
    $("html, body").animate({scrollTop: $(document).height()}, 1000);
}