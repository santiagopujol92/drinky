$(function() {

	var app_id = '957051937754664';
	var scopes = 'email';

  var conectarse= '<p id="textoconec">Conectarse:</p>';
 
  var btn_login = '<div class=""><a href="#" id="login" class="btn btn-social btn-facebook"><span class="fa fa-facebook"></span>Acceder Con Facebook</a></div>';
	var div_session = "<div id='facebook-session'>"+
  "<a href='logout.php' id='logout'   class='btn btn-social btn-facebook' ><span class='fa fa-facebook'></span>Cerrar Sesión de Facebook</a>"+
					  "<br><br><strong></strong>"+
					  "<img>"+
					  ""+
            "<div id='ingresar' class='expandir'><br><a href='index.php' width=500 id='btningresar' class='btn'><h3><b>Ingresar</b></h3><span class='glyphicon glyphicon-glass'></span></a></div></div>";
  

  window.fbAsyncInit = function() {

	  	FB.init({
	    	appId      : app_id,
	    	status     : true,
	    	cookie     : true, 
	    	xfbml      : true, 
	    	version    : 'v2.5'
	  	});


	  	FB.getLoginStatus(function(response) {
	    	statusChangeCallback(response, function() {});
	  	});
  	};

  	var statusChangeCallback = function(response, callback) {
  		//console.log(response);
   		
    	if (response.status === 'connected') {

      		getFacebookData();
    	} else {
     		callback(false);
    	}
  	}

  	var checkLoginState = function(callback) {
    	FB.getLoginStatus(function(response) {
      		callback(response);

    	});
  	}

  	var getFacebookData =  function() {
      var url = '/me?fields=id,name,email';

  		FB.api(url, function(response) {

	  		$('#login').after(div_session);
	  		$('#login').remove();
        $('#logingoogle').addClass('hidden');
        $('#textoconec').remove();

	  		$('#facebook-session strong').text("Bienvenido: "+response.name);
	  		$('#facebook-session img').attr('src','http://graph.facebook.com/'+response.id+'/picture?type=large');
        // console.log("ID: "+ response.id);
        // console.log("Name: "+response.name);
        // console.log("Email: "+response.email);

          function Datos(id, email, cuenta, nombre, foto)   //
            {
                this.id = id;
                this.email = email;
                this.cuenta = cuenta;
                this.nombre = nombre;
                this.foto = 'http://graph.facebook.com/'+foto+'/picture?type=small'; // 
            }

         var dato = new Datos(response.id,response.email,'facebook', response.name, response.id); //
            // Creamos un arreglo para almacenarlos
            var listaDatos = [];
             
            listaDatos.push(dato);
            
             // Convertimos la lista de objetos en una cadena con el formato JSON
              var datosJSON = JSON.stringify(listaDatos);
               
              // Realizamos la petición al servidor
              $.post('loginpost.php', {datos: datosJSON},
                  function(respuesta) {
                      // console.log(respuesta);

              }).error(
                  function(){
                      // console.log('Error al ejecutar la petición de ajax a la pagina php');
                  }
              );
	  	});
  	}

        

  	var facebookLogin = function() {
  		checkLoginState(function(data) {
  			if (data.status !== 'connected') {
  				FB.login(function(response) {
  					if (response.status === 'connected')
              
  						getFacebookData();

              
  				}, {scope: scopes});
  			}
  			
  		})
  	}

    
  	var facebookLogout = function() {
  		checkLoginState(function(data) {
  			if (data.status === 'connected') {
				FB.logout(function(response) {
					$('#facebook-session').before(btn_login);
					$('#facebook-session').remove();
          $('#conectarse').before(conectarse);
          $('#logingoogle').removeClass('hidden');

          
          
				})
			}
  		})

  	}
  

  	$(document).on('click', '#login', function(e) {
  		e.preventDefault();

  		facebookLogin();

  	})

  	$(document).on('click', '#logout', function(e) {
  		e.preventDefault();
 		
  			facebookLogout();        
        }) 
  	})




  // GOOOGLEEE
    
        function onSignIn(googleUser) {
            // Useful data for your client-side scripts:
            var profile = googleUser.getBasicProfile();
            // console.log("ID: " + profile.getId()); // Don't send this directly to your server!
            // console.log('Full Name: ' + profile.getName());
            // console.log('Given Name: ' + profile.getGivenName());
            // console.log('Family Name: ' + profile.getFamilyName());
            // console.log("Image URL: " + profile.getImageUrl());
            // console.log("Email: " + profile.getEmail());

            // The ID token you need to pass to your backend:
            var id_token = googleUser.getAuthResponse().id_token;
          //  console.log("ID Token: " + id_token);
            $('#logingoogle').remove();
            $('#login').remove();
            $('#textoconec').remove();
            $('#google-sesion').removeClass('hidden');
            $('#google-sesion strong').text('Bienvenido: '+profile.getName());
            $('#google-sesion img').attr('src', profile.getImageUrl());


            function Datos(id, email,cuenta, nombre,foto)   //
            {
                this.id = id;
                this.email = email;
                this.cuenta = cuenta;
                this.nombre = nombre;
                this.foto = foto; //
            }

            var dato = new Datos(profile.getId(),profile.getEmail(),'google',profile.getName(),profile.getImageUrl()); //
            // Creamos un arreglo para almacenarlos
            var listaDatos = [];
             
            listaDatos.push(dato);
            
             // Convertimos la lista de objetos en una cadena con el formato JSON
              var datosJSON = JSON.stringify(listaDatos);
               
              // Realizamos la petición al servidor
              $.post('loginpost.php', {datos: datosJSON},
                  function(respuesta) {
                      // console.log(respuesta);

              }).error(
                  function(){
                      // console.log('Error al ejecutar la petición de ajax a la pagina php');
                  }
              );
            
        };


        function signOut() {
            var auth2 = gapi.auth2.getAuthInstance();
            auth2.signOut().then(function () {
              // console.log('User signed out.');
              $('#google-sesion').addClass('hidden');
            });
        }
