	
<?php

if(session_id() == '' || !isset($_SESSION)) {
  session_start();
}

if (!(isset($_SESSION['iniciado']))) {
  header ("Location: logeo.php");
  exit();
}

if ($_SESSION['tipo'] != 'A'){
  header ("Location: logeo.php");
  exit();
}


if ($_SESSION['iniciado'] != '5dbc98dcc983a70728bd082d1a47546e'){
  header ("Location: logeo.php");
  exit();

}

?>

        	<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Drinky || Admin - Productos</title>

     <!--LOADING --> 
    <?php include("loading.php"); ?>
    <!--LOADING -->
    
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
     <link rel="icon" type="image/png" href="images/drinky-logo.png" />
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	 <link href="css/admin-general.css" rel="stylesheet" type="text/css">
    <link type="text/css" rel="stylesheet" href="css/chosen.min.css">
</head>



<body>

    <div id="wrapper">

<?php include('admin-header.php'); ?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            PRODUCTOS
    
                        </h1>
                        <ol class="breadcrumb" >
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="admin-index.php">Tablero</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-file"></i> Datos Productos
                            </li>
                        </ol>

                         
			   <div class="page-content">
                    <div id="tab-general" style="height:100vh;">
                        <div class="col-lg-1 col-xs-12 col-sm-12"><!--relleno-->
                        </div>
                        
                        <div class="col-md-12 col-lg-10 col-xs-12 col-sm-12">
                            <br>
                            <div class="panel panel-grey">
                                <div class="panel-heading">
                                    <h1>Datos Productos</h1>
                                </div>
                                <div class="panel-body pan">
                                	<!--h2 style="color:#FFF; background-color:#3C3C3C; padding:5px; padding-left:15px; margin-right:5px; margin-left:5px">&nbsp;Buscar un Local:</h2> -->
                                    <br>

                                    <input type="hidden" id="usuariocarga" name="usuariocarga" value="<?php echo $_SESSION['usuario']; ?>">
                                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                                        <label class="col-sm-12 control-label"><i class="glyphicon glyphicon-chevron-right color-oficial"></i>&nbsp;Tipo de Producto:</label>
                                        <div class="col-sm-12">
                                            <select class="form-control chosen-select" id="cmbtipoprod"  name="cmbtipoprod">
                                                 <option value="0">Seleccione un Tipo de Producto</option>
                                                 <option value="Todos">Todos</option>
                                                    <?php 

                                                    include('conexion/conexion.php');

                                                    $conection = mysqli_connect($host, $usuario, $pass,$db) or die("error de conexion a DB");

                                                    $select = $conection->query("SELECT idtipo_producto,tipoprod FROM tipo_productos ORDER BY tipoprod");

                                                    while ($result = mysqli_fetch_assoc($select)) 
                                                    {
                                                        ?>     
                                                        <option value="<?php echo $result["idtipo_producto"];?>" id="opttipo<?php echo $result["idtipo_producto"];?>" title="<?php echo $result["tipoprod"];?>">
                                                         <?php echo $result["tipoprod"] . "</option>";

                                                     }
                                                     $select->close();
                                                     $conection->next_result();

                                                     ?>

                                                 </select>
                                        </div>
                                    </div>
                                    
                                                      <div class="row">
                                                        <div id="divcontenido" style="padding:15px" class="col-lg-12">
                                                  
                                                        </div>
                                                    </div>

                                             
                                            </div>
                                        </div>

                        <div class="col-lg-1 col-xs-12 col-sm-12"> <!--relleno-->
                        </div>
                    </div>
                </div>
                        
                        
                    </div>

                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/chosen.jquery.min.js"></script>

   <script>
    function loadingON(){
    $('#loadingDiv').removeClass('hidden');
   }
   function loadingOFF(){
    $('#loadingDiv').addClass('hidden');
    }
   </script>

    <script>

        //INICIALIZO LOS CHOSEN SELECT
        $(".chosen-select").chosen({
                no_results_text: "No hay resultados!",
                placeholder_text_single: "Seleccionar",
                placeholder_text_multiple: "Seleccionar"
        });
        //MOSTRAR DATOS LOCAL SELECCIONADO
        $("select[id=cmbtipoprod]").change(function(){
         var tipoprod = $('#cmbtipoprod').val();

         if (tipoprod == 0){
          $('#divcontenido').empty();
          return false;  
         }
        
        $.ajax({
              url: 'ajax/admin_actualizar_productos.ajax.php',
              data: {'tipo':tipoprod},
              type: 'POST',
              dataType: 'json',

             beforeSend: function () {
              $('body').addClass('loading'); //Agregamos la clase loading al body
            },

              success: function( data ) {
                console.log('peticion lograda con exito');
                console.log(data);

                $('#divcontenido').empty();
                $('#divcontenido').append('<div class="table-responsive"><table class="table table-bordered table-hover">'
                 +'<thead><tr class="danger"><th>Producto</th><th>Tipo de Producto</th>'
                 +'</tr></thead><tbody id="tablebody">');

                   var contarfilas = 0;
                   var contarfilascomi = 0;
                   var contarfilasbebi = 0;
                   var contarfilaspromo = 0;
                   var colortipo = 'default'; 
                    for(var i = 0; i < data.length; i++) {
                        if (data[i][2] == 'Bebidas'){
                            colortipo = 'success';
                            contarfilasbebi = contarfilasbebi+1;
                        }else if (data[i][2] == "Comidas"){
                            colortipo = 'warning';
                            contarfilascomi = contarfilascomi+1;
                        }else if (data[i][2] == "Promociones"){
                            colortipo = 'info';
                            contarfilaspromo = contarfilaspromo+1;
                        }
                    $('#tablebody').append('<tr class="'+colortipo+'"><td>'+data[i][1]+'</td><td>'+data[i][2]+'</td></tr>');
                    
                    contarfilas = contarfilas+1;

                    }
                $('#divcontenido').append('</tbody></table></div>');

                $('#divcontenido').append('<div><h4 class="text-success">Bebidas: <b>'+contarfilasbebi+'</b></h4><h4 class="text-warning">Comidas: <b>'+contarfilascomi+'</b></h4><h4 class="text-info">Promociones: <b>'+contarfilaspromo+'</b></h4><h4>Total: <b>'+contarfilas+'</b></h4></div>');
             
             $('body').removeClass('loading'); //Removemos la clase loading

               },             
              error: function( data ) {
                console.log('se ejecuto mal la peticion');
                console.log(data);

               $('body').removeClass('loading'); //Removemos la clase loading

              }, 
            })
        });



    </script>
<script>loadingOFF();</script>
<div class="modalloading"></div>
</body>

</html>

