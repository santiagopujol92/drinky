-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 08-08-2018 a las 03:59:51
-- Versión del servidor: 10.2.16-MariaDB
-- Versión de PHP: 7.1.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `u584867474_drink`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `albumes`
--

CREATE TABLE `albumes` (
  `id` int(250) NOT NULL,
  `fotoprin` varchar(250) NOT NULL,
  `fotosec1` varchar(250) NOT NULL,
  `fotosec2` varchar(250) NOT NULL,
  `fotosec3` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `albumes`
--

INSERT INTO `albumes` (`id`, `fotoprin`, `fotosec1`, `fotosec2`, `fotosec3`) VALUES
(1, 'principal_1493225577.jpeg', '1_1507669749.jpeg', '2_1488320233.jpg', '3_1488320233.jpg'),
(2, 'principal.jpg', '1.jpg', '2.jpg', '3.jpg'),
(3, 'principal.jpg', '1.jpg', '2.jpg', '3.jpg'),
(4, 'principal_1489795923.jpeg', '1_1489794926.jpeg', '2_1489443944.jpeg', '3.jpg'),
(5, 'principal_.jpg', '1_1489532263.jpeg', '2_1488368155.jpeg', '3_1488368155.jpeg'),
(6, 'principal.jpg', '1.jpg', '2.jpg', '3.jpg'),
(7, 'principal.jpg', '1_1491225586.jpeg', '2.jpg', '3.jpg'),
(8, 'principal.jpg', '1.jpg', '2.jpg', '3.jpg'),
(10, 'principal.jpg', '1.jpg', '2.jpg', '3.jpg'),
(11, 'principal.jpg', '1.jpg', '2.jpg', '3.jpg'),
(12, 'principal.jpg', '1.jpg', '2.jpg', '3.jpg'),
(13, 'principal.jpg', '1.jpg', '2.jpg', '3.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bares`
--

CREATE TABLE `bares` (
  `idbar` int(11) NOT NULL,
  `nombre_bar` varchar(100) DEFAULT NULL,
  `telefono` varchar(100) DEFAULT NULL,
  `email_usu` varchar(100) DEFAULT NULL,
  `direccion` varchar(200) DEFAULT NULL,
  `idbarrio` int(11) NOT NULL,
  `idlocalidad` int(150) NOT NULL,
  `album` int(250) DEFAULT NULL,
  `tipolocal` varchar(30) NOT NULL,
  `activo` char(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `bares`
--

INSERT INTO `bares` (`idbar`, `nombre_bar`, `telefono`, `email_usu`, `direccion`, `idbarrio`, `idlocalidad`, `album`, `tipolocal`, `activo`) VALUES
(1, 'Bonanza &&', '152552252', 'santiagopujol92@gmail.com', 'rondeau 150', 3, 1, 1, 'Bar', 'S'),
(2, 'Wachitas &&', '03513961444', 'santiagopujol92@gmail.com', 'Ituzaingo 150', 3, 1, 2, 'Bar', 'S'),
(3, 'Punto Alvear', '03541426404', 'santiagopujol92@gmail.com', 'Alvear Esquina Alberdi 515', 8, 3, 3, 'Bar', 'S'),
(4, 'Antares', '03514245785', 'santi04_cat@hotmail.com', 'San Lorenzo 79', 3, 1, 4, 'Bar', 'S'),
(5, 'Cafe Imperial', '08107776', 'santi04_cat@hotmail.com', 'Av Libertador 200', 2, 2, 5, 'Bar', 'S'),
(6, 'Unplugged', '34234234', 'santiagopujol92@gmail.com', 'Av. Rafael Nuñez 4812', 7, 1, 6, 'Boliche', 'S'),
(7, 'La Barra Boliche', '34123123', 'santiagopujol92@gmail.com', 'Lima 150', 3, 1, 7, 'Boliche', 'S'),
(8, 'Studio Theather', '3453234', 'antonellarodriiguez@hotmail.com', 'Rosario de Santa Fe 272', 3, 1, 8, 'Boliche', 'S'),
(10, 'Pepito', '2444', 'floru_08@hotmail.es', 'Jdjfjf', 7, 1, 10, 'Bar', 'S'),
(11, 'Palma de Mallorca ', '33333333', 'floru_08@hotmail.es', 'Calle principal 333', 3, 1, 11, 'Boliche', 'S'),
(12, 'Kito', '3445', 'agus95_talleres@hotmail.com', 'Chascomus 1878', 3, 1, 12, 'Boliche', 'S'),
(13, 'Barto', '2147483647', 'camigonzalez27@outlook.com', 'La Cañada 343', 9, 1, 13, 'Bar', 'S'),
(17, '', '', '', '', 0, 0, NULL, '', ''),
(16, '', '', '', '', 0, 0, NULL, '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `barrios`
--

CREATE TABLE `barrios` (
  `idbarrio` int(11) NOT NULL,
  `barrio` varchar(100) NOT NULL,
  `idlocalidad` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `barrios`
--

INSERT INTO `barrios` (`idbarrio`, `barrio`, `idlocalidad`) VALUES
(1, 'Jardín', 1),
(2, 'Centro', 2),
(3, 'Centro', 1),
(4, 'El Alto', 2),
(5, 'Talleres', 1),
(6, 'Tajamar', 2),
(7, 'Cerro', 1),
(8, 'Centro', 3),
(9, 'Guemes', 1),
(10, 'Chateau', 1),
(11, 'Nueva Córdoba', 1),
(12, 'Lago', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cartas`
--

CREATE TABLE `cartas` (
  `idbar` int(11) NOT NULL,
  `idproducto` int(11) NOT NULL,
  `precio` double NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cartas`
--

INSERT INTO `cartas` (`idbar`, `idproducto`, `precio`) VALUES
(2, 1, 67),
(3, 9, 123),
(2, 3, 34234),
(3, 6, 123),
(2, 5, 456456),
(2, 7, 456456),
(2, 8, 456456),
(2, 10, 34234),
(2, 24, 23),
(3, 1, 33.56),
(7, 7, 4564),
(5, 10, 11),
(5, 1, 876876),
(3, 7, 12),
(7, 27, 32234),
(5, 2, 11),
(3, 1213, 123),
(3, 5, 123),
(7, 10, 1231),
(3, 10, 123),
(4, 2, 50),
(10, 9, 110),
(4, 6, 47),
(4, 7, 765),
(4, 8, 75.85),
(10, 4, 90),
(4, 9, 133),
(5, 5, 12),
(6, 1, 100),
(6, 6, 100),
(1, 10, 123),
(8, 5, 23),
(12, 4, 100),
(5, 6, 11),
(5, 3, 11),
(7, 5, 456456),
(5, 7, 11),
(5, 4, 11),
(5, 9, 11),
(5, 43, 111),
(5, 8, 11),
(7, 4, 8384);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `localidades`
--

CREATE TABLE `localidades` (
  `idlocalidad` int(11) NOT NULL,
  `localidad` varchar(150) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `localidades`
--

INSERT INTO `localidades` (`idlocalidad`, `localidad`) VALUES
(1, 'Córdoba'),
(2, 'Alta Gracia'),
(3, 'Carlos Paz');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mensajes`
--

CREATE TABLE `mensajes` (
  `idmensaje` int(11) NOT NULL,
  `tipomsg` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `telefono` int(30) NOT NULL,
  `mensaje` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `mensajes`
--

INSERT INTO `mensajes` (`idmensaje`, `tipomsg`, `email`, `telefono`, `mensaje`) VALUES
(1, 'Consulta', 'santi04_cat@hotmail.com', 4643997, 'No se como cambiar las fotos'),
(2, 'Consulta', 'santiagopujol92@gmail.com', 456345, 'NMIOIIII'),
(3, 'Otro', 'santiagopujol92@gmail.com', 5634534, 'ssdf'),
(4, 'Consulta', 'santiagopujol92@gmail.com', 8776, 'Yygh'),
(5, 'Consulta', 'santiagopujol92@gmail.com', 83838, 'Nffn'),
(6, 'Consulta', 'santiagopujol92@gmail.com', 345345, 'dsfsdf'),
(7, 'Consulta', 'santiagopujol92@gmail.com', 35154321, '????');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `idproducto` int(11) NOT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  `idtipo_producto` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`idproducto`, `descripcion`, `idtipo_producto`) VALUES
(1, 'cerveza quilmes', 1),
(2, 'pizza', 2),
(3, 'gancia', 1),
(4, 'fernet branca', 1),
(5, 'pamcho', 2),
(6, 'vodka', 1),
(7, 'hamburgues', 2),
(8, 'papitas', 2),
(9, 'Vodka', 1),
(10, 'Dr Lemon', 1),
(11, ' 2 x 1 birras', 3),
(12, ' piza mas birra', 3),
(13, 'vodka + 2 speed', 3),
(14, 'fernet y una piza', 3),
(15, 'LOMO + 2 CERVEZAS', 3),
(16, 'CERVEZA Y MANI', 3),
(17, 'descuento en fernet', 3),
(18, '2x1 jugo de naranja', 3),
(19, 'vodka con limon', 3),
(20, '2x1 m cdonal', 3),
(21, '2x1 burger', 3),
(22, '2x1 helado', 3),
(25, '1213', 3),
(24, 'juanca', 3),
(26, 'ghjhgj', 3),
(27, 'SEEE', 3),
(28, 'SAEEEEEEE', 3),
(29, 'fsdfsdf', 3),
(30, 'fsdfsdf', 3),
(31, 'fsdfsdf', 3),
(32, 'AAAAAAAAAA', 3),
(33, 'UYJKUJU', 3),
(34, 'UYJKUJU', 3),
(35, 'dfgdfg', 3),
(36, 'dfgdfg', 3),
(37, 'dfgdfg', 3),
(38, 'dfgdfg', 3),
(39, 'dfgdfg', 3),
(40, 'dfgdfg', 3),
(41, 'dgdfgdf', 3),
(42, 'dgdfgdf', 3),
(43, '2x1 cafe', 3),
(44, '', NULL),
(45, '', NULL),
(46, '', NULL),
(47, '', NULL),
(48, '', NULL),
(49, '', NULL),
(50, '', NULL),
(51, '', NULL),
(52, '', NULL),
(53, '', NULL),
(54, '', NULL),
(55, '', NULL),
(56, '', NULL),
(57, '', NULL),
(58, 'Mega promoción foeh.n', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reservas`
--

CREATE TABLE `reservas` (
  `email_usu` varchar(100) NOT NULL,
  `idbar` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `cantidad_pers` int(11) DEFAULT NULL,
  `telefono_usu` int(30) DEFAULT NULL,
  `estado` varchar(50) NOT NULL,
  `eliminada` char(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `reservas`
--

INSERT INTO `reservas` (`email_usu`, `idbar`, `fecha`, `cantidad_pers`, `telefono_usu`, `estado`, `eliminada`) VALUES
('braiangabriellonni@gmail.com', 2, '2016-12-30 23:10:00', 6, 929393, 'Rechazado', 'N'),
('santi04_cat@hotmail.com', 2, '2016-10-05 23:30:00', 123, 234, 'Confirmado', 'S'),
('santi04_cat@hotmail.com', 3, '2016-10-04 05:25:00', 123, 324234, 'Rechazado', 'S'),
('santi04_cat@hotmail.com', 3, '2016-10-06 06:30:00', 10, 12323556, 'Confirmado', 'S'),
('santi04_cat@hotmail.com', 3, '2016-10-12 10:50:00', 123123, 123123, 'Confirmado', 'S'),
('santi04_cat@hotmail.com', 4, '2016-10-05 10:30:00', 123, 12312, 'Rechazado', 'S'),
('santi04_cat@hotmail.com', 4, '2016-10-07 11:55:00', 123, 123, 'Confirmado', 'S'),
('santiagopujol92@gmail.com', 2, '2016-11-01 05:05:00', 123, 123, 'Rechazado', 'S'),
('santiagopujol92@gmail.com', 2, '2016-11-02 06:30:00', 123, 123, 'Confirmado', 'S'),
('santiagopujol92@gmail.com', 2, '2016-11-12 01:35:00', 11, 234, 'Confirmado', 'S'),
('santiagopujol92@gmail.com', 4, '2016-11-12 11:35:00', 12312, 123, 'Confirmado', 'S'),
('santiagopujol92@gmail.com', 10, '2016-12-06 06:30:00', 11, 34, 'Pendiente', 'N'),
('braiangabriellonni@gmail.com', 4, '2016-12-15 23:00:00', 6, 2147483647, 'Confirmado', 'S'),
('santi04_cat@hotmail.com', 4, '2017-02-10 03:15:00', 34, 34, 'Confirmado', 'N'),
('santiagopujol92@gmail.com', 5, '2017-04-14 15:35:00', 23, 83, 'Pendiente', 'N'),
('santi04_cat@hotmail.com', 4, '2017-02-16 11:35:00', 999999999, 99999999, 'Rechazado', 'S'),
('santi04_cat@hotmail.com', 4, '2017-02-10 07:35:00', 456, 456, 'Confirmado', 'S'),
('santi04_cat@hotmail.com', 4, '2017-02-16 10:50:00', 345, 345, 'Confirmado', 'N'),
('santi04_cat@hotmail.com', 4, '2017-02-09 10:50:00', 345345, 567, 'Rechazado', 'S'),
('santi04_cat@hotmail.com', 4, '2017-02-23 14:50:00', 234, 234, 'Rechazado', 'N'),
('santi04_cat@hotmail.com', 4, '2017-02-03 11:35:00', 345, 345, 'Rechazado', 'S'),
('santi04_cat@hotmail.com', 1, '2017-02-23 14:50:00', 88888888, 88888888, 'Rechazado', 'S'),
('santi04_cat@hotmail.com', 1, '2017-02-22 14:45:00', 88888888, 88888888, 'Rechazado', 'N'),
('santi04_cat@hotmail.com', 7, '2017-02-22 10:50:00', 2147483647, 2147483647, 'Confirmado', 'S'),
('santi04_cat@hotmail.com', 5, '2017-02-27 18:55:00', 123456879, 123546879, 'Rechazado', 'S'),
('santi04_cat@hotmail.com', 10, '2017-02-25 15:35:00', 123546879, 123546879, 'Pendiente', 'N'),
('santi04_cat@hotmail.com', 8, '2017-02-10 07:35:00', 435, 345, 'Pendiente', 'N'),
('santi04_cat@hotmail.com', 4, '2017-02-08 06:30:00', 456456, 456456, 'Confirmado', 'N'),
('santi04_cat@hotmail.com', 4, '2017-02-16 11:55:00', 234, 234, 'Rechazado', 'S'),
('santi04_cat@hotmail.com', 4, '2017-02-14 09:30:00', 567567, 567567, 'Confirmado', 'N'),
('santi04_cat@hotmail.com', 3, '2017-02-18 09:45:00', 838, 737, 'Pendiente', 'N'),
('santi04_cat@hotmail.com', 3, '2017-02-14 10:50:00', 838, 737, 'Confirmado', 'S'),
('santi04_cat@hotmail.com', 3, '2017-04-14 10:35:00', 6, 755, 'Pendiente', 'N');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `solicitudes_bajas`
--

CREATE TABLE `solicitudes_bajas` (
  `idsolicitud` int(11) NOT NULL,
  `idbar` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `motivo` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `solicitudes_bajas`
--

INSERT INTO `solicitudes_bajas` (`idsolicitud`, `idbar`, `email`, `motivo`) VALUES
(7, 2, 'santiagopujol92@gmail.com', 'asdasdas'),
(8, 2, 'santiagopujol92@gmail.com', '4124234'),
(9, 2, 'santiagopujol92@gmail.com', 'asdasd'),
(10, 7, 'santiagopujol92@gmail.com', 'jeje'),
(11, 3, 'santiagopujol92@gmail.com', 'chauu'),
(12, 3, 'santiagopujol92@gmail.com', 'holaaaaaaaaaa'),
(13, 7, 'santiagopujol92@gmail.com', 'jahjaj'),
(14, 4, 'santi04_cat@hotmail.com', 'Nos vemos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `solicitudes_cambiodire`
--

CREATE TABLE `solicitudes_cambiodire` (
  `idsolicitud` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `barid` int(11) NOT NULL,
  `telefono` int(30) NOT NULL,
  `mensaje` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `solicitudes_cambiodire`
--

INSERT INTO `solicitudes_cambiodire` (`idsolicitud`, `email`, `barid`, `telefono`, `mensaje`) VALUES
(1, 'santiagopujol92@gmail.com', 1, 345345, 'sdfsdf'),
(2, 'santiagopujol92@gmail.com', 1, 456456, 'dfgdfg'),
(3, 'santiagopujol92@gmail.com', 1, 3453453, '345345435'),
(4, 'santiagopujol92@gmail.com', 1, 345345, 'fdgdfg'),
(5, 'santiagopujol92@gmail.com', 1, 435345, 'sdfsdf'),
(6, 'santiagopujol92@gmail.com', 1, 345345, 'sdfdsf'),
(7, 'santiagopujol92@gmail.com', 1, 45645, '6dfgdfg'),
(8, 'santiagopujol92@gmail.com', 2, 34534, '5sdfsdf'),
(9, 'santiagopujol92@gmail.com', 1, 345345, 'sdfsdfsdf'),
(10, 'santiagopujol92@gmail.com', 2, 3531, 'hola'),
(11, 'santiagopujol92@gmail.com', 3, 3535, 'klk'),
(12, 'santiagopujol92@gmail.com', 2, 34534, 'sdfdsf'),
(13, 'santiagopujol92@gmail.com', 6, 45, 'Jfjfj'),
(14, 'santiagopujol92@gmail.com', 3, 345345, 'sdfsdf');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `solicitudes_pendientes`
--

CREATE TABLE `solicitudes_pendientes` (
  `idsolicitud` int(11) NOT NULL,
  `email_usu` varchar(100) NOT NULL,
  `nombre_usu` varchar(100) DEFAULT NULL,
  `dni` int(15) NOT NULL,
  `nombre_bar` varchar(100) DEFAULT NULL,
  `domicilio_bar` varchar(150) DEFAULT NULL,
  `localidad` varchar(200) NOT NULL,
  `barrio` varchar(200) NOT NULL,
  `telefono` int(11) DEFAULT NULL,
  `tipo_local` varchar(50) DEFAULT NULL,
  `fecha` date NOT NULL,
  `tipo_solicitud` varchar(10) NOT NULL,
  `motivo` varchar(250) NOT NULL,
  `estado` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `solicitudes_pendientes`
--

INSERT INTO `solicitudes_pendientes` (`idsolicitud`, `email_usu`, `nombre_usu`, `dni`, `nombre_bar`, `domicilio_bar`, `localidad`, `barrio`, `telefono`, `tipo_local`, `fecha`, `tipo_solicitud`, `motivo`, `estado`) VALUES
(11, 'santiagopujol92@gmail.com', 'Santiago Pujol', 34543, 'sdfsd', 'adas', 'asdas', 'adasd', 234234, 'Bar', '2016-10-24', 'Alta', '', 'Pendiente'),
(12, 'camigonzalez27@outlook.com', 'Cami Gonzalez', 39018392, 'Barto', 'Cordoba', 'Guemes', 'La Cañada', 2147483647, 'Bar', '2016-12-09', 'Alta', '', 'Pendiente'),
(14, 'santi04_cat@hotmail.com', 'Santi Pujol', 234, 'asd', 'asd', 'asd', 'asd', 324, 'Bar', '2017-02-16', 'Alta', '', 'Pendiente'),
(15, 'santi04_cat@hotmail.com', 'Santi Pujol', 345345, 'asdfasd', 'adfads', 'asdasd', 'asddasd', 234234, 'Bar', '2017-02-16', 'Alta', '', 'Pendiente'),
(16, 'santi04_cat@hotmail.com', 'Santi Pujol', 34234, 'asdas', 'asdas', 'adasd', 'adasd', 234234, 'Bar', '2017-02-18', 'Alta', '', 'Pendiente'),
(17, 'santi04_cat@hotmail.com', 'Santi Pujol', 34534, 'sdfsd', 'sdfsdf', 'dfgdfg', 'sdfsdf', 345345, 'Boliche', '2017-02-18', 'Alta', '', 'Pendiente'),
(18, 'santi04_cat@hotmail.com', 'Santi Pujol', 234, 'asdas', 'dsfdsf', 'sdfsdf', 'sdfdsf', 234324, 'Bar', '2017-02-19', 'Alta', '', 'Pendiente'),
(19, 'santi04_cat@hotmail.com', 'Santi Pujol', 345345, 'asdfdsf', 'sdfsdf', 'wdfds', 'dfsdf', 435345, 'Bar', '2017-02-19', 'Alta', '', 'Pendiente'),
(20, 'santiagopujol92@gmail.com', 'Santiago Pujol', 23423, 'asdfsdf', 'sdfsdf', 'sdfsdf', '34sdfsd', 345345, 'Bar', '2017-02-19', 'Alta', '', 'Pendiente'),
(21, 'santiagopujol92@gmail.com', 'Santiago Pujol', 334, 'Nfkkf', 'Mdnd', 'Nfkf', 'Kdkd', 8383, 'Boliche', '2017-02-19', 'Alta', '', 'Pendiente'),
(22, 'santiagopujol92@gmail.com', 'Santiago Pujol', 45634, 'sdf', 'asdf', 'sdf', 'sdfs', 3345, 'Boliche', '2017-02-19', 'Alta', '', 'Pendiente'),
(23, 'santi04_cat@hotmail.com', 'Santi Pujol', 838, 'Jdud', 'Uj', 'Jfjf', 'Jdjd', 7373, 'Bar', '2017-02-19', 'Alta', '', 'Pendiente'),
(24, 'santiagopujol92@gmail.com', 'Santiago Pujol', 2234, 'asd', 'adfa', 'asdfasd', 'asdfas', 234234, 'Bar', '2017-02-21', 'Alta', '', 'Pendiente');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_productos`
--

CREATE TABLE `tipo_productos` (
  `idtipo_producto` int(11) NOT NULL,
  `tipoprod` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_productos`
--

INSERT INTO `tipo_productos` (`idtipo_producto`, `tipoprod`) VALUES
(1, 'Bebidas'),
(2, 'Comidas'),
(3, 'Promociones');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ubicaciones`
--

CREATE TABLE `ubicaciones` (
  `idbar` int(11) NOT NULL,
  `latitud` varchar(50) DEFAULT NULL,
  `longitud` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ubicaciones`
--

INSERT INTO `ubicaciones` (`idbar`, `latitud`, `longitud`) VALUES
(1, '-31.4226852', '-64.18459059999998'),
(2, '-31.423031', ' -64.183908'),
(3, '-31.4207578', '-64.49549990000003'),
(4, '-31.4234286', '-64.18606649999998'),
(5, '-31.658392', '-64.425508'),
(7, '-31.4132046', '-64.18062909999998'),
(8, '-31.4171116', '-64.18047530000001'),
(9, '-31.3602026', '-64.23849380000001'),
(10, '-31.4171116', '-64.18047530000001'),
(11, '-4344344', '-43434344'),
(12, '-31.3602091', '-64.23851100000002'),
(13, '-31.4191075', '-64.18981589999999'),
(6, '-31.3602091', '-64.18047530000001');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `email` varchar(100) NOT NULL,
  `idgoogle` varchar(150) DEFAULT NULL,
  `nomgoogle` varchar(150) DEFAULT NULL,
  `idface` varchar(150) DEFAULT NULL,
  `nomface` varchar(150) DEFAULT NULL,
  `tipo` char(1) NOT NULL,
  `activo` char(1) NOT NULL,
  `ultimo_ingreso` datetime NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`email`, `idgoogle`, `nomgoogle`, `idface`, `nomface`, `tipo`, `activo`, `ultimo_ingreso`, `password`) VALUES
('adriana2016rubio@gmail.com', '117412166481404144246', 'Adriana Rubio', NULL, NULL, 'A', 'S', '2016-12-05 18:00:00', '123'),
('bochaacba24@gmail.com', '116791137319137524848', 'Bocha Rodriguez', NULL, NULL, 'U', 'S', '2016-12-05 18:00:00', '123'),
('braiangabriellonni@gmail.com', '107815443078652650440', 'Braian Gabrielloni', NULL, NULL, 'U', 'S', '2017-04-06 17:26:55', '123'),
('fabiandelospiratas@gmail.com', '100166703804694563336', 'Fabian Guzman', NULL, NULL, 'U', 'S', '2016-12-08 18:00:00', '123'),
('juan41355@gmail.com', '101915945861577687388', 'Juan Jorge', '10207719446253434', 'Jorge Pujol', 'U', 'S', '2017-11-15 16:04:33', '123'),
('ma.alear@gmail.com', NULL, NULL, '407814206275581', 'María Alejandra Arroyo', 'U', 'S', '2016-12-03 18:00:00', '123'),
('nahuelnucera990@gmail.com', NULL, NULL, '10207610354198637', 'Nahuel Alejandro Nucera', 'A', 'S', '2016-12-18 16:46:06', '123'),
('robleelian@gmail.com', '109504985011004565714', 'Elian Roble', NULL, NULL, 'U', 'S', '2016-12-05 18:00:00', '123'),
('santi04_cat@hotmail.com', NULL, NULL, '10210404282080308', 'Santi Pujol', 'U', 'S', '2017-12-19 18:12:51', '123'),
('santiagopujol92@gmail.com', '114843399932613042551', 'Santiago Pujol', NULL, NULL, 'A', 'S', '2018-08-02 15:51:05', '123'),
('vanicba@gmail.com', '112574639915299236921', 'Vani Pujol', NULL, NULL, 'U', 'S', '2016-12-02 18:00:00', '123'),
('juanm_rg@hotmail.com', NULL, NULL, '10154895885019248', 'Juan Manuel Rodriguez Gavier', 'U', 'S', '2016-12-19 10:16:21', '123'),
('micaela_ocampo@outlook.es', NULL, NULL, '1286591834730980', 'Micaela Ocampo', 'U', 'S', '2016-12-18 16:53:15', '123'),
('camigonzalez27@outlook.com', NULL, NULL, '10209703865686856', 'Cami Gonzalez', 'U', 'S', '2016-12-08 22:48:45', '123'),
('floru_08@hotmail.es', NULL, NULL, '1331615460214218', 'Floor Cufre', 'U', 'S', '2016-12-09 13:37:19', '123'),
('agus95_talleres@hotmail.com', NULL, NULL, '10211649873268623', 'Agustin Pujol Talleres', 'U', 'S', '2016-12-08 18:00:00', '123'),
('agus95talleres@gmail.com', '104283330280517873482', 'Agustin Pujol', NULL, NULL, 'U', 'S', '2016-12-08 18:00:00', '123'),
('florenciamagalicufre@gmail.com', '113906978285039750647', 'Florencia Cufre', NULL, NULL, 'U', 'S', '2016-12-08 18:00:00', '123'),
('chosimba.73@gmail.com', '112480792628406387618', 'Juan Manuel Rodriguez Gavier', NULL, NULL, 'U', 'S', '2016-12-13 14:18:27', '123'),
('antonellarodriiguez@hotmail.com', NULL, NULL, '1657841640896488', 'Anto Rodriguez', 'U', 'S', '2017-01-11 14:21:10', '123'),
('enri079@hotmail.com', NULL, NULL, '10212059307067713', 'Enri Fo', 'U', 'S', '2017-01-28 19:59:05', '123'),
('caro_ortiz_lina@hotmail.com', NULL, NULL, '10211985449895368', 'Carolina Ortiz', 'U', 'S', '2017-02-05 13:31:46', '123'),
('gustavomenacba@gmail.com', NULL, 'Gustavo Mena', NULL, NULL, 'U', 'S', '0000-00-00 00:00:00', 'tocamela123'),
('gaby', NULL, 'Gaby', NULL, NULL, 'U', 'S', '0000-00-00 00:00:00', '123'),
('oriana', NULL, NULL, NULL, NULL, 'U', 'S', '0000-00-00 00:00:00', '123'),
('anto_1192@hotmail.com', NULL, NULL, '10155486160240639', 'Antonella Rosmari', 'U', 'S', '2017-02-22 20:52:00', ''),
('pedroc777@gmail.com', '108117569842355108484', 'P CP', NULL, NULL, 'U', 'S', '2017-03-12 01:51:30', ''),
('fedeemiguez@gmail.com', '114661541912794738390', 'Fede Miguez', NULL, NULL, 'U', 'S', '2017-06-21 14:31:42', ''),
('daniela.trejo@hotmail.es', NULL, NULL, '10211362744602652', 'Daniela Trejo', 'U', 'S', '2017-09-06 21:59:56', '');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `albumes`
--
ALTER TABLE `albumes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `bares`
--
ALTER TABLE `bares`
  ADD PRIMARY KEY (`idbar`);

--
-- Indices de la tabla `barrios`
--
ALTER TABLE `barrios`
  ADD PRIMARY KEY (`idbarrio`);

--
-- Indices de la tabla `cartas`
--
ALTER TABLE `cartas`
  ADD PRIMARY KEY (`idbar`,`idproducto`);

--
-- Indices de la tabla `localidades`
--
ALTER TABLE `localidades`
  ADD PRIMARY KEY (`idlocalidad`);

--
-- Indices de la tabla `mensajes`
--
ALTER TABLE `mensajes`
  ADD PRIMARY KEY (`idmensaje`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`idproducto`);

--
-- Indices de la tabla `reservas`
--
ALTER TABLE `reservas`
  ADD PRIMARY KEY (`email_usu`,`idbar`,`fecha`);

--
-- Indices de la tabla `solicitudes_bajas`
--
ALTER TABLE `solicitudes_bajas`
  ADD PRIMARY KEY (`idsolicitud`);

--
-- Indices de la tabla `solicitudes_cambiodire`
--
ALTER TABLE `solicitudes_cambiodire`
  ADD PRIMARY KEY (`idsolicitud`);

--
-- Indices de la tabla `solicitudes_pendientes`
--
ALTER TABLE `solicitudes_pendientes`
  ADD PRIMARY KEY (`idsolicitud`);

--
-- Indices de la tabla `tipo_productos`
--
ALTER TABLE `tipo_productos`
  ADD PRIMARY KEY (`idtipo_producto`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `albumes`
--
ALTER TABLE `albumes`
  MODIFY `id` int(250) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `bares`
--
ALTER TABLE `bares`
  MODIFY `idbar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `barrios`
--
ALTER TABLE `barrios`
  MODIFY `idbarrio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `localidades`
--
ALTER TABLE `localidades`
  MODIFY `idlocalidad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `mensajes`
--
ALTER TABLE `mensajes`
  MODIFY `idmensaje` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `idproducto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT de la tabla `solicitudes_bajas`
--
ALTER TABLE `solicitudes_bajas`
  MODIFY `idsolicitud` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `solicitudes_cambiodire`
--
ALTER TABLE `solicitudes_cambiodire`
  MODIFY `idsolicitud` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `solicitudes_pendientes`
--
ALTER TABLE `solicitudes_pendientes`
  MODIFY `idsolicitud` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT de la tabla `tipo_productos`
--
ALTER TABLE `tipo_productos`
  MODIFY `idtipo_producto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
