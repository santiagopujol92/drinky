
<?php

if(session_id() == '' || !isset($_SESSION)) {
  session_start();
}

if ($_SESSION['estado'] != 'S'){
  header ("Location: logeo.php");
  exit();
}

if (!(isset($_SESSION['iniciado']))) {
  header ("Location: logeo.php");
  exit();
}

if ($_SESSION['iniciado'] != '5dbc98dcc983a70728bd082d1a47546e'){
  header ("Location: logeo.php");
  exit();

}

?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Drinky || Mis Locales</title>
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"> 
    <!--LOADING -->
    <?php include("loading.php"); ?>
    <!--LOADING -->

  <link rel="stylesheet" href="css/bootstrap.min.css"/> 
  
  <script  src="js/jquery-3.1.0.min.js"></script>
  <script  src="js/bootstrap.min.js"></script>

  <link rel="stylesheet" href="css/bootstrap-social.css"/>
  <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css ">
  <link rel="icon" type="image/png" href="images/drinky-logo.png" />
  <link rel="stylesheet" href="css/headeryfooter.css"/>
  <link rel="stylesheet" href="css/misbares.css"/>

  <link type="text/css" rel="stylesheet" href="css/chosen.min.css">

<link href="css/fileinput.min.css" media="all" rel="stylesheet" type="text/css"/>
<script src="js/fileinput.min.js" type="text/javascript"></script>
<script src="js/chosen.jquery.min.js"></script>

   <script>
    function loadingON(){
    $('#loadingDiv').removeClass('hidden');
   }
   function loadingOFF(){
    $('#loadingDiv').addClass('hidden');
    }
   </script>

  <style>


.dialogogrande {
  height: 80% ;
  width:80%;
  margin-left:10%;
  margin-right:10%;
}

.modal-content {
  overflow:visible;
}

</style>

</head>
<body>

	<?php include_once("header.php"); ?>
  <br>
  <br>
  <br>

 <div class="carousel slide" data-ride="carousel" id="carousel-1"> 

    <!-- INDICADORES -->
    <ol class="carousel-indicators">
      <li data-target="#carousel-1" data-slide-to="0" style="background:#6CDAF8;border:2px solid #6CDAF8" class="active"></li>
      <li data-target="#carousel-1" data-slide-to="1" style="background:#6CDAF8;border:2px solid #6CDAF8"></li> 
      <li data-target="#carousel-1" data-slide-to="2" style="background:#6CDAF8;border:2px solid #6CDAF8"></li> 
    </ol>

    <!-- CONTENEDOR DE LOS SLIDE, SON ITEMS -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="images/tragos.jpg"   class="img-responsive" alt=""><!--IMAGENES GRANDES OPTIMIZADAS NO PESADAS -->
        <div class="carousel-caption hidden-xs hidden-sm"><!-- CON HIDDEN OCULTAMOS EL TEXTOPARA CIERTO TAMAÑO-->
         <div class="centrar"><h1><b>Consulta toda la info de Bares y Boliches en Córdoba</b></h1></div>
       </div>
     </div>

     <div class="item">
      <img src="images/celu.jpg"   class="img-responsive" alt=""><!--IMAGENES GRANDES OPTIMIZADAS NO PESADAS -->
      <div class="carousel-caption hidden-xs hidden-sm"><!-- CON HIDDEN OCULTAMOS EL TEXTOPARA CIERTO TAMAÑO-->
       <div class="centrar"><h1><b>Consulta toda la info de Bares y Boliches en Córdoba</b></h1></div>

     </div>
   </div>

   <div class="item">
    <img src="images/vinos.jpg"   class="img-responsive" alt=""><!--IMAGENES GRANDES OPTIMIZADAS NO PESADAS -->
    <div class="carousel-caption hidden-xs hidden-sm"> <!-- CON HIDDEN OCULTAMOS EL TEXTO PARA CIERTO TAMAÑO-->
     <div class="centrar"><h1><b>Consulta toda la info de Bares y Boliches en Córdoba</b></h1></div>
   </div>
 </div>

 <a href="#carousel-1" style="color:#6CDAF8;" class="left carousel-control" role="button" data-slide="prev">
  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
  <span class="sr-only"></span> <!-- PARA VERLO EN DISPOSITIVOS DE LECTURA -->
</a>

<a href="#carousel-1" style="color:#6CDAF8;" class="right carousel-control" role="button" data-slide="next">
  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
  <span class="sr-only"></span> <!-- PARA VERLO EN DISPOSITIVOS DE LECTURA -->
</a>
</div>
</div>

   <div  id="header" class="withPhoto withApps normal withOptional" style="background-image: url(images/nueva.jpg);background-size:cover">

    <div class="headerOverlay showBackground">
      <br>
      <div class="container" >
        <div class="row" style="min-height:600px;">

          <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
            <h2><b>Listado de Mis Locales:</b></h2><br>
            <div class="panel panel-primary">

              <div class="panel-heading"><b>Administrar:</b></div>

              <div class="table-responsive table-hover">  <!-- ACHICAR EL TAMAÑO COMO PARA DISP MOVILES SE SIGA VIENDO LA TABLA-->

                <table class="table table-hover">
                 <?php 

                 $miusu = $_SESSION['usuario'];

                 include('conexion/conexion.php');

                 $conection = mysqli_connect($host, $usuario, $pass,$db) or die("error de conexion a DB");

                 $select = $conection->query("SELECT b.* FROM  bares b WHERE b.email_usu = '$miusu' AND activo = 'S' ORDER BY b.nombre_bar");


                 while ($result = mysqli_fetch_assoc($select)) 
                 {

                  ?>

                  <tr  class="btn btn-default">
                    <th width="1000">
                     <h4><b class="azul"><?php echo $result['tipolocal'].' '.$result['nombre_bar'];?></b> </h4>
                   </th>

                   <th>
                    <a href="#modificarcarta" data-id="<?php echo $result['idbar'];?>" data-toggle="modal" title="Modificar Carta" type="submit" class="modal_administrar_carta btn btn-md btn-warning">Carta <span class="glyphicon glyphicon-cutlery"></span> <span class="glyphicon glyphicon-glass"></span></a>
                    <a href="#verreservas" data-id="<?php echo $result['idbar'];?>" data-toggle="modal" title="Administrar Reservas" type="submit" class="modal_adm_reservas btn btn-md btn-info">Reservas <span class="glyphicon glyphicon-list-alt"></span></a>
                    <a href="#editarbar" data-id="<?php echo $result['idbar'];?>" data-toggle="modal" title="Editar Datos Bar" type="submit" class="modal_editar_bar btn btn-md btn-primary">Editar <span class="glyphicon glyphicon-pencil"></span></a>
                    <a href="#eliminarbar" data-id="<?php echo $result['idbar'];?>" data-toggle="modal" title="Dar de Baja Bar" type="submit" class="modal_baja_bar btn btn-md btn-danger">Baja <span class="glyphicon glyphicon-off"></span></a>
                  </th>
                </tr>

                <?php
              }
              $select->close();
              $conection->next_result();

              ?>

            </table>

<!-- MODAL DE EDICION -->

            <div id="editarbar" class="modal fade" >  

              <div class="modal-dialog  ">
                <div class="modal-content">
                  <div class="modal-header">
                    <a type="button" class="close"  data-dismiss="modal" aria-hidden="true">&times;</a> 
                    <h4 class="modal-title"><b>Editar Datos del Local:</b></h4>

                  </div>
                  <div class="modal-body">

                    <form action="" enctype="multipart/form-data" method="post" name"FrmEditar" class="form-horizontal">  

                      <fieldset> 
                        <div class="form-group has-primary">
                          <label for="nombre" class="control-label col-md-2"><u>Nombre Local:</u></label>
                          <div class="col-md-10">
                            <input type="text" class="form-control" id="e_nombre_bar" name ="e_nombre_bar" placeholder="Introduce un Nombre">
                            <div id="complete_nombre_bar" class="hidden"><label><h5><b>Debe ingresar un nombre del local</b></h5></label></div> 
                          </div>  
                        </div>

                        <div class="form-group has-primary">
                          <label for="apellido" class="control-label col-md-2"><u>Teléfono:</u></label>
                          <div class="col-md-10">
                            <input type="text" class="form-control" id="e_telefono" name="e_telefono" placeholder="Escribe un Teléfono">
                            <div id="completetelefono" class="hidden"><label><h5><b>Ingrese un número telefónico no mayor a 15 digitos</b></h5></label></div> 
                          </div> 

                        </div>

                        <div class="form-group has-primary">
                          <a class="control-label col-md-2" title="Solicitar Cambio de Direccion" href="contacto.php">Dirección</a>
                          <div class="col-md-10">
                            <input type="text" class="form-control" id="e_direccion" name="e_direccion" href="contacto.php" disabled placeholder="">
                          </div>  
                        </div>

                    <!--FOTO PRINCIPAL -->

                     <div class="form-group has-primary">
                          <label class="pull-left control-label col-md-2"><u>Foto Principal:</u></label>                   
                          <div class="col-md-10">
                            <a title="Cambiar Foto Principal" id="btnfoto" class=" btn btn-warning">
                              <img id="e_foto" src="" alt="Modificar mi foto prinicpal" width="440" class="img-responsive img-rounded"></a>
                            <div>
                              <br>
                              <input class="cambiarfoto btn btn-warning" type="file" id="fotoprin" name="files[]"  accept=".jpg,.jpeg,.png"></input> 
                              <output id="list"></output>
 
                            </div>

                         </div>        
                                     
                      </div>   
                    <div class="alert alert-info hidden" id="msgfotoprin"><button class="close" data-dismiss="alert"><span>&times;</span></button>
                     <st><span class="glyphicon glyphicon-ok"></span><b>  Foto Principal Modificada, </b>Al Guardar Cambios se Cargará la nueva foto.</st>
                   </div> 

                   <!-- SUBFOTOS -->


                   <div class="form-group has-primary">
                    <label class="pull-left control-label col-md-2"><u>Album:</u></label>
                    <div class="table-responsive">
                      <table>
                        <tr>
                          <th>
                           <a title="Cambiar Foto 1" id="btnfoto1" class=" btn btn-primary">
                            <img id="e_foto1" src=""  alt="" class="img-responsive img-rounded">
                          </a>
                         </th>
                         <th>
                          <input class="btn btn-sm btn-primary" style="width:145px;" type="file" id="fotosec1" name="files[]" accept=".jpg,.jpeg,.png"></input> 
                          <output id="list1"></output>
                        </th>
                      </tr>
                      <tr>
                       <th> 
                         <a title="Cambiar Foto 2" id="btnfoto2" class=" btn btn-primary">
                          <img id="e_foto2" src="i" alt=""  class="img-responsive img-rounded">
                        </a>
                       </th>
                       <th> 
                        <input class="btn btn-sm btn-primary" style="width:145px;" type="file" id="fotosec2" name="files[]" accept=".jpg,.jpeg,.png"></input> 
                        <output id="list2"></output>
                      </th>
                    </tr>
                    <tr>
                     <th>
                      <a title="Cambiar Foto 3" id="btnfoto3" class=" btn btn-primary">
                        <img id="e_foto3" src=""  alt="" class="img-responsive img-rounded">
                      </a>
                    </th> 
                    <th>
                      <input class="btn btn-sm btn-primary" style="width:145px;"  type="file" id="fotosec3" name="files[]" accept=".jpg,.jpeg,.png"></input> 
                      <output id="list3"></output>
                    </th>
                  </tr>

                </table> 
              </div>  

              <br>


              <div class="col-md-12"> 
               <div class="alert alert-info hidden " id="msgfotosec"><button class="close" data-dismiss="alert"><span>&times;</span></button>
                 <st><span class="glyphicon glyphicon-ok"></span><b> Foto Secundaria Modificada, </b> Al Guardar Cambios se Cargará la nueva foto.</st>
               </div> 


             </div>

             <div class="col-md-12"> 
               <div class="alert alert-success hidden" id="msgguardado"><button class="close" data-dismiss="alert"><span>&times;</span></button>
                 <st><span class="glyphicon glyphicon-saved"></span><b> Estupendo! </b>Cambios Efectuados con Éxito. Si no visualiza las nuevas imagenes recarge la página con Shift + F5.</st>
               </div> 
             </div> 

      </fieldset>  

    <div class="modal-footer">
      <a class="btn btn-success" id="btnguardaredicion" onclick="uploadAjax()" title="Actualizar Todo"><b>Guadar <span class="glyphicon glyphicon-ok"></span></b>
        <a type="button" class="btn btn-danger" id="btncancelaredicion" data-dismiss="modal">Cancelar</a>
      </div>
    </form>
  </div>



</div>
</div>
</div>


<!-- MODAL DE ADMINISTRACION DE RESERVAS -->

<div class="modal fade" id="verreservas">  

  <div class="modal-dialog dialogogrande">
    <div class="modal-content">


      <div class="modal-header">
        <a type="button" class="close"  data-dismiss="modal" aria-hidden="true">&times;</a> 
        <h4 class="modal-title" id="titulomodalreservas" ><b>Administrar Reservas Existentes:</b></h4>

      </div>

      <div class="modal-body">



        <div id="" class="table-responsive"> 

          <table  id="tabladereservas" class="table table-hover table-border">

          </table>

        </div><br>  

        <div class="alert alert-warning hidden" id="msgeliminar"><button class="close" data-dismiss="alert"><span>&times;</span></button>
         <st><span class="glyphicon glyphicon-info-sign"></span><b> Atención!</b> Debe Confirmar o Rechazar la reserva antes de eliminarla.</st>
       </div>
     </div>

     <div class="modal-footer">
      <a class="btn btn-success" id="btn_actualizar" onclick="actualizarreservas()"  title="Actualizar Todo"><b>Actualizar <span class="glyphicon glyphicon-refresh"></span></b>
        <a type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</a>

      </div>

    </div>
  </div>
</div>


<!-- MODAL DE MODIFICAR LA CARTA -->

<div class="modal fade" id="modificarcarta">  

  <div class="modal-dialog dialogogrande">
    <div class="modal-content">


      <div class="modal-header">
        <a type="button" class="close" onclick="cerrarmodalcarta()" data-dismiss="modal" aria-hidden="true">&times;</a> 
       <h4 class="modal-title" id="tituloadmincarta"><b>Administrar la Carta:</b></h4>

      </div>

      <div class="modal-body">



        <form action="" enctype="multipart/form-data" method="post" name"FrmEditar" class="form-horizontal">  

          <fieldset> 
            <div class="form-group has-primary">
              <label for="tipoprodu" class="control-label col-md-2"><u>Tipo:</u></label>
              <div class="col-md-10">
                <select class="form-control" name="cmbtipoprodu" id="cmbtipoprodu" placeholder="Selecciona un Producto">
                  <option value="0">-- Selecciona un Tipo de Producto</option>
                 <option value="1">Bebidas</option>
                 <option value="2">Comidas</option>
                 <option value="3">Promociones</option>

               </select>
             </div>  
           </div>

          <div id="divpromo" class="hidden form-group has-primary">
            <label class="control-label col-md-2" id="lblpromo" title="" href=""><u>Promoción:</u></label>
            <div class="col-md-10">
              <input type="text" class="form-control" id="txtpromo" name="txtpromo" placeholder="Ingrese una nueva Promoción">
              <div id="complete_promo" class="hidden"><label><h5><b>Ingrese una Promoción (Ej: 2x1 En Cerveza) </b></h5></label></div>
            </div>  
          </div>

           <div id="divprodu" class="hidden form-group has-primary">
            <label for="prod" id="lblprodu" class=" control-label col-md-2"><u>Producto:</u></label>
            <div class="col-md-10">
              <select class="form-control chosen-select" name="cmbprodu" id="cmbprodu" placeholder="Selecciona un Producto">

              </select>
               <div id="complete_cmbprodu" class="hidden"><label><h5><b>Seleccione un Producto</b></h5></label></div>
            </div>

          </div>

          <div id="divprecio" class="hidden form-group has-primary">
            <label class=" control-label col-md-2" id="lblprecio" title="" href=""><u>Precio:</u></label>
            <div class="col-md-10">
              <input type="text" class=" form-control" id="txtprecio" name="txtprecio" placeholder="Introduce un Precio">
               <div id="complete_precio" class="hidden"><label><h5><b>Debe ingresar un monto numerico (Ej: 35.99) </b></h5></label></div>
            </div>  
          </div>

          <div class="form-group has-primary">
          <div class="col-md-12">      
          <a class="hidden pull-right btn btn-success" id="agregarprodu" onclick="agregarproducto()" title="Actualizar Todo"><b><span class="glyphicon glyphicon-plus-sign"></span> Agregar Producto</b></a>
          </div>
          </div>

        </fieldset>

        <div class="alert alert-success hidden" id="msgagregado"><button class="close" data-dismiss="alert"><span>&times;</span></button>
           <st><span class="glyphicon glyphicon-saved"> </span><b> Muy Bien! </b> Producto Agregado con Éxito</st>
         </div> 
         <div class="alert alert-warning hidden" id="msgexistente"><button class="close" data-dismiss="alert"><span>&times;</span></button>
           <st><span class="glyphicon glyphicon-info-sign"> </span><b> Atención! </b> El Producto que intenta agregar ya esta en la carta. Si desea modificarlo eliminelo y vuelvalo a agregar. Gracias</st>
         </div> 
        
        <div id="" class="table-responsive"> 
          <table id="tablaproductos" class="table table-hover table-border">
        </table>
        </div>

</form>
</div>
  <div class="modal-footer">
      <a type="button" class="btn btn-danger" onclick="cerrarmodalcarta()" data-dismiss="modal">Cerrar</a>
    </div>
  </div>

</div>
</div>
</div>

<!-- MODAL DE BAJA -->

            <div class="modal fade" id="eliminarbar">

              <div class="modal-dialog ">
                <div class="modal-content">

                  <div class="modal-header">
                    <a type="button" class="close"  data-dismiss="modal" aria-hidden="true">&times;</a> 
                     <h4 class="modal-title" id="titulobajabar"><b>Dar de Baja:</b></h4> 
                  </div>

                  <div class="modal-body">

                 <form action="" method="post" name"FrmBajaBar" class="form-horizontal">  <!-- formulario en linea -->

                    <fieldset> 

                      <div class="form-group has-primary">
                        <label for="motivo" class="control-label col-md-2">Motivo:</label>
                        <div class="col-md-10">
                          <textarea height="300" class=" form-control" rows="3" id="txt_b_motivo" placeholder="Introduce Motivo de la Baja de su Local" required></textarea>
                           <div id="complete_motivo" class="validar hidden"><label><h5>Debe ingresar un mensaje. No debe tener mas de 250 caracteres</h5></label></div>
                        </div>  
                      </div>

                    <div class="alert alert-success hidden" id="msg_solibaja_enviada"><button class="close" data-dismiss="alert"><span>&times;</span></button>
                     <st><span class="glyphicon glyphicon-saved"></span></st> Solicitud de Baja enviada con éxito. En menos de 24 hs sera confirmado y eliminado.
                   </div> 
                   <div class="alert alert-danger hidden" id="msg_solibaja_error"><button class="close" data-dismiss="alert"><span>&times;</span></button>
                     <st><span class="glyphicon glyphicon-remove"></span></st> Error al enviar la solicitud. Contactar Administradores.
                   </div> 

         </fieldset> 
          </form>
          </div>
                    <div class="modal-footer">
                      <a type="button" class="btn btn-success" onclick="enviarsolicitudbaja()" id="btnenviarsolibaja"> Enviar Solicitud de Baja</a>
                      <a type="button" class="btn btn-danger pull-right" onclick="cerrarmodalbaja()" data-dismiss="modal" id="cerrarmodalbaja"> Cerrar </a> 
                      
                    </div>
               
                </div>
              </div>
            </div>
<!-- FIN MODAL DE BAJA -->

<!-- fin -->
</div>
</div>
</div>
</div>
</div>
</div>

<?php include('footer.php'); ?>

<div class='modalloading'></div>
</body>

<!-- JAVASCRIPT -->
<script>


  function activarMiMenu() {
    $('#menureservas').removeClass('activo'); 
    $('#menuinicio').removeClass('activo');
    $('#menucontacto').removeClass('activo');
    $('#menulocales').addClass('activo'); 
  }

  window.onload = activarMiMenu;

        //INICIALIZO LOS CHOSEN SELECT
        function iniciarchosen(){
             $(".chosen-select").chosen({
                no_results_text: "No hay resultados!",
                placeholder_text_single: "Seleccionar",
                placeholder_text_multiple: "Seleccionar"
        });
        }
      


//ADMINISTRAR RESERVAS, LEVANTAR RESERVAS

var idbar_reserva;

$(document).on("click", ".modal_adm_reservas", function (){

  $('#tabladereservas').empty();
  $('#titulomodalreservas').html('<h4 class="modal-title" id="titulomodalreservas"><h4><b>Administrar Reservas:</b></h4><h4 style="color:red;"><b>Actualmente no tiene reservas</b></h4>');
  

  idbar_reserva = $(this).data("id");
  $("#verreservas").val(idbar_reserva);
  

  $.ajax({
    url: 'ajax/procesar_administrar_reservas.ajax.php',
    data: {'idbar':idbar_reserva},
    type: 'POST',
    dataType: 'json',

             beforeSend: function () {
             $('body').addClass('loading'); //Agregamos la clase loading al body
            },
       
    success: function( data ) {
      console.log('peticion lograda con exito');
      console.log(data);

if (data['vacio'] == 1){    //SI NO TIENE PRODUCTOS EN LA CARTA SALE
  $('body').removeClass('loading'); //Removemos la clase loading
     return false;
}else{

      var colorestado;
    $('#titulomodalreservas').html('<h4 class="modal-title" id="titulomodalreservas"><h4 id="colorbar"><b>'+data[0][8]+' '+data[0][7]+'</b></h4><b>Administrar Reservas:</b></h4>');
      $('#msgeliminar').addClass('hidden');
      $('#tabladereservas').empty();
      $('#tabladereservas').append('<tr class="tablatit"><th width="200"> <u><b class="azul">Usuario:</b></u>'
        +'</th><th width="200"><u><b class="azul">Nombre:</b></u></th> <th width="200"><u><b class="azul">Fecha:</b> </u>'
        +'</th> <th width="200"><u><b class="azul">Hora:</b></u> </th><th width="500"><u><b class="azul">Teléfono:</b> </u>'
        +'</th><br><th width="200"><u><b class="azul">Personas:</b></u></th><th width="150"><u><b class="azul">Estado:</b></u> '
        +'</th><th width=""></th><th width="">Confirmar<th>Rechazar</th><th>Eliminar</th></tr>');

      for(var i = 0; i < data.length; i++){

        if (data[i][6] == 'Confirmado'){
          colorestado = 'confirmado';
        }else if (data[i][6] == 'Rechazado'){
          colorestado = 'rechazado';

        }else if (data[i][6] == 'Pendiente'){
          colorestado= 'enespera';
        }

        $('#tabladereservas').append('<tr id="fila'+i+'" class="pintar">'
          +'<th width="200">    '
          +'<b>'+data[i][0]+'</b>'
          +'</th>'
          +' <th width="200">'
          +data[i][1]
          +' </th>'
          +' <th width="500">'

          +data[i][2]
          +'</th>'
          +' <th width="500">'

          +data[i][3]
          +'</th>'

          +' <th width="500">'

          +data[i][4]
          +' </th>'
          +'  <br>'
          +' <th width="200">' 
          +data[i][5]
          +' </th>'
          +' <th width="100">'
          +' <b id="celdaestado'+i+'" class="'+colorestado+'">'+data[i][6]+'</b> '
          +' </th>'
          +' <th width="200">'
          + '<th width="200"><a class="btn btn-success" onclick="cambiarcolorconfirmar('+i+')" data-id="'+data[i][0]+'*'+data[i][2]+' '+data[i][3]+'*'+idbar_reserva+'" id="btnconfirmar" title="Confirmar"><b><span class="glyphicon glyphicon-ok"></span></b></a></th>'
          +'<th width="200"><a class="btn btn-danger"  onclick="cambiarcolorrechazar('+i+')" data-id="'+data[i][0]+'*'+data[i][2]+' '+data[i][3]+'*'+idbar_reserva+'" id="btnrechazar" title="Rechazar"><b><span class="glyphicon glyphicon-remove"></span></b></a></th>'
          +'<th width="200"><a class="eliminarreserva btn btn-danger" onclick="borrarreserva('+i+')" data-id="'+data[i][0]+'*'+data[i][2]+' '+data[i][3]+'*'+idbar_reserva+'" id="btnborrar'+i+'" title="Eliminar"><b><span class="glyphicon glyphicon-trash"></span></b></a></th></tr>');
 
 $('body').removeClass('loading'); //Removemos la clase loading

} 

}

},             
error:  function( data ) {

$('body').removeClass('loading'); //Removemos la clase loading

  console.log('se ejecuto mal la peticion');
  console.log(data);
}                          
})

}); 


//FINALIZAR LEVANTAR RESERVAS

//INICIAR ACTUALIZAR RESERVAS

function actualizarreservas(){


    $.ajax({
    url: 'ajax/procesar_administrar_reservas.ajax.php',
    data: {'idbar':idbar_reserva},
    type: 'POST',
    dataType: 'json',

             beforeSend: function () {
             $('body').addClass('loading'); //Agregamos la clase loading al body
            },

    success: function( data ) {
      console.log('peticion lograda con exito');
      console.log(data);


if (data['vacio'] == 1){    //SI NO TIENE PRODUCTOS EN LA CARTA SALE
         $('body').removeClass('loading'); //Removemos la clase loading
     return false;
}else{

      var colorestado;
 $('#titulomodalreservas').html('<h4 class="modal-title" id="titulomodalreservas"><h4 id="colorbar"><b>'+data[0][8]+' '+data[0][7]+'</b></h4><b>Administrar Reservas:</b></h4>');
      $('#msgeliminar').addClass('hidden');
      $('#tabladereservas').empty();
      $('#tabladereservas').append('<tr class="tablatit"><th width="200"> <u><b class="azul">Usuario:</b></u>'
        +'</th><th width="200"><u><b class="azul">Nombre:</b></u></th> <th width="200"><u><b class="azul">Fecha:</b> </u>'
        +'</th> <th width="200"><u><b class="azul">Hora:</b></u> </th><th width="500"><u><b class="azul">Teléfono:</b> </u>'
        +'</th><br><th width="200"><u><b class="azul">Personas:</b></u></th><th width="150"><u><b class="azul">Estado:</b></u> '
        +'</th><th width=""></th><th width="">Confirmar<th>Rechazar</th><th>Eliminar</th></tr>');

      for(var i = 0; i < data.length; i++){

        if (data[i][6] == 'Confirmado'){
          colorestado = 'confirmado';
        }else if (data[i][6] == 'Rechazado'){
          colorestado = 'rechazado';

        }else if (data[i][6] == 'Pendiente'){
          colorestado= 'enespera';
        }

        $('#tabladereservas').append('<tr id="fila'+i+'" class="pintar">'
          +'<th width="200">    '
          +'<b>'+data[i][0]+'</b>'
          +'</th>'
          +' <th width="200">'
          +data[i][1]
          +' </th>'
          +' <th width="500">'

          +data[i][2]
          +'</th>'
          +' <th width="500">'

          +data[i][3]
          +'</th>'

          +' <th width="500">'

          +data[i][4]
          +' </th>'
          +'  <br>'
          +' <th width="200">' 
          +data[i][5]
          +' </th>'
          +' <th width="100">'
          +' <b id="celdaestado'+i+'" class="'+colorestado+'">'+data[i][6]+'</b> '
          +' </th>'
          +' <th width="200">'
          + '<th width="200"><a class="btn btn-success" onclick="cambiarcolorconfirmar('+i+')" data-id="'+data[i][0]+'*'+data[i][2]+' '+data[i][3]+'*'+idbar_reserva+'" id="btnconfirmar" title="Confirmar"><b><span class="glyphicon glyphicon-ok"></span></b></a></th>'
          +'<th width="200"><a class="btn btn-danger"  onclick="cambiarcolorrechazar('+i+')" data-id="'+data[i][0]+'*'+data[i][2]+' '+data[i][3]+'*'+idbar_reserva+'" id="btnrechazar" title="Rechazar"><b><span class="glyphicon glyphicon-remove"></span></b></a></th>'
          +'<th width="200"><a class="eliminarreserva btn btn-danger" onclick="borrarreserva('+i+')" data-id="'+data[i][0]+'*'+data[i][2]+' '+data[i][3]+'*'+idbar_reserva+'" id="btnborrar'+i+'" title="Eliminar"><b><span class="glyphicon glyphicon-trash"></span></b></a></th></tr>');
  
  $('body').removeClass('loading'); //Removemos la clase loading

  }
} 

},             
error:  function( data ) {

  console.log('se ejecuto mal la peticion');
  console.log(data);

  $('body').removeClass('loading'); //Removemos la clase loading
}                          
})

}

//FINALIZAR ACTUALIZAR RESERVAS

//CONFIRMAR RESERVA

var datosreserva;
var arraydatos;


$(document).on("click", "#btnconfirmar", function () {

  datosreserva = $(this).data("id");
  $("#verreservas").val(datosreserva);
  arraydatos = datosreserva.split("*",3); 


  $.ajax({
    url: 'ajax/procesar_misbares_confirmar_reserva.ajax.php',
    data: {'email':arraydatos[0],'idbar':arraydatos[2],'fecha':arraydatos[1]},
    type: 'POST',
    dataType: 'json',

             beforeSend: function () {
             $('body').addClass('loading'); //Agregamos la clase loading al body
            },

      
    success: function( data ) {
      console.log('peticion lograda con exito');
      console.log(data);

      if(data['confirmado'] == 1){
       console.log(data['mensaje']);
        
     }

     $('body').removeClass('loading'); //Removemos la clase loading
     $('#img_confirmar'+posicioncelda).removeClass('glypicon glyphicon-ok');
     $('#img_confirmar'+posicioncelda).append('<img id="loading_confirmar" src="images/ring-alt.gif" width="16" style="padding:0px;margin:0px;">');

   },             
   error:  function( data ) {
    console.log('se ejecuto mal la peticion');
    console.log(data);

     $('body').removeClass('loading'); //Removemos la clase loading

  }                          
}) 

}); 
//FINALIZAR CONFIRMAR RESERVAS

//RECHAZAR RESERVA QUE HAGA EL LOADING EN EL BOTON
var datosreserva;
var arraydatos;

$(document).on("click", "#btnrechazar", function () {
  
  datosreserva = $(this).data("id");
  $("#verreservas").val(datosreserva);
  arraydatos = datosreserva.split("*",3); 

  $.ajax({
    url: 'ajax/procesar_misbares_rechazar_reserva.ajax.php',
    data: {'email':arraydatos[0],'idbar':arraydatos[2],'fecha':arraydatos[1]},
    type: 'POST',
    dataType: 'json',

             beforeSend: function () {
             $('body').addClass('loading'); //Agregamos la clase loading al body
            },

    success: function( data ) {
      console.log('peticion lograda con exito');
      console.log(data);

 $('body').removeClass('loading'); //Removemos la clase loading

      if(data['rechazado'] == 1){
       console.log(data['mensaje']);
       
     }

   },             
   error:  function( data ) {
    console.log('se ejecuto mal la peticion');
    console.log(data);
     $('body').removeClass('loading'); //Removemos la clase loading
  }                          
}) 

}); 

//FIN RECHAZAR RESERVA



//FUNCIONES DE INTERACCION MODAL

function cambiarcolorconfirmar(posicioncelda) {

 $('#celdaestado'+posicioncelda).removeClass('enespera');
 $('#celdaestado'+posicioncelda).removeClass('rechazado');
 $('#celdaestado'+posicioncelda).addClass('confirmado');
 $('#celdaestado'+posicioncelda).html('Confirmado');

}

function cambiarcolorrechazar(posicioncelda) {

 $('#celdaestado'+posicioncelda).removeClass('enespera');
 $('#celdaestado'+posicioncelda).removeClass('confirmado');
 $('#celdaestado'+posicioncelda).addClass('rechazado');
 $('#celdaestado'+posicioncelda).html('Rechazado');
}

//ELIMINAR RESERVA UPDATEANDO ELIMINADA y SACAR FILA

function borrarreserva(posicionfila){
  
 if ($('#celdaestado'+posicionfila).hasClass('enespera')){ 

  $('#msgeliminar').removeClass('hidden');
  return false;

}else if (($('#celdaestado'+posicionfila).hasClass('confirmado') || $('#celdaestado'+posicionfila).hasClass('rechazado'))) {

  var datosreserva;
  var arraydatos;

       datosreserva = $('#btnborrar'+posicionfila+'').data("id");
      $("#verreservas").val(datosreserva);
      arraydatos = datosreserva.split("*",3); 

      $.ajax({
        url: 'ajax/procesar_misbares_eliminar_reserva.ajax.php',
        data: {'email':arraydatos[0],'idbar':arraydatos[2],'fecha':arraydatos[1]},
        type: 'POST',
        dataType: 'json',

             beforeSend: function () {
             $('body').addClass('loading'); //Agregamos la clase loading al body
            },
 
        success: function( data ) {
          console.log('peticion lograda con exito');
          console.log(data);

          if(data['eliminado'] == 1){
           console.log(data['mensaje']);
         }

            $('body').removeClass('loading'); //Removemos la clase loading

       },             
       error:  function( data ) {

        console.log('se ejecuto mal la peticion');
        console.log(data);

        $('body').removeClass('loading'); //Removemos la clase loading
      }                          
    });

$('#fila'+posicionfila).remove();
$('#msgeliminar').addClass('hidden');
}

}
//FIN ELIMINAR RESERVA UPDATEANDO ELIMINADA y SACAR FILA

/*EDITAR BAR LOAD*/

var idbar_e;

$(document).on("click", ".modal_editar_bar", function (){

//HACER CLEAR DE MODAL Y REVISAR UPLOAD
$('#btnfoto').empty();
$('#btnfoto1').empty();
$('#btnfoto2').empty();
$('#btnfoto3').empty();
$('#e_nombre_bar').val("");
$('#e_telefono').val("");
$('#e_direccion').val("");

  idbar_e = $(this).data("id");
  $("#editarbar").val(idbar_e);

  $.ajax({
    url: 'ajax/procesar_load_editar_bar.ajax.php',
    data: {'idbar':idbar_e},
    type: 'POST',
    dataType: 'json',

             beforeSend: function () {
             $('body').addClass('loading'); //Agregamos la clase loading al body
            },
 
    success: function( data ) {
      console.log('peticion lograda con exito');
      console.log(data);

      $('#e_nombre_bar').val(data[0][0]);
      $('#e_telefono').val(data[0][1]);
      $('#e_direccion').val(data[0][2]);
      $('#btnfoto').empty();
      $('#btnfoto1').empty();
      $('#btnfoto2').empty();
      $('#btnfoto3').empty();

      //if (data['foto0'] != null){
        $('#btnfoto').append('<img id="e_foto" src="images/'+data[0][3]+'/principal.jpg" alt="" width="440"  class="cambiarfoto img-responsive img-rounded">');
      //}
     // if (data['foto1'] != null){
        $('#btnfoto1').append('<img id="e_foto1" src="images/'+data[0][3]+'/1.jpg" alt="" width="150"  class="cambiarfoto1 img-responsive img-rounded">');
     // }
      //if (data['foto2'] != null){
        $('#btnfoto2').append('<img id="e_foto2" src="images/'+data[0][3]+'/2.jpg" alt="" width="150"  class="cambiarfoto2 img-responsive img-rounded">');
     // }
     // if (data['foto3'] != null){       
        $('#btnfoto3').append('<img id="e_foto3" src="images/'+data[0][3]+'/3.jpg" alt="" width="150"  class="cambiarfoto3 img-responsive img-rounded">');
     // }

        $('body').removeClass('loading'); //Removemos la clase loading

    },             
    error:  function( data ) {

      console.log('se ejecuto mal la peticion');
      console.log(data);

      $('body').removeClass('loading'); //Removemos la clase loading
    } 

  })

});



//ABRIR DIALOG DE FOTO

$(document).on("click", ".cambiarfoto" , function () {

  if (window.File && window.FileReader && window.FileList && window.Blob) {
  } else {
    alert('The File APIs are not fully supported in this browser.');
  }
function handleFileSelect(evt) {
    var files = evt.target.files; // FileList object
    var output = [];
    for (var i = 0, f; f = files[i]; i++) {
    /*  output.push('<img class="img" width="300" src="images/',escape(f.name),'"></img><li><strong>', escape(f.name), '</strong> (', f.type || 'n/a', ') - ',
                  f.size, ' bytes, last modified: ',
                  f.lastModifiedDate.toLocaleDateString(), '</li>');*/
        $('#msgfotoprin').addClass('hidden');
        $('#msgfotoprin').removeClass('hidden');                  
    /*    $('#btnfoto').empty();   //NO MUESTRA LA FOTO PORQ PRIMERO TENGO Q GUARDARLA EN EL DIRECTORIO 
        $('#btnfoto').append('<img id="e_foto" src="images/'+escape(f.name)+' alt="" width="440"  class="cambiarfoto img-responsive img-rounded">');
*/
}
document.getElementById('list').innerHTML = '<ul>' + output.join('') + '</ul>';
}
document.getElementById('fotoprin').addEventListener('change', handleFileSelect, false);

});



//ABRIR DIALOG SUBFOTO1

$(document).on("click", "#fotosec1" , function () {

  if (window.File && window.FileReader && window.FileList && window.Blob) {
  } else {
    alert('The File APIs are not fully supported in this browser.');
  }
  function handleFileSelect(evt) {
    var files = evt.target.files; 
    var output = [];
    for (var i = 0, f; f = files[i]; i++) {      
        $('#msgfotosec').addClass('hidden');
        $('#msgfotosec').removeClass('hidden');
}
document.getElementById('list1').innerHTML = '<ul>' + output.join('') + '</ul>';
}
document.getElementById('fotosec1').addEventListener('change', handleFileSelect, false);

});



//ABRIR DIALOG SUBFOTO2

$(document).on("click", "#fotosec2" , function () {

  if (window.File && window.FileReader && window.FileList && window.Blob) {
  } else {
    alert('The File APIs are not fully supported in this browser.');
  }
  function handleFileSelect(evt) {
    var files = evt.target.files; 
    var output = [];
    for (var i = 0, f; f = files[i]; i++) {      
        $('#msgfotosec').addClass('hidden');
        $('#msgfotosec').removeClass('hidden');
}
document.getElementById('list2').innerHTML = '<ul>' + output.join('') + '</ul>';
}
document.getElementById('fotosec2').addEventListener('change', handleFileSelect, false);

});



//ABRIR DIALOG SUBFOTO3

$(document).on("click", "#fotosec3" , function () {

  if (window.File && window.FileReader && window.FileList && window.Blob) {
  } else {
    alert('The File APIs are not fully supported in this browser.');
  }
  function handleFileSelect(evt) {
    var files = evt.target.files; 
    var output = [];
    for (var i = 0, f; f = files[i]; i++) {      
        $('#msgfotosec').addClass('hidden');
        $('#msgfotosec').removeClass('hidden');
}
document.getElementById('list3').innerHTML = '<ul>' + output.join('') + '</ul>';
}
document.getElementById('fotosec3').addEventListener('change', handleFileSelect, false);

});

/*ENVIAR CON AJAX ARCHIVO Y GUARDAR FILE EN DIRECTORIO DE SERVIDOR LOCAL+ID*/

function uploadAjax() {


        var inputFileImage = document.getElementById("fotoprin");
        var inputFileImageSec1 = document.getElementById("fotosec1");
        var inputFileImageSec2 = document.getElementById("fotosec2");
        var inputFileImageSec3 = document.getElementById("fotosec3");
        var file = inputFileImage.files[0];
        var file1 = inputFileImageSec1.files[0];
        var file2 = inputFileImageSec2.files[0];
        var file3 = inputFileImageSec3.files[0];
        var e_nombrebar = $("#e_nombre_bar").val();
        var e_telefono = $("#e_telefono").val();

        var data = new FormData();

if (!isNaN(e_telefono) && e_telefono.length < 16 && e_telefono != "" && e_nombrebar != ""){

$('#completetelefono').addClass('hidden');
$('#complete_nombre_bar').addClass('hidden');

  if (file != null){
      data.append('file', file);   
  } 
  if (file1 != null){
      data.append('file1', file1);
  } 
  if (file2 != null){
      data.append('file2', file2);   
  } 
  if (file3 != null){
      data.append('file3', file3);   
  } 
        
  data.append('bar', idbar_e);
  data.append('nombrebar', e_nombrebar);
  data.append('telbar', e_telefono);

      $.ajax({
            url: "ajax/uploadfile.ajax.php",
            type: 'POST',
            contentType: false,
            data: data,
            processData: false,
            cache: false,

             beforeSend: function () {
             $('body').addClass('loading'); //Agregamos la clase loading al body
            },

       success: function( data ) {
          console.log('peticion lograda con exito');
          console.log(data);
           
          if (data['foto'] != null){
             $("#btnfoto").empty();
             $("#btnfoto").append('<img id="e_foto" src="images/Local'+idbar_e+'/'+data['foto']+'" alt="Modificar mi foto prinicpal" width="440" class="img-responsive img-rounded">')
          }
          if (data['foto1'] != null){
             $("#btnfoto1").empty();
             $("#btnfoto1").append('<img id="e_foto1" src="images/Local'+idbar_e+'/'+data['foto1']+'"  alt="" width="150" class="img-responsive img-rounded">')
          }
          if (data['foto2'] != null){
             $("#btnfoto2").empty();
             $("#btnfoto2").append('<img id="e_foto2" src="images/Local'+idbar_e+'/'+data['foto2']+'"  alt="" width="150" class="img-responsive img-rounded">')
          }
          if (data['foto3'] != null){
             $("#btnfoto3").empty();
             $("#btnfoto3").append('<img id="e_foto3" src="images/Local'+idbar_e+'/'+data['foto3']+'"  alt="" width="150" class="img-responsive img-rounded">')
          }

           $('#msgfotoprin').addClass('hidden');
           $('#msgfotosec').addClass('hidden');
           $('#msgguardado').removeClass('hidden'); 

           $('body').removeClass('loading'); //Removemos la clase loading
            
       },             
       error:  function( data ) {
        console.log('se ejecuto mal la peticion');
        console.log(data);  

        $('#msg_error_upload').removeClass('hidden');   

       $('body').removeClass('loading'); //Removemos la clase loading
      }                          
    });


    $('#msgfotoprin').addClass('hidden');
    $('#msgfotosec').addClass('hidden');
    $('#msgguardado').addClass('hidden');
    $('#msgguardado').removeClass('hidden');



    }else{
          if((isNaN(e_telefono) || e_telefono.length > 15 || e_telefono == "") && (e_nombrebar == "")){
            $('#complete_nombre_bar').removeClass('hidden'); 
            $('#completetelefono').removeClass('hidden'); 
            return false;
          }else if (isNaN(e_telefono) || e_telefono.length > 15 || e_telefono == ""){
            $('#complete_nombre_bar').addClass('hidden');
            $('#completetelefono').removeClass('hidden'); 
            return false;
          }else if (e_nombrebar == ""){
            $('#completetelefono').addClass('hidden');
            $('#complete_nombre_bar').removeClass('hidden'); 
            return false;
          }
    }

}

$(document).on("click", "#btncancelaredicion" , function () {
   $('#msgguardado').addClass('hidden');
   $('#msgfotoprin').addClass('hidden');
   $('#msgguardado').addClass('hidden'); 
   $('#completetelefono').addClass('hidden');
   $('#complete_nombre_bar').addClass('hidden');
   $('#msg_error_upload').addClass('hidden');

});


//EDICION CARTA LOAD , TRAER PRODUCTOS CARGADOS Y COMBOS DE TIPO Y PRODUCTO PARA AGREGAR

  var idbarcarta;

  $(document).on("click", ".modal_administrar_carta", function () {

    $("#tablaproductos").empty();
    $('#tituloadmincarta').html('<h4 class="modal-title" id="tituloadmincarta"><h4><b>Administrar la Carta:</b></h4><h4 style="color:red;"><b>Actualmente no tiene productos en la carta</b></h4>');


    idbarcarta = $(this).data("id");
    $("#modificarcarta").val(idbarcarta);

    $.ajax({
      url: 'ajax/procesarcarta.ajax.php',
      data: {'idbar':idbarcarta},
      type: 'POST',
      dataType: 'json',

       beforeSend: function () {
             $('body').addClass('loading'); //Agregamos la clase loading al body
       },
        
      success: function( data ) {

        console.log('peticion lograda con exito');
        console.log(data);

     if (data['vacio'] == 1){    //SI NO TIENE PRODUCTOS EN LA CARTA SALE
      $('body').removeClass('loading'); //Removemos la clase loading
           return false;
     }else{

           $('#tituloadmincarta').html('<h4 class="modal-title" id="tituloadmincarta"><h4 id="colorbar"><b>'+data[1][5]+' '+ data[1][3]+'</b></h4><b>Administrar la Carta:</b></h4>');
           $("#tablaproductos").empty();

            $("#tablaproductos").append('<table id="tablaproductos" class="table table-hover table-border"><tr  class="danger tablatit"><th width="200">'
              +'<h4><u> <b>Tipo Prod.</b></u></h4></th><th width="300">'
              +'<h4><u><b>Producto</b></u></h4></th><th width="200"><h4><u> <b>Precio</b> </u></h4></th>'
             +'<th width="100"><h4><b>Eliminar</b></h4></th></tr>');

        for(var i = 1; i < data.length; i++) {
            $("#tablaproductos").append('<tr id="filapro'+i+'" class="pintar">'
          +'<th width="200">    '
          +'<b>'+data[i][2]+'</b>'
          +'</th>'
          +' <th width="500">'
          +data[i][0]
          +' </th>'
          +' <th width="100">$ '
          +data[i][1]
          +'</th>'
          + '<th width="100">&nbsp;&nbsp;&nbsp;&nbsp;<a class="datosparaborrar btn btn-sm btn-danger" onclick="borrarproducto('+i+')" data-id="'+data[i][1]+'*'+data[i][4]+'" id="btneliminarprod'+i+'" title="Eliminar Producto"><b><span class="glyphicon glyphicon-trash"></span></b></a></th></tr>');  
        }

      $("tablaproductos").append('</table>');

      $('body').removeClass('loading'); //Removemos la clase loading
    }
 },             
 error:  function( data ) {

  console.log('se ejecuto mal la peticion');
  console.log(data);

  $('body').removeClass('loading'); //Removemos la clase loading
}   
}) 
                     
});

//BORRAR FILA DE PRODUCTO Y DELETE DE LA TABLA CARTA

function borrarproducto(posicionfila){

  var datoscarta;
  var datos;

      datoscarta = $('#btneliminarprod'+posicionfila).data("id");
      datos = datoscarta.split("*",2); 

     $.ajax({
        url: 'ajax/procesar_misbares_eliminar_producto_carta.ajax.php',
        data: {'pre':datos[0],'idprodu':datos[1],'barid':idbarcarta},
        type: 'POST',
        dataType: 'json',

       beforeSend: function () {
             $('body').addClass('loading'); //Agregamos la clase loading al body
       },
        
        success: function( data ) {
          console.log('peticion lograda con exito');
          console.log(data);

          if(data['eliminado'] == 1){
           console.log(data['mensaje']);
          $('#filapro'+posicionfila).remove();

           $('body').removeClass('loading'); //Removemos la clase loading
         }

       },             
       error:  function( data ) {
        console.log('se ejecuto mal la peticion');
        console.log(data);

        $('body').removeClass('loading'); //Removemos la clase loading

        alert('No se pudo eliminar el producto');

      }  

    });
    
}

//UPDATEAR CARTA EN CARTA Y SI ES PROMOCION CREAR LA PROMOCION CON TIPO 3 Y DESPUES UPDATEAR CARTA

var tipoprodu;

$(document).ready(function(){


  $('select[id=cmbtipoprodu]').val(0); 

  $("select[id=cmbtipoprodu]").change(function(){
    tipoprodu=$('select[id=cmbtipoprodu]').val();

    if (tipoprodu == 0){

     $('#agregarprodu').addClass('hidden');
     $('#divprodu').addClass('hidden');
     $('#divprecio').addClass('hidden');   
     $('#divpromo').addClass('hidden');
     $('#txtprecio').empty();

   }else if (tipoprodu == 3){

    $('#divpromo').removeClass('hidden');
    $('#agregarprodu').addClass('hidden');
    $('#divprodu').addClass('hidden');
    $('#divprecio').removeClass('hidden');   
    $('#agregarprodu').removeClass('hidden');

   }else{ 
    
     $.ajax({
      url: 'ajax/procesartipoproducto.ajax.php',
      data: {'tipoprodu':tipoprodu},
      type: 'POST',
      dataType: 'json',

       beforeSend: function () {
             $('body').addClass('loading'); //Agregamos la clase loading al body
       },
        
      success: function( data ) {
        console.log('peticion lograda con exito');
        console.log(data);

        $('#divpromo').addClass('hidden');
        $('#txtprecio').empty();
        $('#divprecio').removeClass('hidden');
        $('#cmbprodu').empty();
        $('#divprodu').empty();   
        $('#divprodu').removeClass('hidden');
        $('#divprodu').append('<label for="prod" id="lblprodu" class=" control-label col-md-2"><u>Producto:</u></label>'
            +'<div class="col-md-10"><div id="complete_cmbprodu" class="hidden"><label><h5><b>Seleccione un Producto</b></h5></label></div><select class=" form-control chosen-select" name="cmbprodu" id="cmbprodu" placeholder="Selecciona un Producto">');
        $('#cmbprodu').append('<option value="0">-- Selecciona un Producto</option>');
        for(var i = 1; i < data.length; i++) {
          $("#cmbprodu").append('<option value="'+data[i][1]+'">'+data[i][2]+'</option>');
        }
        
        $('#divprodu').append('</select></div>');
        $('#agregarprodu').removeClass('hidden');

        $('body').removeClass('loading'); //Removemos la clase loading

        iniciarchosen();

      },             
      error: function( data ) {
        console.log('se ejecuto mal la peticion');
        console.log(data);
        $('#agregarprodu').addClass('hidden');
        $('#divprodu').addClass('hidden');
        $('#divprecio').addClass('hidden');    
        $('#divpromo').addClass('hidden');

        $('body').removeClass('loading'); //Removemos la clase loading

      }                         
    })
}

});

});

//CERRAR MODAL

function cerrarmodalcarta(){

  $('#cmbprodu').empty();
  $('#divprodu').addClass('hidden');
  $('#txtprecio').val("");
  $('#divprecio').addClass('hidden');
  $('#agregarprodu').addClass('hidden');
  $('#divpromo').addClass('hidden');
  $('#msgagregado').addClass('hidden');
  $('#txtpromo').val("");
  $('#cmbtipoprodu').val(0);
  $('#msgexistente').addClass('hidden');
  $('#complete_precio').addClass('hidden');
  $('#complete_promo').addClass('hidden');

}

//AGREGAR PRODUCTO A LA CARTA Y SI ES PROMOCION A LAS 2 TABLAS

function agregarproducto(){

  var a_idtipoprodu = $('#cmbtipoprodu').val();
  var a_precio = $('#txtprecio').val();
  

  //SI ES PROMOCION
  if ($('#cmbtipoprodu').val() == 3){
    var a_promo = $('#txtpromo').val();
    var idnuevapromo;

if (!isNaN(a_precio) && a_precio != "" && a_promo != ""){
$('#complete_precio').addClass('hidden');
//AGREGAR NUEVA PROMOCION A PRODUCTOS

     $.ajax({
      url: 'ajax/agregarpromocion.ajax.php',
      data: {'tipoprodu':a_idtipoprodu, 'promo':a_promo, 'pre':a_precio,'barid':idbarcarta},
      type: 'POST',
      dataType: 'json',

       beforeSend: function () {
             $('body').addClass('loading'); //Agregamos la clase loading al body
       },
           
      success: function( data ) {
        console.log('peticion lograda con exito');
        console.log(data);

        idnuevapromo = data['id_nuevapromocion'];

        $('body').removeClass('loading'); //Removemos la clase loading
//INICIAR AGREGAR PRODUCTO A CARTA CON IDNUEVAPROMO

 $.ajax({
      url: 'ajax/agregarproducto.ajax.php',
      data: {'tipoprodu':a_idtipoprodu, 'produ':idnuevapromo, 'pre':a_precio,'barid':idbarcarta},
      type: 'POST',
      dataType: 'json',

       beforeSend: function () {
             $('body').addClass('loading'); //Agregamos la clase loading al body
       },

      success: function( data ) {
        console.log('peticion lograda con exito');
        console.log(data);

$('body').removeClass('loading'); //Removemos la clase loading
//ACTUALIZAR TABLA CARTA

    $.ajax({
      url: 'ajax/procesarcarta.ajax.php',
      data: {'idbar':idbarcarta},
      type: 'POST',
      dataType: 'json',

       beforeSend: function () {
             $('body').addClass('loading'); //Agregamos la clase loading al body
       },

      success: function( data ) {
        console.log('peticion lograda con exito');
        console.log(data);

        $('#tituloadmincarta').html('<h4 class="modal-title" id="tituloadmincarta"><h4 id="colorbar"><b>Bar ' + data[1][3] + '</b></h4><b>Administrar la Carta:</b></h4>');
 $("#tablaproductos").empty();

$("#tablaproductos").append('<table id="tablaproductos" class="table table-hover table-border"><tr  class="danger tablatit"><th width="200">'
              +'<h4><u> <b>Tipo Prod.</b></u></h4></th><th width="300">'
              +'<h4><u><b>Producto</b></u></h4></th><th width="200"><h4><u> <b>Precio</b> </u></h4></th>'
             +'<th width="100"><h4><b>Eliminar</b></h4></th></tr>');
        for(var i = 1; i < data.length; i++) {
            $("#tablaproductos").append('<tr id="filapro'+i+'" class="pintar">'
          +'<th width="200">    '
          +'<b>'+data[i][2]+'</b>'
          +'</th>'
          +' <th width="500">'
          +data[i][0]
          +' </th>'
          +' <th width="100">$ '
          +data[i][1]
          +'</th>'
          + '<th width="100">&nbsp;&nbsp;&nbsp;&nbsp;<a class="datosparaborrar btn btn-sm btn-danger" onclick="borrarproducto('+i+')" data-id="'+data[i][1]+'*'+data[i][4]+'" id="btneliminarprod'+i+'" title="Eliminar Producto"><b><span class="glyphicon glyphicon-trash"></span></b></a></th></tr>');  
        }

      $("tablaproductos").append('</table>');

       $('body').removeClass('loading'); //Removemos la clase loading
     },             
     error:  function( data ) {

      console.log('se ejecuto mal la peticion');
      console.log(data);

      $('body').removeClass('loading'); //Removemos la clase loading
    }   
    });   

//FINALIZAR ACTUALIZAR TABLA CARTA

      },  
      error: function( data ) {
        console.log('se ejecuto mal la peticion');
        console.log(data);

        $('body').removeClass('loading'); //Removemos la clase loading
      }                         
    });


//FINALIZAR AGREGAR PRODUCTO A CARTA

      $('#msgexistente').addClass('hidden');
      $('#msgagregado').removeClass('hidden');
      },             
      error: function( data ) {
        console.log('se ejecuto mal la peticion');
        console.log(data);
         $('#msgagregado').addClass('hidden');
         $('#msgexistente').addClass('hidden');

         $('body').removeClass('loading'); //Removemos la clase loading
      } 

    });

//FINALIZAR AGREGAR NUEVA PROMOCION A PRODUCTOS

}else{
  $('#msgagregado').addClass('hidden');
  $('#msgexistente').addClass('hidden');
  if((isNaN(a_precio) || a_precio == "") && (a_promo == "")){
  $('#complete_precio').removeClass('hidden');
  $('#complete_promo').removeClass('hidden');
  }else if (isNaN(a_precio) || a_precio == ""){
  $('#complete_promo').addClass('hidden');
  $('#complete_precio').removeClass('hidden');
  }else if (a_promo == ""){
  $('#complete_precio').addClass('hidden');
  $('#complete_promo').removeClass('hidden');
  }
}


    //SI NO ES PROMOCION Y ES PRODUCTO
  }else{

  var a_idprodu = $('#cmbprodu').val();
 
if (!isNaN(a_precio) && a_precio != "" && a_idprodu != 0){
$('#complete_precio').addClass('hidden');
$('#complete_cmbprodu').addClass('hidden');
     $.ajax({
      url: 'ajax/agregarproducto.ajax.php',
      data: {'tipoprodu':a_idtipoprodu, 'produ':a_idprodu, 'pre':a_precio,'barid':idbarcarta},
      type: 'POST',
      dataType: 'json',

       beforeSend: function () {
             $('body').addClass('loading'); //Agregamos la clase loading al body
       },

      success: function( data ) {
        console.log('peticion lograda con exito');
        console.log(data);

        if(data['existente'] == 0){
        $('#msgexistente').addClass('hidden');
        $('#msgagregado').removeClass('hidden');

        $('body').removeClass('loading'); //Removemos la clase loading

//ACTUALIZAR TABLA CARTA

    $.ajax({
      url: 'ajax/procesarcarta.ajax.php',
      data: {'idbar':idbarcarta},
      type: 'POST',
      dataType: 'json',

       beforeSend: function () {
             $('body').addClass('loading'); //Agregamos la clase loading al body
       },

      success: function( data ) {
        console.log('peticion lograda con exito');
        console.log(data);

     

        $('#tituloadmincarta').html('<h4 class="modal-title" id="tituloadmincarta"><h4 id="colorbar"><b>Bar ' + data[1][3] + '</b></h4><b>Administrar la Carta:</b></h4>');
 $("#tablaproductos").empty();

$("#tablaproductos").append('<table id="tablaproductos" class="table table-hover table-border"><tr  class="danger tablatit"><th width="200">'
              +'<h4><u> <b>Tipo Prod.</b></u></h4></th><th width="300">'
              +'<h4><u><b>Producto</b></u></h4></th><th width="200"><h4><u> <b>Precio</b> </u></h4></th>'
             +'<th width="100"><h4><b>Eliminar</b></h4></th></tr>');
        for(var i = 1; i < data.length; i++) {
            $("#tablaproductos").append('<tr id="filapro'+i+'" class="pintar">'
          +'<th width="200">    '
          +'<b>'+data[i][2]+'</b>'
          +'</th>'
          +' <th width="500">'
          +data[i][0]
          +' </th>'
          +' <th width="100">$ '
          +data[i][1]
          +'</th>'
          + '<th width="100">&nbsp;&nbsp;&nbsp;&nbsp;<a class="datosparaborrar btn btn-sm btn-danger" onclick="borrarproducto('+i+')" data-id="'+data[i][1]+'*'+data[i][4]+'" id="btneliminarprod'+i+'" title="Eliminar Producto"><b><span class="glyphicon glyphicon-trash"></span></b></a></th></tr>');  
        }
      $("tablaproductos").append('</table>');

     $('body').removeClass('loading'); //Removemos la clase loading
     },             
     error:  function( data ) {

      console.log('se ejecuto mal la peticion');
      console.log(data);

          $('body').removeClass('loading'); //Removemos la clase loading
    }   
    })  

    //FINALIZAR ACTUALIZAR TABLA CARTA
       
        }else{       
        $('#msgagregado').addClass('hidden');
        $('#msgexistente').removeClass('hidden');
        }

         $('body').removeClass('loading'); //Removemos la clase loading
      },             
      error: function( data ) {
        console.log('se ejecuto mal la peticion');
        console.log(data);
         $('#msgagregado').addClass('hidden');
         $('#msgexistente').removeClass('hidden');

        $('body').removeClass('loading'); //Removemos la clase loading
      }                         
    });
}else{
  $('#msgagregado').addClass('hidden');
  $('#msgexistente').addClass('hidden');

  if (a_idprodu == 0){
  $('#complete_cmbprodu').removeClass('hidden');

  }else{
  $('#complete_cmbprodu').addClass('hidden');  
  }
  if (isNaN(a_precio) || a_precio == ""){
  $('#complete_precio').removeClass('hidden');
  }else{
  $('#complete_precio').addClass('hidden');
  }

}
}

}

//FINALIZAR FUNCION AGREGARPRODUCTO

//ENVIAR SOLICITUD BAJA BAR

var idbar_baja;

$(document).on("click", ".modal_baja_bar", function (){
  
  idbar_baja = $(this).data("id");
  $("#eliminarbar").val(idbar_baja);
 
});

function enviarsolicitudbaja(){

   var motivo_baja = $('#txt_b_motivo').val();
   if (motivo_baja.length < 250 && motivo_baja != ""){

    $('#complete_motivo').addClass('hidden');

  $.ajax({
      url: 'ajax/procesar_baja_bar.ajax.php',
      data: {'barid':idbar_baja,'motivo':motivo_baja},
      type: 'POST',
      dataType: 'json',

       beforeSend: function () {
             $('body').addClass('loading'); //Agregamos la clase loading al body
       },

      success: function( data ) {
        console.log('peticion lograda con exito');
        console.log(data);

        if(data['registrado'] == 0){
        $('#txt_b_motivo').val("");
        $('#msg_solibaja_enviada').addClass('hidden');
        $('#msg_solibaja_error').removeClass('hidden');

        $('body').removeClass('loading'); //Removemos la clase loading
        }else{
        $('#txt_b_motivo').val("");
        $('#msg_solibaja_error').addClass('hidden');
        $('#msg_solibaja_enviada').removeClass('hidden');

        $('body').removeClass('loading'); //Removemos la clase loading
        }

       },             
      error: function( data ) {
        console.log('se ejecuto mal la peticion');
        console.log(data);
   
      $('#msg_solibaja_enviada').addClass('hidden');
      $('#msg_solibaja_error').removeClass('hidden');

       $('body').removeClass('loading'); //Removemos la clase loading
      }, 
    });

  }else{
    $('#msg_solibaja_error').addClass('hidden');
    $('#msg_solibaja_enviada').addClass('hidden');  
    $('#complete_motivo').removeClass('hidden');
  }

}


function cerrarmodalbaja(){
  $('#txt_b_motivo').val("");
  $('#complete_motivo').addClass('hidden');
  $('#msg_solibaja_error').addClass('hidden');
  $('#msg_solibaja_enviada').addClass('hidden');
}

//ACTUALIZAR RESERVAS CADA 10 SEG

/*$(document).ready(function() { 
    setInterval(actualizarreservas, 10000);
       
});*/

</script>

<script type="text/javascript">
document.oncontextmenu = function(){return false;}
</script>
<script> loadingOFF(); </script>
</html>