
<?php

if(session_id() == '' || !isset($_SESSION)) {
  session_start();
}

if (!(isset($_SESSION['iniciado']))) {
  header ("Location: logeo.php");
  exit();
}

if ($_SESSION['tipo'] != 'A'){
  header ("Location: logeo.php");
  exit();
}


if ($_SESSION['iniciado'] != '5dbc98dcc983a70728bd082d1a47546e'){
  header ("Location: logeo.php");
  exit();

}

function echoActiveClassIfRequestMatches($requestUri)
{
    $current_file_name = basename($_SERVER['REQUEST_URI'], ".php");

    if ($current_file_name == $requestUri)
        echo ' activo';
}

function echoActiveClassIfRequestM($requestUri)
{
    $current_file_name = basename($_SERVER['REQUEST_URI'], ".php");

    if ($current_file_name == $requestUri)
        echo 'class="active"';
}

?>

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Ir a Drinky</a>
            </div>
            <!-- Top Menu Items -->
          <ul class="nav navbar-right top-nav">
              <!--  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu message-dropdown">
                        <li class="message-preview">
                            <a href="#">
                                <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                    <div class="media-body">
                                        <h5 class="media-heading"><strong></strong>
                                        </h5>
                                        <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="message-preview">
                            <a href="#">
                                <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                    <div class="media-body">
                                        <h5 class="media-heading"><strong><?php echo $_SESSION['usuario']; ?></strong>
                                        </h5>
                                        <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="message-preview">
                            <a href="#">
                                <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                    <div class="media-body">
                                        <h5 class="media-heading"><strong> <?php echo $_SESSION['usuario']; ?> </strong>
                                        </h5>
                                        <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="message-footer">
                            <a href="#">Read All New Messages</a>
                        </li>
                    </ul>
                </li> -->
                <!--<li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu alert-dropdown">
                        <li>
                            <a href="#">Alert Name <span class="label label-default">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-primary">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-success">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-info">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-warning">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-danger">Alert Badge</span></a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">View All</a>
                        </li>
                    </ul>
                </li>-->
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $_SESSION['usuario']; ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                      <!--  <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li> -->
                        <li> 
                            <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a class="btn-lg" style="padding:25px;" href="admin-index.php"><i class="fa fa-fw fa-dashboard"></i> Tablero</a>
                    </li>
                    <li class="hidden">
                        <a href="admin-charts.php"><i class="fa fa-fw fa-bar-chart-o"></i> Charts</a>
                    </li>
                    <li class="hidden">
                        <a href="admin-tables.php"><i class="fa fa-fw fa-table"></i> Tables</a>
                    </li>
                    <li class="hidden">
                        <a href="admin-forms.php"><i class="fa fa-fw fa-edit"></i> Forms</a>
                    </li>
                    <li class="hidden">
                        <a href="admin-elements.php"><i class="fa fa-fw fa-desktop"></i> Bootstrap Elements</a>
                    </li>
                    <li class="hidden">
                        <a href="admin-bootstrap-grid.php"><i class="fa fa-fw fa-wrench"></i> Bootstrap Grid</a>
                    </li>
                    <li>
                        <a class="btn-lg"  style="padding:25px;" href="javascript:;" data-toggle="collapse" data-target="#menusolicitudes"><i class="fa fa-fw fa-arrows-v"></i> Solicitudes <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul  id="menusolicitudes" class="collapse">
                            <li>
                                <a href="#">Alta Local</a>
                            </li>
                            <li>
                                <a href="#">Baja Local</a>
                            </li>
                            <li>
                                <a href="#">Cambio de Dirección</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="btn-lg" style="padding:25px;" href="javascript:;" data-toggle="collapse" data-target="#menuubicacion"><i class="fa fa-fw fa-arrows-v"></i> Ubicaciones <i class="fa fa-fw fa-caret-down"></i> </a>
                        <ul id="menuubicacion" class="collapse">
                            <li>
                                <a href="admin-ubicacion-localidades.php"> ABM Localidad</a>
                            </li>
                            <li>
                                <a href="admin-ubicacion-barrios.php">ABM Barrio</a>
                            </li>
                            <li>
                                <a href="admin-ubicacion-cordenadas.php">ABM Cordenadas Local</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="btn-lg" style="padding:25px;" href="javascript:;" data-toggle="collapse" data-target="#menuproductos"><i class="fa fa-fw fa-arrows-v"></i> Productos <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="menuproductos" class="collapse">
                            <li>
                                <a href="admin-productos-datos.php">Datos Productos</a>
                            </li>
                            <li>
                                <a href="admin-productos-productos.php">ABM Producto</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="btn-lg" style="padding:25px;" href="javascript:;" data-toggle="collapse" data-target="#menulocales"><i class="fa fa-fw fa-arrows-v"></i> Locales <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="menulocales" class="collapse">
                            <li>
                                <a href="admin-locales-datos.php">Datos Locales</a>
                            </li>
                            <li>
                                <a href="admin-locales-new.php">Agregar Nuevo Local</a>
                            </li>
                            <li>
                                <a href="admin-locales-update.php">Alta / Baja / Modificación</a>
                            </li>
                            <li>
                                <a href="admin-locales-eliminar.php">Eliminar Local</a>
                            </li>

                        </ul>
                    </li>
                     <li>
                        <a class="btn-lg" style="padding:25px;" href="admin-mensajes.php"><i class="fa fa-fw fa-file"></i> Mensajes</a>
                    </li>
                    <li>
                        <a class="btn-lg" style="padding:25px;" href="javascript:;" data-toggle="collapse" data-target="#menuusuarios"><i class="fa fa-fw fa-user"></i> Usuarios <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="menuusuarios" class="collapse">
                            <li>
                                <a href="admin-usuarios-monitoreo.php">Datos y Monitoreo de Usuarios</a>
                            </li>
                            <li>
                                <a href="admin-usuarios-altabaja.php">Alta/Baja y Roles de Usuario</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>
